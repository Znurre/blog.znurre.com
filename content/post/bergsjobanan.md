+++
draft = false
title = "Bergsjöbanan"
date = "2020-07-01T22:27:00+02:00"
tags = ["cykling", "äventyr"]
type = "post"

+++


I måndags hade jag egentligen tänkt publicera den första delen av den serie som skulle bli introduktionen till min cykelsemester, men istället så har jag snubblat över någonting annat, dock inte helt orelaterat, som jag kände ett behov av att dokumentera.

Som så ofta är fallet med mig så snöar jag lätt in på ett visst ämne och blir närmast besatt av det under en kort period. Den här gången handlade det om en smalspårsbana i trakten - Bergsjöbanan, som enligt informationen jag kunnat hitta var i bruk fram till 1962 då den lades ner på grund av olönsamhet.

Någon gång har sedan själva spåret tagits bort, men det finns inga uppgifter om exakt när. Det innebär att det finns en banvall kvar, som det fortfarande går att finna spår av i naturen, och det var denna jag hittade när jag igår letade efter nya rutter att cykla i närheten. 

På min kommande cykelsemester kommer jag vid flera tillfällen att cykla på gammal banvall som gjorts om till cykelväg, och det är väldigt tacksamt eftersom det är så pass lite lutning. Därför blev jag förvånad, och väldigt nyfiken när jag fick reda på att vi faktiskt hade en gammal banvall även här i närheten. 

Det började med att jag cyklade den bit som går mellan Rogsta och Via, eftersom det var den enda bit jag kände till vid det tillfället. Den delen av sträckan är cirka 5 km, och börjar som vanlig grusväg för att sedan bli något smalare och sämre efter cirka 1 km. Här finns även en skylt som varnar för travhästar.

Fortsätter man vidare så kommer man sedan ut på den vanliga grusvägen mellan Rogsta och Via, men kan då välja att fortsätta rakt över, in på vad som ser ut som en skogsstig. Här fortsätter banvallen i en kombination av halvbred skogsstig med pressad jord och barr, och stundvisa inslag av lös sand. De sandiga partierna erbjuder en hel del utmaning, och ger även en ledtråd kring vad man kan förvänta sig av återstoden av vägen.

Slutligen kommer man ut i ett litet bostadsområde i Via, och här valde jag att cykla hem igen. Väl hemma kunde jag dock inte släppa tanken på järnvägen, och försökte således att hitta mer information kring den och dess ursprungliga sträckning.

Det visade sig finnas väldigt lite information, men jag hittade [följande](http://www.jvmv2.se/gamlaforum/3673.htm) [sidor](http://cyklabanvall.nu/en/top/smalspariga-jaernvaegar/norra-haelsinglands-jaernvaeg/) som bara ökade på min nyfikenhet ytterligare. 

Enligt dessa skulle alltså stora delar av banvallen fortfarande finnas kvar, och dessutom gå att cykla. Däremot fanns det så pass lite information att ett sådant tilltag framstod vara förknippat med en stor mängd ovisshet, och det fick det hela att kännas som ett riktigt äventyr.

Jag skapade så gott jag kunde upp en rutt på Strava med hjälp av satelitbilder och OpenStreetMaps. Det visade sig i efterhand att någon redan mappat ut Bergsjöbanan som en rutt på OSM, men det upptäckte jag först senare.

Så var det dags att ge sig ut. Till sällskap lyckades jag lura med mig Jimmy, som har en elcykel med halvgrova däck, och vi gav oss alltså ut för att upptäcka Bergsjöbanan i dess helhet.

Innan ni läser resten rekommenderar jag er att öppna [följande karta](http://umap.openstreetmap.fr/en/map/untitled-map_475767#11/61.8560/17.4758), så att ni lättare kan följa med i den rutt som beskrivs nedan.

Vi började även denna gång i Rogsta, även om vi teoretiskt sett hade kunnat starta redan i Östanbräck. Enligt information från ovanstående sidor så ska dock banvallen på den sträckan vara i princip igenväxt. Kanske någonting att undersöka nästa gång?

Turen gick alltså sedan via den del av sträckan jag redan hade åkt en gång tidigare den dagen, och vi tog oss rätt snabbt till Via. Därefter hittade vi utan problem fortsättningen på banvallen, där början hade blivit till grusväg. Redan efter ett femtiotal meter så svängde dock banvallen av inåt skogen, och vi tog därför av från den betydligt bättre grusvägen till det som mestadels liknade två hjulspår i gräset. Det var dock tydligt att detta varit banvallen, då vegetationen här var i princip obefintlig i förhållande till omgivningen, och det var inget problem att cykla där.

Redan efter ett lite tag började det dock bli utmanande att cykla där, då banvallen gått från att vara gräs, till vad som mest liknade en smal, vägliknande sandstrand. Så länge man höll farten uppe gick det hjälpligt att komma fram, men även jag slirade runt rejält, och Jimmy hade det betydligt värre med sin elcykel. Det blev inte heller bättre av att sanden hade samlat sig i sanddyner. 

Vi funderade ett tag på att ge upp, men kämpade vidare, och fick lön för mödan. Några hundra meter senare blev underlaget hårdare och mer lättcyklat igen, och ytterligare lite senare kom vi ut på en större grusväg.

Vid det här laget hade vi kommit till Långsjön, och cyklade nu åter parallelt med Ostkustbanan. När vi hade cyklat längs ungefär hälften av sjön möttes vi av en vägbom som hade som syfte att spärra för obehörig motortrafik. Vi ignorerade den, och fortsatte vår resa, fortfarande längs en grusväg som höll god kvalitet.

Värt att nämna är också att vägen hela tiden varit väldigt plan, utan mycket till lutning alls. Precis vad man hade kunnat förvänta sig av en gammal banvall alltså.

Nu närmade sig dock det första av två stora hinder som jag förutsett skulle bli problematiska för oss. Precis bortom Långsjön svänger nämligen Otkustbanan österut, och korsar därför den gamla banvallen, dock utan att erbjuda någon typ av övergång eller överfart. 

Vi hade nu två alternativ. Antingen kunde vi vända tillbaka till början av sjön, och istället åka längs den östra sidan av sjön. Det skulle ta oss till en järnvägsövergång, och så småningom tillbaka till banvallen. Nackdelen var såklart att detta var en stor omväg. Det andra alternativet innebar att åka tillbaka en bit, och sedan korsa E4an. På andra sidan fanns nämligen en grusväg som troligen skapats för att kunna utföra underhållsarbete på de elledningar som går längs E4an. 

Vi valde det senare alternativet, och blev medvetna om hur bortskämda vi varit vid den i princip obefintliga lutningen hittils. Den här nya vägen bjöd nämligen på rikligt med stigning. Några minuter senare var vi åter igen tillbaka till E4an, nu en bit längre fram. Här korsade vi E4an ytterligare en gång, och befann oss så åter igen på banvallen.

Med detta hinder överstökat började dock hinder nummer två att komma allt närmare. Lite längre fram gick nämligen banvallen ut i det som numera är E4an, och försvann således. E4an är som känt omringad av viltstängsel, så vi kunde inte heller ta oss ut dit även om vi nu hade velat. Ett svagt hopp tändes dock när vi upptäckte en skogsstig som gick nästan rakt österut från den plats vi befann oss.

Utan att veta vart denna stig skulle ta oss gav vi oss iväg. Vi hade tur, och märkte snart att vi kommit ut på den väg som gick öster om sjön, men på norra sidan om Ostkustbanan, och kunde således fortsätta på den i några hundra meter innan del sammanstrålade med banvallen igen.

Banvallen fortsatte sedan i form av hyfsat fin grusväg längs Harsjön, och svängde sedan västerut mot Vattrång. Precis innan Vattrång paserade vi under E4an i en tunnel. 

Här blev det lite knepigt igen. Skulle vi följa den gamla banvallens sträckning slaviskt skulle vi behöva cykla ut på vad som såg ut att vara en åker, och det kändes inte helt okej, så vi valde att ta en omväg istället. När vi kom till den punkt på andra sidan där banvallen började igen såg vi dock att det faktiskt gick en tydlig väg in även på den sida vi kommit från. Det är alltså möjligt att det faktiskt går att cykla där, bara det att vägen syns bättre om man kommer från Bergsjö-hållet.

Intressant kuriosa är också att den väg som började här faktiskt verkar heta just Bergsjöbanan. Det är en grusväg, ganska smal, dock relativt lättcyklad med en lite tuffare cykel. Den fortsatte sedan i ytterligare 5 km eller så, innan vi kom ut på Forsavägen som löper mellan Vattlång och Harmånger.

Här hade järnvägen tidigare gått en kort bit genom vad som nu var ett område med några hus och tomter. Vi tog således en liten omväg, men kom snabbt tillbaka på banvallen igen, som här hade formen av en fin och bred grusväg som följde Storsjöns strand nästan hela vägen till Bergsjö.

Vi kom snart fram till ett litet samhälle som heter Högen, och som ligger precis innan Bergsjö. Här fick vi återigen kompromissa något, eftersom delar av banvallen tyvärr är försvunna. Vi valde att göra det enkelt för oss, och åkte helt enkelt ut på Bergsjövägen en bit, innan vi återigen svängde av ner till höger för att ta banvallen den sista biten till Bergsjö.

För att sammanfatta så var faktiskt större delen av sträckan helt klart cyklingsbar, till och med njutningsbar skulle jag säga. Däremot förtog vissa delar av vägen något av nöjet, som t.ex. den otroligt sandiga biten mellan Via och Långsjön, avsaknaden av överfart över Ostkustbanan efter Långsjön, samt det ställe där banvallen helt försvann i det som nu är E4an.

Jag skulle rekommendera alla som har en gravel bike eller MTB att testa denna rutt, dock med en modifikation. Genom att följa Hudiksvägen från Via en bit ges möjligheten att följa den grusväg som går öster om Långsjön istället för banvallen på den västra sidan. På så sätt kan alla de tre problematiska delarna undvikas i ett svep. Givetvis inte lika kul att inte kunna följa banvallen hela vägen, och det kommer även att öka på lutningen något. Totalt sett lär det dock bli en mycket bättre upplevelse.
