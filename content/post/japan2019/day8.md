+++
keywords = [
  "japan",
  "traveling",
]
type = "post"
draft = false
date = "2019-08-11T14:37:13+02:00"
title = "Japan 2019: Day 8"

+++

# Japan 2019: Day 8

We woke up in Takayama at 9:00 for a change, as we did not have to check out until 11:00.

We used that time to prepare ourselves, and we then went down to the lobby to ask the receptionist for help with booking bikes for the following day.

The idea all along had been to go by bike over the small islands connecting Honshu with Shikoku, but Steven had missed the fact that reservations for bike rental had to be done far in advance, so the cheaper rental service was already fully booked.

There was another bike rental service as well, which was however a bit trickier to book due to only accepting reservations over phone. Therefore we figured a native japanese speaker should have an easier time doing the call.

The lady at the reception gladly helped us, but unfortunately there was no response, and as such we decided to try again ourselves later, either from a public phone booth or using Skype.

After having checked out from the hotel, we went to reserve seats for our train rides for the day. This is done at the station buildings, and luckily it was not too far from our hotel.

During this trip, we have reserved seats a number of times. It's a convenient way of making sure you have a designated seat for a particular train ride. Without a seat reservation, you might have to stand during the entire train ride, which is obviously not too nice.

This particular seat reservation was special though. The guy behind the counter who helped us reserve our seats was so insanely fast. We had barely even mentioned where we wanted to go before he had printed the tickets for us. So far, I have never seen this happen.

Our hotel reservation did not come with breakfast, and we had not yet eaten anything. The time was already past 11 though, so we went for an early lunch instead at a small restaurant on the same street as the hotel we had stayed at. The place seemed to be run by one old man with a passion for cooking, and it was a nice and cosy little place.

I ordered a dish with fried chicken in sweet and sour sauce, and Steven went for a spicy ramen. Both our dishes were quite okay, although I found mine to be very strange. The name of the dish was very fitting. It **was** really sweet and sour. The whole dish was drenched in vinegar, and had quite an edge, which I assume came from leek and black pepper.

The chicken was okay. Still included more skin than I was comfortable with, but at least it was only meat inside.

Following the early lunch, we went for a stroll in the city around the older, preserved parts of the city.

![Takayama](/takayama1.jpg)

We found our way up to a small, elevated park, where we sat down and admired the view of the city roofs.

![Takayama from above](/takayama2.jpg)

The heat was still intense as always, and I was trying to cover up my sunburnt shoulders by keeping my cardigan on, which of course made things even warmer. We therefore made our way down to the city again, and sat down on a bench in the shadow outside a small store serving a variety of small dishes. 

I ordered a "kakigoori", or shaved ice, for both of us. Kakigoori is one of the best ways to survive in the extreme summer heat of Japan, as it's freezing cold and also gives you plenty of liquid, while being tasty at the same time.

I found the kakigoori sold at this store to be a bit pricy, but we soon understood why when we received two **huge** bowls of kakigoori.

![Kakigoori](/kakigoori.jpg)

Needless to say, it took us quite some time to eat all of it.

When we were done, I asked the lady at the store for directions to the nearest public phone booth. It just so happened that it was under a tree on the other side of the road. They apparently still have plenty of public phone booths here in Japan.

Inside the phone booth it was scorching hot as the wind could not get inside. I tried to call the number to the bike rental service, but it seemed like they could not hear me. After the second attempt with the same result, I concluded that this phone booth was probably not working as it should, and we instead went for a nearby park in order to make a Skype call from Steven's phone.

I did the Skype call, as Steven does not speak very much japanese, and this time around I could speak with the person in the other end properly. Unfortunately, they were also fully booked. We started to feel disheartened, but I was not ready to give up just yet.

After some searching, I found out that there was actually a third rental service available. Upon calling them, I was told that they unfortunately did not have any bikes available for our specified dates, but they did give me an advice. There was apparently a newly opened store renting out bikes that most people did not know about. I was told to give them a call to check if maybe they would have any bikes available.

I therefore placed a third and final Skype call. The person on the other end of the line for this store did not speak a word of english, and as such I was forced to speak japanese, which actually went surprisingly well. I managed to book bikes for the following day, but unfortunately not for two days since they were fully booked for the monday.

Even so, this felt like a great achievement. This was my first time talking japanese on phone, where you cannot fall back to gestures and body language.

Given that we would only be able to rent the bikes for one day, and that we would have to bring them back to the store as well, we were forced to reconsider our plans a bit. The initial idea had been to go by bike to one of the islands connecting Honshu and Shikoku, where we would stay for the night at a hotel that Steven had booked, and then keep going to Shikoku where we would leave the bikes for them to be picked up.

Instead, Steven booked a hotel on Honshu, in a city near Onomichi where we would pick up the bikes. We figured that we would ride the bikes during the day, return them, and then go to our hotel for the night.

We both felt it was quite a shame that we would only be able to ride the bikes for a few hours, and we were also too late to be able to cancel the other hotel reservation for free. Had we went for this change of plans we would have had to pay the full price for the night, which would have been around 1000 SEK.

The new hotel reservation had very liberal terms, allowing one to cancel their reservation at any time without any fee. Therefore, we decided to wait with cancelling the first hotel reservation since the fee would stay the same anyway.

We hoped that maybe when we went to the bike rental service the following day, some other customer might have cancelled their reservation, and in that case we could go for an only slightly modified version of the original plan.

There was not much more we could do at that moment, so we decided to continue our sightseeing a bit.

![Me at a shrine entrance](/takayama3.jpg)

The heat was insane as always, and my neck was burning from the sunburn I had incurred the day before as my cardigan did not cover it. Taking inspiration from the japanese people around me, I put up my umbrella, which is a very usual sight here in Japan even during sunny days since many japanese women use an umbrella to get some cover from the sun. 

We then sat down for a while at a bench in the shadow near the river flowing through the town, and I went down to the river to wet a towel for my shoulders. 

![Wetting my towel in the river](/towel.jpg)

The cold water against my neck and shoulders was very soothing, and we stayed at that bench for quite some time.

We discussed our transport options for the day. and decided to take an earlier train than initially planned as we both felt quite done with Takayama. The initial transport option would have resulted in an arrival time of around 22:00 in the evening to Kyoto where we would be spending the night, and we were both happy to trade an earlier departure time from Takayama for an earlier arrival time in Kyoto.

As we started to approach our new intended departure time, we went to the station again to make new seat reservations and cancel the others, even though we did not have much hope for any seats to be free since were so late with our reservations.

The ticket office was manned by the same guy as before, and just as before he was insanely quick to make the reservations for us. He managed to find seats for us for both of our trains, and even managed to get us seats next to each other. What a hero :)

We boarded the train which would be going for Nagoya, where we would be transferring to Shinkansen for the last distance to Kyoto. Only later did we realize that we could have taken the same train all the way to Kyoto. Apparently this train split in two halves at a certain station, one part going to Nagoya while the other went to Kyoto. The arrival time would have been roughly the same, with the benefit of not having to change trains and wait at the station.

The train for Nagoya was late, which is a very rare occurence in Japan. It was only late by a few minutes though, and we managed to catch our next train without any problems.

Steven had booked a hotel very close to Kyoto station, so the walk there was very short, which I greatly appreciated. I really liked the hotel, which featured small, but very modern and clean rooms.

![Kyoto Tower Annex](/kta.jpg)

At this point we were both very hungry, and asked the staff for restaurant recommendations.

Steven had not yet tried Sukiyaki, a dish which Kyoto is very known for, and which is basically thin slices of meat grilled together with vegetables in a boiling soy sauce. I therefore asked for a Sukiyaki restaurant, which the very friendly young receptionist gladly helped us with. He recommended us to go to the top floor of a big shopping mall which was featuring several different restaurants.

We quickly found the restaurant, and sat for probably 30 minutes waiting to have a seat since the restaurant was very busy.

When we got our table, we got a very basic explanation of how to cook our food, and that rice and vegetables were available as a buffet style service. We were also given quite a large amount of meat to grill. After this, we were left completely alone without any help at all. 

When I've had Sukiyaki previous years there has always been someone keeping an eye on the temperature of the pot, pouring in more sauce and so on, but at this place there was nothing like that. Our sauce got burnt, and everything we tried to grill afterwards tasted like burnt soy sauce.

In the end, the whole thing just felt like a huge waste of money since it was not exactly cheap either.

We went to bed shortly afterwards since we would have to wake up early the morning after.
