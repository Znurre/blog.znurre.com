+++
tags = [
  "japan",
  "traveling",
]
date = "2019-08-14T16:03:52+02:00"
title = "Japan 2019: Day 11"
draft = false
type = "post"

+++

# Japan 2019: Day 11

We woke up quite early the next morning, as the plan was to go up the mountains with an early bus. The idea was to go to an old village, which had been preserved in its original condition, and walk around there while waiting for another bus which would take us to our two primary destinations for the day.

One of the intended destinations was a village which had more or less been emptied of people, and in their place, an old woman had made dolls and populated the village with them. The second intended destination was two vine bridges up in the mountains.

As there was a typhoon coming from the sea towards our current location, we were a bit unsure of whether it would be safe to carry out the plans. Typhoons usually bring with them heavy winds and rains, disrupting traffic among other things, and we did not want to risk getting stuck in the mountains.

In the end we decided that the typhoon was so far away that it would not cause us any problems, and went ahead with the plan as intended.

We therefore took the bus up in the mountains, which was so full of people that we had to stand. Most of them were tourists however, and they all got off at a popular tourist attraction about halfway to our final destination.

The road we were going on was full of heavy curves, and it was narrow to the point where only one vehicle could pass in either direction at some stretches. As a result, there were a lot of sudden stops and negotiations between the vehicles of who should keep moving on and who should retreat to make place for the other. Trying to keep myself standing firmly inside the bus was quite a workout.

About an hour later we reached our stop and got off the bus. As usual, Steven was the one who had arranged everything, so I did not really know what to expect at all.

We quickly realized, however, that the old, preserved village was not really a village. It was rather just a few old houses scattered along a mountain side. Having walked all the way up, we were a bit disappointed and quickly descended again. At least we had a good view of the valley down below.

![Valley](/valley.jpg)

On the way down, we opted to use the old roads which had existed long before the car roads were built. This provided us with a few interesting views, such as this temple.

![Forest template](/forest_temple.jpg)

I was using my umbrella to shield myself from the sun, but I found that it was also very effecient for shielding off the huge amount of dragonflies on the roads. Aside from dragonflies, we also saw many lizards.

![Lizard](/lizard.jpg)

Back down in the valley, we went to the only restaurant available in the area, with was a soba restaurant. Apparently, this area of Japan was famous for its soba noodles, as we could learn from eavesdropping on the conversation by a japanese family who was also eating there at the same time as us.

I am personally not a big fan of soba, but it was food at least, and compared to other soba I've had, it was actually quite good.

![Soba](/soba.jpg)

We tried to kill time before we could hop on the next bus which would take us to the deserted village. This proved to be hard as there was not much to do or see in the surroundings. The most interesting thing to look at was the river that flowed through the valley. Its water was crystal blue, and the rocks surrounding the river were all tinted blue.

![Blue river](/blue_river.jpg)
![Blue rocks](/blue_rocks.jpg)

As we still had plenty of time to kill, we decided to take a walk to the next bus stop instead, where we would be able to get on the next bus.

During our walk, plenty of cars were passing by, and we frequently had to lean against the mountain wall to give enough space for them to pass by.

Once we reached the bus stop, we sat down and waited for a while before the bus arrived. We had some problems communicating with the bus driver, and as such we ended up skipping our first intended stop. As we passed by the village, we could at least see some of the dolls.

Ths bus stopped at our final destination, the vine bridges, and we got off. There was an admission fee for going down to the bridges which we paid, and then we started descending the wooden stairs down into the forest.

We quickly spotted the two vine bridges, but decided to go down to the river flowing beneath first as it was very beautiful. 

![River beneath the bridges](/river2.jpg)

From down there, we also got a good view of the two bridges.

![Vine bridge 1 from below](/vine_bridge1.jpg)
![Vine bridge 2 from below](/vine_bridge2.jpg)

We walked over the smaller of the two bridges and found ourselves at a small campsite, where we also spotted a sign for a forest path which we decided to follow.

The forest path was absolutely amazing. While there had been plenty of people around the bridges, the forest path was completely devoid of people, and we could walk there in silence and admire the nature surrounding us.

![Forest path 1](/forest_path1.jpg)
![Forest path 2](/forest_path2.jpg)

We had a good view of the river, as the forest path was parallel to it. At one point, the river turned into a beautiful waterfall.

![Waterfall 1](/waterfall1.jpg)

While that waterfall was artificial, there were small, natural waterfalls as well.

![Natural waterfall](/waterfall2.jpg)

On our way back, I was stung by some kind of insect. While the bite was very small, it quickly got swollen and red in an area about as big as a fist.  Luckily, there are very few dangerous insects in Japan, so I was not too bothered by it.

As we got back to the campsite, we headed for the second bridge which we would use to cross the river and get back up to the bus stop. On the way there though, we spotted a second sign pointing towards **another** forest path. We did not have much time unfortunately, so we only walked a little bit, but from what I could see, it was just as beautiful.

![Jungle](/jungle.jpg)

We then crossed the bridge and headed back to the bus stop.

![Vine bridge ahead](/vine_bridge3.jpg)

The bus arrived before too long, and we used it to get to a small village called Kyojo where we would change to a second bus which would take us back to the train station. While the village was small, we figured we might find something to eat there. All of the other possible bus stops were even more remote locations.

There was a small store in the village, and I went there to buy something small to eat while waiting for the bus which would not leave for another 50 minutes or so. The store owner was very friendly, and recommended me a banana bread, which was shaped as a small banana and contained banana inside.

I had to pay with a 10000 yen bill, as that was the only cash I had brough with me. It felt quite silly though, given the small cost of the bread, but the store owner happily accepted it and gave me change.

We then went to wait at the bus stop for the bus to come. The arrangement was quite strange though, as the only visible sign of a bus stop was on the wrong side of the road. I was worried we would not be able to board the bus from there, so I tried asking an old man who was smoking near the bus stop what to do.

In the end, I came to the conclusion that it would be okay to wait at the wrong side of the road, and so we did. When the bus arrived, we waved to it, indicating that we wanted to get on, and the driver gestured to us to get to the other side of the road.

I am not sure whether it was the right way to do it or not, but at least it worked.

The bus ride was pretty much the same as our trip the same morning, except we were going the opposite direction, back to the station. At one bus stop, the bus driver suddenly got off the bus, leaving the doors open and the engines on. He then went to take a smoke. It seems like people here are not very conscious about the environment.

We took the train to Kouchi where we would spend the night. The only reason for that detour was because the hotel availability was good there.

On the train, I noticed a girl sitting on the other side of the car using a portable DVD player, seemingly watching music videos. I did not know those things even existed anymore. It really felt like a blast from the past.

Having checked in to the hotel in Kouchi, we ventured outside again to look for a place to have dinner. By chance, I walked in to a place called Watamin, which seemed to boast a wide menu. I figured there would at least be something on the menu that would satisfy us.

It turned out that this restaurant was really cool. They gave us a tablet loaded with an app where we could browse the menu and place orders, similar to Pinchos in Sweden. The big difference was that we did not have to use our own phones, we could use a tablet form factor which was way more suitable, and the waiters came with the food to our table.

While the concept was really cool and convenient, the food was unfortunately quite mediocre to say the least. It was not bad by any means, but I was not impressed.

This night we spent in separate rooms. All of the twin bed rooms were apparently sold out, and as I did not feel too comfortable sharing a bed, Steven had been kind enough to book separate rooms for us instead.

It felt a bit weird sleeping alone after having spent more than a week together, but it was also quite liberating, and I could listen to audio books as I fell asleep.
