+++
type = "post"
author = "author"
tags = [
  "japan",
  "traveling",
]
date = "2019-08-09T10:14:05+02:00"
title = "Japan 2019: Day 6"
draft = false

+++

# Japan 2019: Day 6

Once again a delayed post. Hopefully I will be able to catch up with this one.

We started our day at 8:00, which was the time we had set the alarm for in order to make it in time for the breakfast included in the hotel reservation. The breakfast was very crowded, and the quality of the breakfast was so-so. It worked good enough to fill us up though, and gave us a good start on the day.

After having finished the breakfast, we headed up to our room to prepare ourselves for departure. I put on makeup and packed my bags, and that was all I had time for before we had to leave. Most hotels here in Japan have a checkout time at 10:00 already, which means that the mornings become quite rushed for us. I've found that one good survival strategy here in this heat is to put up my hair in a bun, as it keeps me way cooler than having it flow over my shoulders, but I did not even have time for that.

Despite leaving at such a relatively early time, it turned out we had missed the train to our intended destination of the day. Since we missed the canyon boat tour last day, we figured we would push it to the next day instead, but due to the late departure from the hotel, we could not get the timetable for the trains to sync well enough. Therefore, we gave up on that idea, and instead decided to go for a boat tour at the coast. The tour was something Steven had looked up beforehand, and was supposed to be very scenic and beautiful.

The trip there involved riding a local train for 40 minutes, and when we came to the destination the time was already past 13:00 and we were both starting to get hungry again.

As we looked around in the surroundings, we could only find restaurants serving raw fish, oysters or beef tongue. The latter being something that this area of Japan is apparently very famous for.
I was a bit hesitant to try beef tongue, but as there did not seem to be any other reasonable alternatives, we went for a nice looking restaurant boasting a full menu with nothing but different dishes made with beef tongue.

Both me and Steven went for a dish with thin slices of beef tongue topped with sesame seeds and some sweet sauce, served over a bowl of rice. I was surprised by how delicious it was, and how tender the beef tongue was. After this experience, I would gladly eat beef tongue again.

![Beef tongue](/beef_tongue.jpg)

We then headed off for the dock to buy tickets for the boat tour. We did not have to wait long since the boat arrived within 10 minutes or so. Having boarded the boat, we headed for the back of the second floor where there were benches for passengers to sit. We figured the wind would help keep us cool during the ride, which was very welcome since the heat was as intense as ever. Since this was a guided tour boat however, there were speakers outside used for informing the passengers of what they were seeing on either side of the boat. As the sound from the engine and the wind was quite loud, this voice had to be even louder in order to overpower it. This quickly became very exhausting. In addition, the place where we were sitting was covered from the wind by the front of the boat, making it very hot out there, and there was a rather strong smell of exhausts from the boat's engine. We therefore moved inside instead where there was air conditioning, with the downside of not getting fresh sea air and not having as good of a look of the surroundings as before.

The tour completed in around 40 minutes, and I don't think either of us was too impressed by it.

![View from the harbour](/boat_tour1.jpg)
![Me and Steven sitting on the second floor](/steven_lova_boat_tour.jpg)

When we came back to Sendai again after the boat tour, we wandered around in the station vicinity for a bit as there was a festival being held in the city. This festival is called Tanabata, and is a traditional festival being held throughout the entire country. However, it's usually being held a month earlier in the rest of Japan. For this festival, people write their wishes on pieces of paper and then tie them up.

There were two long shopping streets fully decorated with beautiful paper crafts hanging from the roofs, and there were so many people that it was difficult to get through.

![Tanabata](/tanabata.jpg)

We managed to pass through the people and get back to the station just in time for the train leaving for Takasaki. The train trip went well, and we arrived late evening in Takasaki and headed for our hotel that was located literally outside the station.

This hotel was a bit strange in that the lobby was only available by first riding the elevator to the 10th floor, but the lobby made a good impression, and the check in process went without any issues. One thing worth mentioning is that I was referred to as "ojouchan", which is kinda hard to translate literally, but could probably best be described as "little lady", and is mostly used for kids as far as I can understand. Some sources on the internet claim that this phrase can be used with adults as well though, if they dress well and look good. Anyway, I take it as a compliment :)

After receiving the key to the room, we asked for recommendations for nearby restaurants, and was recommended to go to one of the upper floors in a nearby buildings. This turned out to be something akin to a food court, with many different restaurants sharing the same roof, albeit without any shared seating.

After having looked around for a bit, we decided to try our luck at a restaurant specializing in dishes made with chicken. Both me and Steven went for "karaage", which is deep fried chicken. It was a set, and as such it came with rice, salad, and the customary miso soup.

This "karaage" was probably the worst thing I've ever had in Japan. Not only do they have a thing for not removing the chicken skin here, but these chicken pieces were also huge and included everything from actual meat to cartilage and tendons. I could not finish my chicken pieces and had to leave them. The rice, salad and soup were enough to fill me up though, and the whole set was extremely cheap, so no big loss.

As we lay in bed that evening for the usual late night chat, we realized something. The train we had booked for the morning after would depart at around 7:00, which felt way too optimistic. We would have had to wake up at around 5:00 to be able to make it in time for the train without having to rush, and neither of us were very keen on doing that.

Steven was about to give up on the plans for tomorrow's trip as well, before I found out that there was another transport option involving a bus. In the end, we decided to go for that option, which meant that we would have to catch a train at around 9:00 instead.

We immediately went to bed in order to get some sleep before we had to wake up at 7:00 the following morning.
