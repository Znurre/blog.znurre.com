+++
draft = false
tags = [
  "japan",
  "traveling",
]
date = "2019-08-05T02:50:11+02:00"
title = "Japan 2019: Day 3"
type = "post"
author = "author"

+++

# Japan 2019: Day 3

After having slept some 6 hours or so, I woke up at around 21:00 in the evening. Luckily, I managed to fall asleep again until around 01:00 in the night, providing me with a total of around 9 hours of sleep which I needed badly.

Today's evening might be a bit rough, having woken up so early, but at least it won't be as bad as yesterday. On the positive side of things, I should be tired enough to fall asleep wihout any problem. I think in the end this tactic might work quite well for adjusting to the timezone difference.

![Chiba at night](/chiba.jpg)

Steven woke up at about the same time as me, and after having spent an hour or so chatting we went to a nearby 7eleven store to buy some instant ramen to eat. In a sense, this became my breakfast for the day.

![Instant ramen](/instant_ramen.jpg)

We then checked out from the hotel and headed for Haneda airport, where we would take domestic flight to Chitose.

The trains were really crowded, and we had to spend nearly 1.5 hours standing on a crowded train where one could barely move, which was not all that pleasant.

Due to the intensity of yesterday's arrival at Narita airport, I had forgotten to take out the shoe box I had brought with me for the purpose of transporting my liquids. As a result, I no longer had any possibility of traveling with checked in luggage on our flight to Chitose.

Fortunately, the rules for carry on luggage are so much more relaxed here in Japan. I was allowed to bring with me all of my liquids, and also my flammable dry shampoo without even any questions.

The flight to Chitose was fast and uneventful, clocking in on around 1.5 hours in total flight time. Worth mentioning is that the plane was unreasonably large. It felt like it was even larger than the plane we used for getting from Helsinki to Narita, and it was completely filled up, which is crazy considering this was a domestic flight. One should not underestimate Japan's population I guess :)

Once at the airport we took the train to Minami-Chitose, a small nearby station, where we would then change trains to go to our intended first destination, Noribetsu.

![Bike shed](/bikeshed.jpg)

While waiting for the train to arrive, we decided to take a stroll in the station surroundings. We stumbled upon a huge mall just a hundred meters outside of the train station, which boasted over 140 stores and a few restaurants. The whole thing felt quite surrealistic, finding a huge mall like that in such a small place. The mall was not really very crowded either, making for quite an apocalyptic feeling.

![The mall](/mall.jpg)
![The mall](/lova_mall.jpg)

Steven had his lunch there, fried chicken, which was apparently not all that good :)

![Steven's lunch](/slunch.jpg)

We then headed back to the station and boarded the train which would take us to Noboribetsu, with our goal set on reaching a place called "Noboribetsu Onsen", famous for an attraction known as "Hell Valley" which is basically a wooden path laid out in a outlandish landscape with volcanic activity.

![Noboribetsu Onsen](/noboribetsu_onsen.jpg)

Unfortunately, Hell Valley was not quite as good as anticipated, but we had a nice walk in the surroundings anyway.


![Hell Valley](/hell_valley.jpg)
![Volcanic lake](/volcanic_lake.jpg)
![Lova in the forest](/lova_forest.jpg)
![Steven in the forest](/slaes_forest.jpg)

After the visit to Hell Valley, we took the bus back to the train station where we jumped on the train that would take us to Hakodate where we were going to spend the night on the bottom floor of a house owned by an older couple.

The train ride was around 2.5 hours on a diesel train, which might have been a first for me. Sitting on a train that sounds like a bus, shift gears when accelerating... now that is a weird feeling :)

![Our room](/room1.jpg)
![The living room](/living_room1.jpg)

Once we had checked in we started a laundry of our dirty clothes and then went for a nearby Izakaya restaurant recommended by the couple in order to have a late dinner.

The dinner was good, although we had to wait for a bit before getting our seats since the restaurant was so crowded. Once seated, we made our order. I ordered grilled chicken with rice and a green salad to go with it, or so I thought. Steven ordered the same, except for the green salad. I got my green salad, which was huge, and finished it all while Steven could only sit and watch. Apparently Izakayas usually implies sharing the dishes, so maybe they waited for us to finish the green salad before delivering the other dishes. 

Anyway, we went back to the house with full stomachs, ready to hit the bed. We actually managed to stay up until 10, which feels like a great achievement! :)
