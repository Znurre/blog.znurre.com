+++
draft = false
date = "2019-08-14T03:33:58+02:00"
title = "Japan 2019: Day 9"
type = "post"
tags = [
  "japan",
  "traveling",
]

+++

# Japan 2019: Day 9

We woke up early in order to take an early train for Onomichi where we would pick up our bikes.

The train trip went without any issues, although the train was jam packed full of people and we did not have any seat reservations. I had to stand all the way between Kyoto and Osaka, and had to eat the breakfast I had brought with me while standing.

We arrived to Onomichi station, where the bike rental store was also located. We walked down to the store and I talked to the man at the reception. Apparently, we were in luck, as it was now possible for us to extend the rental time from one day to two days, allowing us to more or less go with the original plan.

The difference, of course, was that we would not be able to leave the bikes on Shikoku for them to be picked up, but instead have to ride them back to the store. We decided to leave that issue for later. In worst case, we could always take a ferry back, bringing the bikes with us.

After a very thorough explanation of security and how to use the bikes, we finally set off for a small ferry that would carry us and the bikes over to the first of the small islands connecting Honshu and Shikoku.

![Me and our bikes](/bikes.jpg)

The bikes were not very good, but at least it was better than nothing.

The ferry did not take long, and before long, we were on the road with our target set on reaching the place where we would stay for the night. This gave us an aproximate distance of about 40 km to cover by bike, which we figured would be quite doable, despite the heat. 

We stopped at a small platform to drink some liquid and to admire the views, which were really fabulous. Green, fluffy mountains, with large beaches and crystal blue waters.

![Me taking shelter in the shadows](/rest1.jpg)
![View 1](/shikoku1.jpg)
![View 2](/shikoku2.jpg)

It did not take too long before I noticed the first problem. My saddle, which had been adjusted by the store staff, had already started to lose height. Before too long, I was sitting in a very uncomfortable position where I could not put enough strength to the pedals, especially during the climbs.

The bikes had not come with any tools, and as such, there was no way for me to adjust it. I tried asking two people on sport bikes whether they had the tools required to adjust it, but their hex keys were too small.

The roads were mostly located along the coast, making them quite flat and easy to ride, but the islands themselves were connected by bridges, and each time we were to use one of them to get to the next island, we had to make a roughly 60m climb with a slope of about 3%.

![Bridge connecting two islands](/bridge1.jpg)

After one particularily difficult climb to one of the bridges I started feeling the first signs of a sunstroke, but after some rest in the shadow with a cold drink, things felt better again.

![Steven resting](/rest2.jpg)

We continued the bike trip, climbing to the first bridge. This part of Japan is very friendly for cyclists, and there were designated roads for cyclists to use when ascending to and crossing the bridges.

![Bike road](/bike_road.jpg)

Somewhere along the road, I found a place with lots of bikes outside. I figured that they might have tools to adjust my saddle height, and luckily they did. I could finally adjust my saddle to a more suitable height, making the rest of the trip so much more enjoyable.

After around 30 km though, the heat started to really get to me again. The heat was really intense, and my body was telling me that this was a really bad idea. This was despite the fact that I had been drinking more liquid than ever before. According to a quick calculation, I must have had at least 3L of liquid during the bike trip.

We had to keep going though, as we had no other good options to make it to our accomodation for the night.

![Resting in the heat](/heat.jpg)

It did not help that both me and Steven were starting to get really bad sunburns. I was wearing a cardigan to cover my shoulders, but I had exposed my arms as to at least keep them a little bit cooler. Steven was using SPF 50++ sunblock, but given how pale he was to begin with, it did not seem to be enough.

The views continued to be very beautiful though, and especially the view from the bridges were absolutely stunning.

![Views from a bridge](/shikoku3.jpg)

Somehow, we managed to make it to the place where we would spend the night. It was an old, run down japanese inn run by an old couple. They only accepted payment in cash, and both me and Steven had to add together our last money to be able to pay, making us more or less broke.

The inn had a very weird feeling. The layout of the place was very weird, and the old lady who ran the place was walking all around as if she was patrolling the place. Our room was very basic, but it had air conditioning at least.

There was a dinner option available at the inn, but as we were completely out of money we could not pay for that. Instead, we resorted to the option of buying instant ramen available at the inn for a small sum of money, and sat down in the dining room to eat.

The dining room itself was completely empty, and the decoration was very strange.

Neither of us had much change of clothes as this point, so we did a quick laundry and hung the clothes to dry on our room.

On a positive note, the inn was facing the sea, and we had a very beautiful view outside the windows as the sun was about to set.

![Sunset](/sunset.jpg)

When the sun had set completely, we dared go outside again as we could now be certain that we would not make our sunburns worse at least.

![Steven's sunburn](/steven_sunburn.jpg)

We took a walk to a nearby drug store where we bought a spray that was good for curing burns, and then continued to a Lawson store where we both withdrew some more cash as well as buying some light food as neither of us was full from the small instant ramen dinner we had eaten.

On the way there, we noticed a large amount of people heading towards the inn with **a lot** of luggage. Judging by their language, they were french, and especially one of girls in the party was wearing **extremely** revealing clothes that felt more suitable for a photo shoot for a porn magazine than a trip to Japan.

Once back at the inn again, we had a second light dinner at our room compromised of the items we had purchased at Lawson, while we were trying to fend of the french people knocking on our door all the time.

The lady at the inn had informed us that the breakfast would be served already at 7 the morning after, so we went to bed quite early to catch enough sleep.
