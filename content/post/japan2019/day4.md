+++
tags = [
  "japan",
  "traveling",
]
author = "author"
date = "2019-08-06T02:28:08+02:00"
title = "Japan 2019: Day 4"
type = "post"
draft = false

+++

# Japan 2019: Day 4

Today I woke up at 7:00 in the morning from the alarm I had set, after having fallen asleep rather quickly at around 22:00 the previous day.

We decided to set the alarm so early since we had a rather daunting task to finish: eat the food provided by the friendly hosts, in the form of a big slice of melon and macha cake, a cake made of japanese green tea. I could just barely force myself to eat all of it.

![Breakfast table](/breakfast.jpg)
![Macha cake](/macha.jpg)

After checking out we headed towards Mt. Hakodate. Our intention was to hike up the mountain along a forest path which Steven had heard about, but we did not find it. Instead we took the ropeway up. I am usually not too comfortable with taking ropeways due to the height, but this time around it was actually not too bad, and the trip was quite short as well with the mountain being only about 300m in height.

Once we reached the top of the mountain, we stayed there for a while, looking out at the sight of Hakodate stretching out below us.

![View from Mt Hakodate](/view.jpg)

We then managed to find the forest path and decided to descend the mountain using it. The couple whose house we had been staying at had given us a bug repellant spray, as the forest path was supposedly filled with bugs. We did not need to use it though, as the only bugs we saw were butterflies and a few lizards who quickly escaped as soon as they heard us coming. 

![Forest 2](/forest2.jpg)

The forest path was very beautiful, and I was admiring the forest until we stumbled across a sign warning us for a dangerous snake existing in the area, apparently more dangerous than the European viper. From then on I looked very closely on the ground, afraid to step on one.

![Forest 1](/forest1.jpg)

Once we had descended, we walked back to the house where the couple had been kind enough to let us store our luggage during the hike, and after having picked that up and left our goodbyes, we moved on towards the station.

There was still roughly an hour before the train would leave, and so we set out to find somewhere to eat lunch.

We found a wonderful small cafe with an america inspired interior, the walls being clad in DC comic posters among other things. The food was very japanese though, and was simple but very good.

After the lunch we continued on to the train station where we quickly found our train. We were roughly 20 minutes early, but the train was already on the platform, so we made sure to get good seats. After a few minutes, more people started boarding the train, and among them was a rather rough looking guy who tried to hit on me the entire ride. I was quite happy to get off at Shin-hakodate station, where we changed to Shinkansen for Aomori.

The Shinkansen stopped right outside of Aomori, and  to get into the actual city we had  to change to a local train at the station. When going down from the platform, an entire school class was standing there to welcome the people arriving.
They were holding small hand made paper crafts which said "Welcome to Aomori" on the back, and one girl came to me and gave me the one she was holding, which I found extremely cute and flattering.

I made sure to put it on my bag to show it off to the world :)

![Hand craft](/hand_craft.jpg)

The local train for Aomori central station was extremely crowded. There were people standing in the entire train, and up to a hundred people did not make it and had to wait at the platform for the next train.

Once we got to Aomori, we went to the tourist information center to look up information about the festival and the viability of catching a train from Aomori in the evening, as we would be staying in another nearby city due to all the hotels in Aomori being fully booked. The people at the tourist center recommended us to take an earlier train, while the festival was still going, to avoid the most crowds.

The heat was quite intense at this point. I would say this was easily the most humid and hot day so far during our trip. We tried to seek shelter and some place to sit down in the nearby Aomori park, where we spent nearly 3 hours just chatting and looking at all the people dressed up for the festival.

![Aomori park](/aomori_park.jpg)

When it was about 30 minutes left for the festival to start, we went out to survey the area for a good place to stand and watch. We stumbled across a photographer who wanted to take my picture, which I let him do. He gave me his business card and told me to send him an e-mail to receive the photos, but unfortunately I lost the business card, so that won't be an option.

We found a good spot to stand, where we could clearly see the festival street, and stood there for around 20 minutes before the festival was about to begin. We were then offered free seats by a friendly group of people, which we gratefully accepted.

The festival was quite epic, with people banging on drums, playing the flute and people dancing and singing. There were also several hand made floats which depicted creatures from Japanese mythology, which were carried around by people.

![Festival 1](/festival2.jpg)
![Festival 2](/festival3.jpg)

There was a huge amount of people watching the show, to the point where it was difficult to get around.

We watched the festival for about 1 hour before leaving for the station, as we wanted to be there in good time for the train to leave, seeing as it would likely be very crowded by people going home from the festival.

Since we managed to get to the train in such a good time, we managed to get seats, which was very nice since the trains became crowded as expected, with lots of people having to stand.

It was quite late when we reached our final destination for the day, Hachinohe, where we would stay at a hotel. Steven had told me it would be roughly a 3 km walk to the hotel, which I was prepared for. However, it turned out he had made some kind of mistake when estimating the distance, and the real distance was closer to 6 km.

As we approached the address of the hotel, we saw a hotel in the distance, which we instinctively assumed was our hotel. It did not seem very reasonable that there would be another hotel there since the location was quite far from any major attraction.
We quickly became aware that it was the wrong hotel though, as they did not find any reservation in Steven's name, and so when we double checked the hotel name, we realized we had went to the wrong hotel.

At first we were afraid that we might have entered the wrong hotel name in Google Maps, and thus having walked to a completely different place, but quick research showed that we were very close to our real destination.

Just slightly beyond the hotel we had arrived to was another, smaller and quite bland looking hotel. We entered the lobby and checked in, and was surprised by how pricy it was. Roughly 1500 SEK for the two of us for one night. We figured that maybe it was quite a high standard hotel afterall, despite the shady exterior. However, that illusion was quickly shattered as we entered the room.

While the room was relatively clean, lots of things were either broken or did not work properly, the interior was old and patched in several places, and the whole place pretty much gave off a rather sloppy feeling.
Looking to Booking.com, it turned out that this hotel was usually priced at a whoppingly low 370 SEK per night for two people, which felt like a more reasonable price given the standard. Most likely they had used the fact that room availability in the entire area was so low due to the festival, and increased their prices as a result.

![Sloppy hotel](/sunkhotell.jpg)

We finally fell asleep sometime after midnight after a very long day.
