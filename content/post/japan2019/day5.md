+++
type = "post"
title = "Japan 2019: Day 5"
draft = false
author = "author"
tags = [
  "japan",
  "traveling",
]
date = "2019-08-07T13:08:14+02:00"

+++

# Japan 2019: Day 5

Once again, sorry for the late post! I feel like I am falling behind my schedule a bit with these posts, but it's actually surprisingly hard to get the time required to write them, especially during days such as today as you will soon get to see.

We woke up to the alarm we had set at 08:00 and headed straight for the breakfast that was included in the hotel price. Unsurprisingly, the breakfast held about the same quality as the rest of the hotel, which meant the selection of food was very limited, and what was available held very low quality.

After having finished breakfast, we headed out with our aim set for a park shown on the map. The plan was to go to the park, and maybe rest there for a while and hide from the heat, and then continue down to a river we had spotted the evening before when walking to the hotel. Due to the darkness, we had not been able to get a good look at it, but it looked like a nice place with a lot of greenery.
We would finally make a stop at a small station near the river and then go to the central station of Hachinohe where we would change to Shinkansen for our next stop.

On the way to the park, we actually found another, elevated park where one could get quite a good view of the city.

![Steven is putting on some sunscren](/slaesh_sunscreen.jpg)
![Overlook from the park](/overlook.jpg)

We rested there for a bit before continuing on to the next park, which was our real destination. It was extremely hot and humid as usual. It feels as if it gets warmer and more humid for every day.

A few minutes later we reached the park and we realized it was not much more than some kind of arena, a swimming pool and a parking lot.
There was however a small grass field, with a few places to sit down and rest.

![Park-ing lot](/park-ing_lot.jpg)
![Park](/park.jpg)

After leaving the park, we started walking towards the small station I mentioned earlier and stopped by the river on the way there. It turned out to not be quite as beautiful as we had imagined, so we quickly moved on.

![River](/river.jpg)

The small station only had one platform, which probably made it the smallest train station I've been to so far in Japan.

It was still about 30 minutes before the train would arrive, so we could only sit and wait. Luckily we could sit partially in the shadow, making the wait almost endurable. Even so, I could feel the sweat running on my back as I sat there.

![Station](/station1.jpg)

The train arrived and we both stepped on board. Only a few minutes into the ride I realized something. I had forgotten my camera at the station. There was not much to do. The next stop was the final destination, and we had at least 5 minutes until then. When approaching the station, the train stopped to make way for a train going the opposite direction. This meant our option of going back by train was now gone.

As soon as the train had arrived at our destination, we hurried out to the front of the station, and I took a taxi while Steven stayed at the station looking after our luggage.
The taxi ride was quite long, and once we got back to the station the taxi driver waited for me, while I ran to check the bench where we had been seated.
I was very relieved to find that the camera was still there, and I picked it up and returned to the taxi which brought me back to the station.

Due to this incident, we didn't have too much time to spare before our train would leave, but I still had time to pick up an "ekiben", or lunch box that you buy at the train station to eat on the train. It turned out to be really good, as it was freshly made and its content was still warm.

![Lova eating "ekiben"](/ekiben.jpg)

When we arrived at the next station, we still had plenty of time for the next train. The heat was really extreme, and so we took shelter in one of the waiting rooms on the platform where our train would depart from. Or so we thought. We sat in the waiting room for quite some time, when finally a train arrived on the platform, seemingly in very good time for the departure, but with the wrong number and destination.

Steven said we would wait for the number and destination to change, as it was still in the process of being cleaned up, so we waited until a few minutes before the departure time specified on our tickets. When the number and destination for the train still had not been updated when there was only 2 minutes left we started to panic. When double checking the time for the departure of the platform, we realized it was set an hour in the future. We had been waiting at the wrong track, and could only stand and watch as the train we were supposed to go with left from another platform.

This completely messed up our plans for the day. We had intended to stop at a station on the way to Sendai, and take a small detour for a boat ride along a river located in a canyon. Instead, we had to cancel these plans and instead go directly to Sendai where we would spend the night.

We finally arrived at our destination, Sendai, and changed to the subway to get to the hotel, which was located in the outskirts of the city. Despite taking the subway, we had to walk for quite a bit before reaching our destination, and the heat and my heavy luggage made it a very uncomfortable trip.

The checkin at the hotel was smooth, and getting to shower was such a relief. My body was so sticky in most places due to the humid and the heat.

After showering, I went down to the lobby to book a 1 hour full body massage which was available at the hotel. My body was aching from carrying all this heavy luggage, and especially my shoulders and back, so I figured a nice massage would do the trick.

When attempting to go back up to the room again, we realized neither of us had brought the key card with us, effectively resulting in us now being locked out of the room. Luckily, the reception issued new key cards for us without any questions. Even so, it was very embarrassing to say the least.

We went for dinner at the hotel restaurant at 19:30, and both of us ordered what is called "hamburg" in Japan, which is a rather strange dish composed of a hamburger served without bread, usually together with rice and some kind of sauce, in this case a sauce made of mustard and mushrooms. It came with salad and miso soup as is customary, and was very good in my opinion, although I am not sure whether Steven would agree :)

After the dinner, Steven went down to the lobby floor to take care of our laundry, while I prepared for the massage. When the "sensei" who was carrying out the massage came to the room, I was surprised by how big he was. Usually japanese people are very slim and slender, but this man was far from it. He looked very kind though, and had a very calm and soothing voice. He instructed me how to lay down on the bed, and then he begun the massage, which he told me was called "relaxation", and that it differed from regular massage in that it was not intended for healing. Even so, I felt like this "relaxation" was one of the best massages I've ever had, and it did wonders for my stiffness.

We went to bed at around 22:30, but spent at least 1 hour chatting about various things, before finally falling asleep around midnight.
