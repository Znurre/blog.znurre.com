+++
date = "2019-08-20T05:45:18+02:00"
type = "post"
draft = false
tags = [
  "japan",
  "traveling",
]
title = "Japan 2019: Day 17"

+++

# Japan 2019: Day 17

I woke up around an hour before the alarm, having slept very bad the entire night. Once the alarm had gone off at 8, we started preparing to leave, and then checked out from the ryokan.

We went to buy breakfast at a Familymart store on the other side of the road, and then took a walk to find a place to eat it. A look at the map revealed a nearby park, so we started walking there.

It seems like Japan has a thing in general for elevated parks, as this one was yet another elevated park, giving us a great view of the onsen town of Kinosaki while eating our breakfast.

![Breakfast in the park](/kinosaki3.jpg)
![Kinosaki-onsen from above](/kinosaki2.jpg)

After finishing our breakfast, we descended again and started walking towards the station. I ordered a freshly made ekiben at a store near the station, and while waiting for it to be prepared we went to reserve seats for the train we would take to Osaka, where we would spend one night before going back to Tokyo.

The reserved seat tickets we received were completely in japanese, without the usual romanized station names. I am not sure if the guy behind the counter simply forgot to activate it, or if my japanese was good enough that he figured I would not need it. I'd like to think it was the latter :)

I picked up my ekiben that was now prepared, and we then spent around an hour waiting at the station for our train to leave, which gave me an opportunity to write some more on the blog.

The train ride to Osaka was quite long but uneventful, and when we reached our hotel it was still too early to check in, as the check in time was from 16:00. It's quite crazy how late the check in times are here in Japan.

We left our luggage at the hotel and went for an early dinner. Our first attempt was a small cafe just outside the hotel, but as soon as we stepped into the cafe a thick cloud of cigarette smoke made us aware that this place allowed smoking inside, so we quickly stepped outside again.

Our next attempt was the tried and tested place where we had had both dinner and breakfast previously, and this time we had better luck. They seem to be doing business all day.

We both ordered a dish with ginger flavored stir fried pork and vegetables, served with rice of course. It was not bad at all.

During our dinner, the rain had started falling outside, and as we had left our bags at the hotel we did not have any umbrellas with us. We stood and waited for a while, but the rain did not show any sign of stopping soon, and the weather forecast forecasted at least one hour of rain.

In the end, we decided to run all the way to the hotel in the rain. By the time we reached the hotel, we were quite drenched in water.

After spending some time at the hotel, we decided to go check out a bar that would open at 19:00 in the evening, offering retro games among other things. The walk there was quite far, and we got to see quite a bit of Osaka's night life on our way there. Osaka really has way more of a big city feeling than Tokyo in my opinion.

We reached the bar 3 minutes after its opening, so we were the only customers there at the time. I ordered a Mtn Dew, and Steven followed my example. We then went ahead to browse the available games, and tried out a few of them.

In the end, we went for a Steam powered PC that they had available with a few multiplayer games. I managed to beat Steven quite badly at Puyo Puyo Tetris, but he gave me a real challenge in Towerfall. I won the last match though, which is all that matters.

We stayed there for roughly 40 minutes before walking back to the hotel again, this time along the river, which provided some beautiful night sceneries.

I had a hard time falling asleep that night, mostly because of the Mtn Dew, and also because I was hungry as a result of the early dinner we had eaten.
