+++
date = "2019-08-19T04:38:03+02:00"
type = "post"
draft = false
tags = [
  "japan",
  "traveling",
]
title = "Japan 2019: Day 15"

+++

# Japan 2019: Day 15

This morning we woke up at 8 by the alarm, as we had decided to stick to our original plan for the remaining days as the typhoon had now left the country without leaving too much devastation behind. We therefore headed towards Tottori by means of a railway passing through the mountains.

While waiting for the train, I sat down at the station to eat a breakfast which consisted of an ekiben with thin fried slices of meat and rice.

The first train was an old, local, 2 car diesel train which we rode all the way to a city called Tsuyama. As the city was located up in the mountains, the air was quite fresh and easy to breathe, and even though the heat was really scorching as usual, the humidity was at least not so bad.

Our objective here was a museum that Steven wanted to visit, dedicated to the introduction of western science to Japan by means of trade with Holland way back in time, and Tsuyama happened to be the center of that exchange.

The walk to the museum was quite long, but at least we had the chance to look at the beautiful surroundings in the shape of lush riverbanks and old houses while walking there. 

![Riverbank](/river3.jpg)
![Tsuyama 1](/tsuyama.jpg)
![Tsuyama 2](/tsuyama2.jpg)

The museum turned out to only have text in japanese, and as such it was a bit difficult for me at least to enjoy it. We stayed there for maybe 30 minutes before we started walking back towards the station again.

On the way back, we stopped by a cafe to have lunch. I did not really know what to expect, but from the menu outside I could at least make out 2 possible dishes I could consider eating.

The cafe turned out to be very cosy and nice, combining old with new as the house was old, while the interior felt very modern and bright.

![Cafe 1](/cafe1.jpg)
![Cafe 2](/cafe2.jpg)

I sat down to look at the menu at the table, but could not find the dishes mentioned on the sign. When the waitress came to pick up our orders I inquired about this, and was told that the menu was only for the dinner, and that the dishes available for the lunch were only written on a chalkboard.

She then went on to explain the different options, and I settled for some dish which I understood to be chicken breast fillet of some kind, while Steven went for a curry flavored yakiniku beef meat option.

Both dishes were to be served with rice, salad and soup, but none of us had expected that we would also be served small onigiri in quite large quantities. I tried my best to eat as much as I could of them before the rest of the food was delivered.

![Onigiri](/onigiri.jpg)

The lunch eventually arrived, and it consisted of many small dishes in addition to the primary dish we had both selected.

![Lova's dish](/dish1.jpg)
![Steven's dish](/dish2.jpg)

I can't say the food was great, but at least it was good enough, and definitely enough quantity to fill us both up.

After the lunch, we walked back to the train station where we took another local train for our next stop on the way, which would be a small village named Chizu.

This village was apparently listed as one of the most beautiful villages of Japan, and Steven had been there before and wanted to have another look at an old temple located in the outskirts of the village.

The train there was only one car, a so called "one man" train, which felt more like a bus, with the driver handling everything on the train.

We eventually arrived to Chizu, and had a stroll in the surroundings before finally arriving to the temple.

![Outside the temple](/temple1.jpg)

After this small excursion, we once again went back to the train station to continue our journey for the final stop of the day, which would be a larger coastal town of Honshu named Tottori.

Having checked in to the hotel, we decided to go have dinner in the restaurant of the hotel.

The dinner was simple but quite good. I was probably way too tired at this point though, as I kept laughing uncontrollably at everything, including Steven's attempts to pay for the dinner :)

We went to sleep sometime at midnight, which would give us roughly 8 hours of sleep since we had set the alarm at 8 for the next morning.
