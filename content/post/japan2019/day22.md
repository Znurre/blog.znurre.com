+++
type = "post"
title = "Japan 2019: Day 22"
tags = [
  "japan",
  "traveling",
]
date = "2019-08-30T09:05:50+02:00"
draft = false

+++

# Japan 2019: Day 22

And so we woke up to the last proper day in Tokyo and Japan.

Despite being very tired of Japan by now, it felt quite strange and a bit sad to soon be going back home again.

We did not really have any plans for this last day, so we decided to try our luck at some more shopping for items that I had not managed to get my hands on yet. More specifically, I was interested in CDs by DJ Sharpnel and MOSAIC.WAV.

Our first stop was Akihabara, where I figured I would have best luck. Before going there though, we first checked out from the hotel and left our bags in their luggage storage, as to not have to carry them around with us the whole day. Once in Akihabara, we first went to Toranoana, where I asked a guy working there whether they had any CDs by DJ Sharpnel. After looking at the computer for a while, he finally returned with a negative response. He did, however, give me a map where he circled two other stores which might have the CDs. As always, the service here in Japan is spotless. I then asked him whether they had any MOSAIC.WAV CDs, whereupon he once again turned to the computer. 

It turned out they had one CD by them, but he was unsure exactly where it would be located. He knew for sure it was not in the current store, but he said it could be available in either of two other Toranoana stores in Akihabara, one of which was located in the house next door. As that store happened to be closest, we headed there right away, and I caught the first available staff person and inquired about the availability of this CD.

This staff person once again turned to a computer available in the store, and after waiting for what felt like 5 minutes while he looked up information and made phone calls, he finally returned to us saying that the CD should be available on the third floor, and he was kind enough to go with us up there to show where it was. 

Once up there, however, things quickly became even more complicated. He tried to locate the CD, but without luck, and it did not take long before he had also involved a colleague in the search. They walked all around the place, checking hidden drawers and going through the bookshelves in search of the CD, and after another 15 minutes or so, a third person was involved.

I started regretting even asking about the CD, but seeing as they had already put so much work into finding the CD for me, I figured I could not exactly tell them that I was not interested anymore.

Finally, after 20 minutes or so, the first staff person came back to me, telling me that they had been wrong all along, and that the CD was really on the basement floor. He had me go down there alone though. To this day, I still don't know if that was a strategy of his to get rid of this bothersome customer, or if he did not feel comfortable going down there considering the material available there.

As we stepped onto the basement floor, we quickly realized that this was the floor for 18+ material, and needless to say, I did not feel all too comfortable walking around down there. We also saw no sign of CDs, and I was about to give up, when finally Steven spotted a shelf with a few CDs in a corner.

We headed there, and I actually found the CD that the staff person had been talking about. I soon realized why it was located on this floor, however, as it turned out to be the OST to some kind of erotic game, and there was no mention of MOSAIC.WAV at all.

Disappointed, we headed out from the store towards the two other locations as circled on the map by the staff of the first Toranoana store.

The first one was LASHINBANG, which apparently is a famous store. Personally I've previously always been looking for new goods, so I had not set my foot there until now. It turned out to be a gold mine for me though, as I found tons of MOSAIC.WAV CDs, both new ones and older ones. Definitely a place to visit again if I ever go back to Tokyo. They did not have any DJ Sharpnel CDs, however, so my search continued.

The next stop was Mandarake, another new experience for me. Mandarake is a tall building with several floors, each one dedicated to a different medium. After some searching, we found the right floor for doujin music CDs, and I asked if they had any DJ Sharpnel CDs, which they unfortunately did not.

I did, however, find tons of older IOSYS CD releases, some of which I purchased. IOSYS was the circle that introduced me to japanese doujin music in the first place around 2008-2009 or so, so having some of those old CDs in physical form felt like reason enough to warrant a purchase, especially given the low price. 

As I was still without any DJ Sharpnel CDs, our search continued. Steven mentioned Tower Records in Shibuya as being one of the largest music stores in Japan, so we decided to go there as we did not have anything else planned for the rest of the day anyway.

Since we were in Akihabara, we decided to go to Shibuya by the Yamanote line, which came with the benefit of letting Steven use his JR pass. I had left mine at the hotel for whatever reason, so I had to pay with my Suica card. This lead to us taking different entrances for the train platforms, since I could use the usual ticket gate, while Steven had to go through the manual one. We were close to getting separated, as I was used to us sticking together, paying no attention to what Steven was doing. Luckily I quickly realized that he was not behind me, and we could meet up and go to the train together.

We spent more than 20 minutes on the train for Shibuya, and when we got off the train we were unsure which exit to use to get to Tower Records. After discussing back and forth for a while, we settled for an exit, and headed for the ticket gates. Once again, I realized that we had been separated, but this time around there were too many people for me to even see Steven, and so I figured I would wait outside the station instead.

Shibuya station is like a maze, and after walking for a while I finally found myself outside, but completely dislocated. To make things worse, I had no internet connection on my phone anymore. I had been leeching on Steven's WiFi the last days in Tokyo since the data pack I had purchased was limited to 14 days of usage, but now I was outside of its range.

Without internet connection I could not bring up a map, and there was no public map nearby. I waited for a while, thinking that Steven would show up, but when he didn't, I started walking in what I thought would be the right direction. After a little while, I reached a public phone booth, and noticed that there was a public WiFi network available. Apparently, all phone booths in Tokyo come with a public WiFi spot for people to use. Very convenient!

Having brought up Google Maps, I could finally see my position, and realized that I was completely off. I tried to send an e-mail to Steven, explaining the situation, but my phone refused to send it for whatever reason.

I assesed the situation, and came to the conclusion that the easiest way to find each other would be to meet up at the place we both knew we would be going to, and hoped that Steven would realize this as well. With that in mind, I started heading towards Tower Records. I had hoped to find a public WiFi on my way there in order to send Steven an e-mail, but I did not have much luck. Even if there might have been a few open, public networks, they were all drenched in the pollution that is Shibuya's WiFi airspace. Hundreds of WiFi networks kept popping up and disappearing in a matter of split seconds, rendering any attempt to connect to either of them futile.

It did not take long before I arrived at Tower Records, and I could quickly confirm that Steven was not there. I once again tried to find an open WiFi network, and after a lot of trouble, I managed to connect to the Tower Records WiFi network. My mail client still did not want to work for whatever reason, but in the end I managed to use the GMail mobile web interface to send an e-mail to Steven. A few minutes later I received a reply from him confirming that he had receive the message and was now on his way to Tower Records.

As soon as he arrived, my priority was food. This entire ordeal had used close to an hour of time, and I was starving. There was a cafe inside Tower Records, and I headed there in a brisk pace without even looking for alternatives, since I just wanted something to eat in that instant.

We found our way to the cafe, but had to wait before being let inside, as the cafe was very crowded. A few minutes later we were seated in very strange chairs, and presented with the menu, which was very limited in the food department to say the least. There were two items on the menu which would constitute as food, the rest was sweets. We quickly decided to take one of each. I would go for some kind of fish and tomato pasta, while Steven would go for the second dish, some kind of curry.

It took what felt like close to 10 minutes before someone came to pick up our order, and then it took around 30 minutes more to get our food. At this point my stomach was numb of hunger, so I didn't really have a problem with waiting, but the constant k-pop playing as background music certainly didn't make the wait very enjoyable. The walls were covered with signed k-pop artist posters, and I generally felt very out of place.

Once we received the food, we could quickly determine that neither of us was too fond of it, but my hunger had me finish my dish in just a couple of minutes.

Having finished the food, we paid and headed for one of the floors where we figured I might be able to find DJ Sharpnel CDs. I found a staff to ask for assistance, and who tried to find the CDs for me, but in the end she had no luck. It seems very difficult to buy DJ Sharpnel CDs in Japan.

Without any other errands left to do in Shibuya, we took the subway back to the hotel to pick up our bags, and then rode Narita Express to Narita as we would spend the night at a hotel close to the airport. 

Before going to the hotel though, the idea was to pick up my bag that we had stored at the airport. This would allow us to prepare our bags already in the evening, instead of having to do it at the airport in the morning the next day where we would have limited time. 

The pickup of the bag went without issues, and we continued on to the bus stops outside of Narita to wait for the next shuttle bus to our hotel. 

After waiting for roughly 20 minutes, the bus arrived, and we were brought to our hotel where we could check in.

We left our bags on the room, and then went to have dinner at one of the hotel's two restaurants. My preference was the Chinese restaurant available at the hotel, but that one was full, so we had to settle for the Japanese restaurant instead.

The service at the restaurant was mediocre, and the food was expensive. I ordered something with fish, while Steven went for a meat dish.

After some time waiting, we received our dinner, and I was not too impressed by my dish. It was neither much food, nor all that good. However, Steven was kind enough to let me have a few bites of his dish, and damn was it good. The meat was really tender and tasty, and it came with various fried vegetables and mushrooms. 

After that, I just had to order one for myself as well, and in the end our last dinner turned out quite expensive, but it felt like we ended our trip on a high note.
