+++
date = "2019-08-14T04:48:11+02:00"
title = "Japan 2019: Day 10"
type = "post"
draft = false
tags = [
  "japan",
  "traveling",
]

+++

# Japan 2019: Day 10

This day started in a rather hectic way, as we were more or less woken up by the old lady telling us to come down to eat breakfast. It turned out we had overslept by more than 30 minutes, and the reason was my phone which had apparently not accepted any charge during the night.

We hurried down to the breakfast, which was **huge**. We were served bacon and scrambled eggs, toast, vanilla yoghurt, green salad, potato salad, and an entire cooked fish, among other things. This gave us a good start on the day at least.

After having finished our breakfast, we went back to our rooms to pack our things. I decided to skip makeup, as we wanted to leave as soon as possible, before the heat became too bad. Our goal was to go back the same way we had come the day before, but stop about halfway to take a ferry back to Onomichi.

We wished to get there before noon, as that is when the heat reaches its peak. I therefore set off at a brisk pace, keen to reach our goal as soon as possible. Unfortunately, Steven was completely out of energy and could not keep up.

I stopped at a vending machine to buy some water and to wait for him. Unfortunately, when I had withdrawed money from the Lawson store the day before, I had only received two 10000 yen bills. Those bills can be compared to 1000 SEK bills, and the vending machines do not accept them. So, even though I had money, I could not actually use it.

An old lady walked past the vending machine, and I asked her if she could possibly change my 10000 yen bill for 1000 yen bills, as I wanted to buy something to drink. She did not have enough change, but she offered to treat me to something to drink. People here in Japan are so nice.

Steven came only a few minutes later. He had fallen from his bike and hurt his arm to the point where he was bleeding. We cleaned the wound and used some of the spray we had purchased for our sunburns, since it contained alcohol. I figured it might at least help sterilize the wound a little bit.

The rest of the trip went well, and we stopped by at a beach near the point where the ferry left from to take a long overdue swim in the ocean.

![Beach](/beach.jpg)

This was, by the way, the first time Steven took a swim in 10 years according to himself.

The water was cool and soothing, and had it not been for our goal to make it to the ferry before too long, I would have liked to stay there for the rest of the day.

Steven's sunburn kept getting worse despite his attempts to protect it with sunblock. His skin was red, and he had started getting blisters as well.

![Steven on the bridge](/steven_bridge.jpg)

We made it to the ferry just in time. Had we arrived just 20 minutes later, we would have had to wait for 3 hours. The ferry trip was quick and comfortable, with air conditioning and comfortable seats.

When we arrived to Onomichi, we immediately went to return the bikes, and then took the first train towards our first destination, Okayama, where we would change trains for a small town named Ikeda where we would spend the night.

The train we selected was a Kodama train, the slowest Shinkansen service. It was using an older Shinkansen train type that I had not used before, and it stopped at one station for a full 12 minutes before finally leaving for Okayama.

In Okayama, we had quite some time to kill before changing trains for Ikeda. We decided to go for lunch, and found a restaurant on a shopping street serving Hamburg steak dishes. We both went for a set menu with chorizo sausages on top of an hamburg steak, served with rice, salad and soup.

The food was good, and the place was quite cosy and casual, however the music was horrible.

Before leaving the place, I used the opportunity to go to the restroom, and found that I had brought with me quite a lot of sand from the beach which was still stuck in my bikini bottoms that I was still wearing underneath my clothes. I wonder what the person in charge of cleaning the toilets will think :)

On the train to Ikeda, Steven had a moment of panic as he thought he had lost his phone on the station. Luckily we quickly found it on his seat, where it had ended up after sliding out of his pocket.

I went to the toilet on the train, and was surprised to find that it was a traditional japanese squat toilet, requiring me to squat over the toilet as the train was twisting and turning. That was quite an experience.

Having arrived at Ikeda, we started walking to our accomodation for the night. We did not know what to expect from it. The ratings on booking.com had not been fantastic, the score being somewhere between 7 and 8. There were also no pictures available, and only very few reviews had been written, leaving us completely in the dark.

Luckily, the place turned out to be quite good. The receptionist was a young guy who spoke decent english, and who was very friendly and easy to chat with. We received the keys to what was more or less our own apartment, with a single room, private bathroom and our own private entrance.

I took a shower and put on makeup. It felt so good to get rid of the wet bikini I had been wearing underneath my clothes, and to get the stuck salt from the salt water off my body.

The hotel where we were staying also featured a restaurant and a cafe, and I was in dire need for something cold to drink, so I suggested we go there. The cafe was run by the same guy that helped us check in. It turned out we were a bit early, as the cafe would not be open for another 5 minutes, but he let us sit there while he was preparing its opening.

We both ordered an orange juice, and I ordered a chocolate cake while Steven ordered a chocolate crepe.

The hipster vibes of the place increased further as the guy proudly told us that the cake was baked with tofu, and therefore was completely vegan. I am not sure whether that was a good thing or not. Personally I did not find it all that good at least.

Since it was still quite early, we figured we would take a stroll in the surroundings. We were recommended a few places, and went for a temple that seemed to lie in the outskirts of the town. Getting up there was quite a climb, but we got a very good view over the town at least.

![View over Ikeda](/ikeda1.jpg)

When we were to descend again, the sun had already set and we had to walk down in the dark.

![Steven in the dark](/ikeda2.jpg)

It was starting to get late, and I was getting hungry again. The map we had received from the guy at the hotel had a list of restaurants, and we tried to decide which one to go to.

The one we had first intended to go to was completely full, so we kept walking in search for another one. We found a restaurant which was almost empty, which we later noticed was part of another hotel. Their menu was quite interesting. For instance, they served deer, hunted in a valley close to the town. This was the first time I have seen deer on the menu in a japanese restaurant, so of course I just had to try it.

It came in the form of a stew in red wine sauce, served with rice of course, and was quite good. Steven ordered dumplings, which were also quite decent.

Another interesting detail of the restaurant was one table which had 4 JR express train seats instead of normal chairs. Upon asking where they had got them from, I was told that they had been given to this hotel by JR themselves. I later also noticed advertisements inside the JR trains promoting this very hotel.

Back at our room, we spent quite some time trying to find the air conditioner remote which we had lost somewhere. It took us an unreasonably long time before we found it under one of the beds.

We went to bed after the usual late night chat.
