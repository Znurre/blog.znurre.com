+++
tags = [
  "japan",
  "traveling",
]
title = "Japan 2019: Day 13"
type = "post"
draft = false
date = "2019-08-18T11:28:11+02:00"

+++

# Japan 2019: Day 13

We woke up at 9 or so, without an alarm this time as we did not have a train to catch, nor a check out time to adhere to.

My breakfast consisted of melonpan, yoghurt and Coca Cola Energy Cola from 7eleven, same as more or less each morning so far.

As this stay in Osaka was completely unplanned, we did not have any real plans for the day. We therefore ventured out to a part of Osaka known as Nipponbash. This area of Osaka can be compared to Akihabara in Tokyo in that it is centered around Japanese popular culture such as anime, manga, CDs, DVDs, figurines, games and so on.

I went to Toranoana and Melonbooks where I found 4 CDs that I purchased, of which two of them had been on my wish list to buy when going to Japan. We also checked out some other shops, for example an electronics shop where Steven purchased some components for a project he is working on.

After this, we went back to the hotel to drop off our purchased goods, and headed back almost in the same direction. The goal was another popular district of Osaka known for its shopping streets. I wanted to buy some clothes among other things as only having one real outfit with me was starting to get tiresome.

Before going shopping though, we made sure to get ourselves some lunch. I had intended to introduce Steven to Ichiran in Tokyo, a restaurant serving delicious tonkotsu ramen. It turned out, however, that they had a restaurant in Osaka as well, so I figured we could as well go check it out while in Osaka instead.

We had to queue for maybe 20 minutes or so before getting our seats. The noodles were delicious as always, maybe a bit worse than what I've had at their restaurant in Tokyo though.

With some food in our stomachs we then went for one of the largest shopping street in Osaka. We went into some stores, but the problem with being a foreigner in Japan is of course that all of the local brands do not have your sizes. It's always possible to buy items from large fast fashion stores such as H&M, UNIQLO, etc, but I don't really see the point of that as I could as well do it in Sweden.

In the end, I did purchase the new edition of Levi's perfect tee, which I had already intended to purchase since some time ago, but which had not been available in stores in my city.

We then walked back to the hotel again, where we spent some time before heading off for dinner. A walk in the surroundings did not reveal too many interesting restaurants, so in the end we went for a somewhat generic looking restaurant. Turned out it was part of a chain, but the food was absolutely acceptable.

Of course, after having finished my meal, I noticed advertising for another dish I would have loved to try had I known it was available. I set my mind on going back there the next day to order it.

The typhoon had started showing its signs in Osaka as well, bringing in heavy rain over the city. We used this opportunity to record a greeting for my friend Björn's wedding, which would take place the coming saturday. This trip of ours had unfortunately been planned and decided way ahead of the wedding invitation, which made me unable to attend.

This turned into yet another late evening, as we did not have to set the alarm for the next day. In a sense, this felt like the first real day of vacation. Although traveling around and doing activities is fun, not having a place to return to every night and spending most of the days going from place to place is quite taxing.
