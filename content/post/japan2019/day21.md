+++
draft = false
type = "post"
tags = [
  "japan",
  "traveling",
]
date = "2019-08-28T08:59:36+02:00"
title = "Japan 2019: Day 21"

+++

# Japan 2019: Day 21

We woke up by the alarm this morning, which we had set for 10 as we had booked the yukata rental for 12 o'clock.

I spent even more time than usual on my makeup this morning, as I wanted to look the best I could. Today was a very special day for me afterall. I have always looked up to all the beautiful japanese women wearing yukata, and today I would get to be one of them.

After the preparations, we headed to 7eleven as usual for our breakfast. I also made sure to go to the toilet, as I was unsure how easy it would be to go to toilet once I was wearing the yukata.

As we approached the rental store, I was filled with anticipation and nervosity.

The store manager was very friendly, and spoke at least a bit of english. We got to choose the patterns we wanted, both for the actual yukata and the obi, and then she and her assistant helped us put on the yukata. She also put up my hair in a very cute style, with flower ornaments.

I don't think I've ever felt this pretty before.

![Me in yukata](/yukata.jpg)

When walking outside the store, I was very anxious about what people would think. While yukata is quite a common sight in these areas of Tokyo, it's not all that common to see foreigners wear them, and especially not someone as tall as me.

We decided to go to our hotel room first, getting the camera and our umbrellas among other things, and the first thing that happened on our way there was that we had our photograph taken by a man with a camera. We did not get all that many strange looks though, and I was slowly getting more comfortable.

One thing we both realized early on was how **extremely hard** it was to walk properly with a gown reaching all the way to your ankles, and the geta sandals on top of that. Our walking speed was severely limited, to the point where we did not dare pass over a road unless the traffic light had just changed to green.

We decided to take a stroll in the more touristy parts of Asakusa, the part of Tokyo where we were staying. Not only would we blend in better there, we also figured that we would be able to take some nice pictures with the old buildings as a background.

Those streets were so full of tourists though that it was hard to pass through properly, even less take a picture with only us in the picture.

In the end, we decided to go for a walk along the Sumida river flowing through a large part of Tokyo. This path was much less crowded, and we did not meet many people at all.

Unfortunately, this path was not as scenic as one might expect from a path following a river, and it was mostly just concrete.

One thing we quickly realized as well, was how warm the yukata was. Yukata is the cotton variation of the traditional kimono, and is usually worn in the summers as it's much cooler. Even so, compared to my usual attire it was unreasonably hot. The heat together with the difficulty of walking made us take several stops along the way.

![Lova and Steven wearing yukata](/lova_steven_yukata.jpg)

Having walked for what felt like close to an hour, we decided to try and head for a cafe for something to drink. This proved to be more difficult than expected, since the part of Asakusa where we had ended up after our walk was more of a residential area without many shops, restaurants or cafes.

Steven brought up some options using Google Maps on his phone, and we started walking towards one a few km away which was supposed to be open. Unfortunately, we soon realized Google was wrong, as when we got there it was closed.

I started heading in the direction of the more touristy parts of Asakusa with the intention of maybe finding something on the way there. One tricky thing with Google Maps is that it does not list all cafes and restaurants, especially smaller ones, so sometimes the only way of finding a restaurant or cafe is by walking around and looking for signs.

This worked in our favor, and we quickly found a small cafe. While the selection of drinks was rather limited, I settled for a mango juice while Steven went for ice tea.

There were 3 women in the small cafe, including the owner, and they seemed very friendly with each other. After a while, the store owner asked me where we came from, and we were drawn into quite a long discussion about various things. 

The discussion started out in japanese, but after half an hour or so, it turned out one of the women spoke perfect english as her man was from the US. After this, the remainder of the discussion was an unhealthy mix of japanese and english :)

It also became apparent that these 3 women were all related, as one of them was the sister of the store owner, and the last one was her mother.

![Cafe](/cafe.jpg)

One thing worth mentioning is that the store owner (left) was 40 years old, while her sister (right back) was past 60, and her mother (right front) was 93 years old! It's quite amazing how young japanese people look.

We spent close to 2 hours at this cafe, and then we started walking back to our hotel where we rested for a bit before returning the yukata to the rental store.

![Laptop and yukata](/laptop_yukata.jpg)

It felt so liberating being back to our old clothes again. Being able to walk properly was such a relief. 

As we had not had anything to eat during the day, we went to Ueno to have a dinner at TGI Friday's. By this time I was so tired of japanese food, so I wanted to avoid it as much as possible. I had a very nice steak, while Steven had a burger. 

During the dinner, I had the idea of going to the cinema again, as the previous cinema visit had been such a success.

Looking at the schedule for the same cinema we had been to the previous day, I could see that The Lion King was showing. I had been wanting to see this new adaptation for some time, so it felt like a great opportunity, and so I booked two tickets for us.

Unfortunately, I was not too impressed by this movie. It might have been because I have seen the original quite a few times, and this adaptation being so true to the original. I also had a hard time connecting with the characters due to the realistic animal approach used in this new adaptation. It felt more like watching a documentary on Discovery Channel than watching a movie.

Back at the hotel again, Steven inspected his foot. The geta sandals had not only scraped off parts of his skin, he also found himself with one of the largest blisters I've ever seen between his toes where the string of the geta sandal had been rubbing against his skin.

![Blisters](/blister.jpg)

We had booked another hotel close to the airport for the next day, meaning we would have to check out at 10 in the morning. As a result, we tried to go to sleep quite early that night, with mixed success.
