+++
author = "author"
tags = [
  "japan",
  "traveling",
]
date = "2019-08-03T10:19:24+02:00"
draft = false
title = "Japan 2019: Day 2"
type = "post"

+++

# Japan 2019: Day 2

Sorry for the late update, but this "day" has been rather intense as you will get to see.

After a night of roughly 6 hours of sleep and a slightly delayed flight from Arlanda to Vantaa in Helsinki, we were 
just trying to kill time before we could board our flight to Japan at 16:05.

![Waiting at Arlanda](/arlanda.JPG)

Before that though, we figured we would have something to eat, which also came with the benefit of allowing me to use some leftover Euro from my last trip to Italy.

My intention was to exchange any remaining Euro to Yen, along with some British Pounds I had laying around at home from one of my previous visits to London. Unfortunately it seems like these British Pounds are phased out, and therefore cannot be exchanged outside of England.
Forex might have accepted them with a reduced exchange rate according to their site, but unfortunately there were no Forex offices at Vantaa airport, so they might just end up as dead weight unless I can find some place in Japan to exchange them.

In the end we settled for lunch at O'Learys and we ordered a burger each, both of which were unexpectedly decent.

![Happy after a meal at O'Learys](/olearys.JPG)

An interesting question I was wrestling with was whether to treat the actual trip to Japan and our first day in Japan as 
separate days, since technically, when we arrive in Japan it will be morning there, while it's still night at home.
But knowing myself, I knew I would not be able to sleep on the plane, basically forcing me to pull an all nighter, making it "one day" in a sense.

In the end I decided to treat them as one single day.

The plane ride was uneventful, and as expected I didn't manage to catch any sleep at all. The in flight entertainment was loaded with plenty of movies I wanted to see though, so I had no problem keeping myself occupied during the flight.

Dinner on the plane was quite decent. I went for beef slices with red wine sauce and potato gratin, while Steven went for fried chicken with rice.

![Dinner on the plane](/food.JPG)

When we arrived in Japan roughly 9 hours and 10 minutes after departure, I was very tired to say the least. Steven managed to catch some sleep at least, so he was quite alert.

![Tired me](/tired.JPG)

Somewhere along the plane trip my eyes started to get really watery, which I assume could have to do with the dry air on the plane, although I've never experienced it before. It got worse and worse, to the point where they were so irritated and filled with tears that I could barely see a thing, just in time for immigration where I could barely see what I was writing on the immigration forms.

Immigration went without issue though, and I could pick up my checked in luggage without any issue as well. When trying to repack my luggage my eyes were stinging to the point I had to go to the toilet and completely wash my face, removing all of my makeup in the process.

This helped a bit at least. My eyes were still red, and I probably looked like a drug addict, but at least the tears stopped flowing.

We took the train to Chiba where our hotel for the night was located. It was still around noon at this point, and we were both hungry, so before going to the hotel we went for lunch at a steak house where I had sirloin steak with rice, while Steven had hamburg steak with rice.

The food was quite decent, but the entire inside of the steak house was covered in smoke and steam from the food, so my clothes ended up quite smelly after the visit.

![My sirloin steak](/steak.jpg)

Our original plan was to check in at the hotel, leave our luggage there and then head out again to watch some buddha statues engraved into a mountain wall. I was tired to the point where I just wanted to sleep though, and Steven was kind enough to accept just staying at the hotel.

The hotel was actually better than I had imagined, sporting facilities such as USB charging, shampoo, conditioner and a fridge among other things. The interior was nice and modern, and we got a room on the 14th floor providing us with a nice view of the city of Chiba. However, I was very surprised to find that the bed was extremely "firm". Laying down on the bed, it was only flexing slightly, giving the impression of a rather thin matress placed upon a plane made of masonite.

![Our beds](/hotel.jpg)

I had planned to force myself to stay up at least until 20:00 or so in the evening in order to try and get into the japanese timezone as soon as possible, but I just couldn't force myself to stay up. I was so tired I could barely think properly, having slept so little the night before as well.

Therefore, I finally crashed at around 15:00.
