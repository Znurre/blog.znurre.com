+++
type = "post"
tags = [
  "japan",
  "traveling",
]
date = "2019-08-25T01:50:01+02:00"
draft = false
title = "Japan 2019: Day 19"

+++

# Japan 2019: Day 19

Today we woke up with only one real goal for the day, to go to Akihabara for some geeky shopping.

Both me and Steven had things we wanted to buy, and we had predicted this shopping to take most of the day. However, it turned out that Toranoana had pretty much stopped selling doujin CDs, and Melonbooks did not have too much of it either, making my errands come to a quick end.

We had lunch at Subway in Akihabara, since that has become somewhat of a tradition for me. I was surprised to find that they only accepted cash.

After lunch and some additional strolling around, we then went back to the hotel. 

At this point, I had a complete mood crash. These 2 weeks have been disastrous for my self esteem, being surrounded by fashionable, good looking, small and slim japanese women all days. I could not help but feel big, clumsy and ugly. I was so tired of Japan, and just wanted to go home.

I spent a few hours at the hotel feeling miserable, and then we went outside again. This time around we went to Kappabashi to look for sushi wares for a friend of mine who is running a sushi restaurant back in Sweden and who had requested me to buy some things for him.

After walking around for some time, taking pictures of various sushi wares, we started heading back to our hotel. On the way back, I managed to find a Mountain Dew Violet in a vending machine. I had been wanting to try this for a long time, and so I purchased it and brought it back with me to the hotel.

![Mountain Dew Violet](/mtn_dew.jpg)

As expected, it was not all too good, and in the end I did not even drink all of it.

Later in the evening, we went to Hard Rock Cafe in Ueno again to have dinner. I felt like this was one of the few things that could cheer me up, and a good burger and a Mango Tango definitely helped improve my mood.

Having returned from Ueno, we walked out to find a coin laundry to have our last laundry here in Japan.

I had found a somewhat nearby coin laundry which had also received good ratings on Google, but after our experience there I would have to say I do not agree with them. Our clothes did not get clean at all. Maybe the washing machine was out of detergent, who knows, but the clothes looked just as dirty as before and I had to hand wash my clothes afterwards.

There was also a cockroach inside the laundry, making me question the cleanliness of the place.
