+++
draft = false
title = "Japan 2019: Day 1"
tags = [
  "japan",
  "traveling",
]
date = "2019-08-02T13:07:47+02:00"
type = "post"

+++

# Japan 2019: Day 1

Hello everyone. Today marks the start of this year's trip to Japan.

For those of you who are not aware, I am going there yet again, this time for 3 weeks together with a good friend of mine 
named Steven.

This time around I figured I would do something I usually don't:  write a travel log.

It's unusual for me to be away from home for this long, and rather than having to keep everyone at home up to date with 
whatever is happening here, I found it more convenient to refer anyone who might be interested to this blog instead, where I 
will post 
excerpts from our daily adventures along with pictures.

It's said that one of the best ways to get to know another person is to travel together, and I've already learned one thing 
about Steven before the trip has even properly begun: he is a very cautious person, unwilling to take any risks.

Steven opted to take the earlier plane to Helsinki where we will transfer to the flight to Narita.

Previous years I've always went for the flight that leaves at around 14:00 or so from Sweden, resulting in a transfer window 
of around 30 minutes which is more than enough considering that there are no additional security checks once in Helsinki, and 
the 
airport is rather small.
This was not enough for Steven though, who wanted larger margins, resulting in us boarding the plane for Helsinki at 08:30 
tomorrow morning and then waiting in Helsinki for roughly 6 hours. Oh joy :)

My response was to demand we spend the night at Arlanda, since otherwise I would have to basically get up in the middle of 
the night to catch a train in time for the flight.
As a result, I am now on my way to Arlanda where we will meet and spend the night at Clarion Hotel before leaving tomorrow 
morning.

I am really looking forward to the trip and the things we will experience.

To be honest, I don't know much about the schedule or the plan for this trip since Steven has mostly arranged everything on 
his own.
Not having to be in charge of everything during the trip will be a first for me since my childhood, and will 
hopefully be relaxing and a nice change of pace.

Despite not having to care about the planning of the trip at all, I've actually spent way more time than ever before worrying 
about the details for the trip. More specifically what to wear, and how to be able to carry a large quantity of liquid 
(makeup and skincare) that is way above the limit for hand luggage without having to bring along any large bags.

You see, one of the few things I **do** know about this trip is that there will be plenty of walking, and we are literally 
gonna travel through more than half of Japan, making us backpackers in a sense I guess.

Despite being a backpacker, there are some things I just cannot give up. Japan is one of the most fashionable countries in 
the world, and if I am going there I at least want to look somewhat decent. That means I will need good looking clothes, 
proper skincare and makeup.

One would think that a reasonable thing to do then would be to buy a large backpack with proper support in order to fit 
everything I want to carry along, but no, that would 
not look fashionable enough, so instead I opted for a 15L backpack by my favorite brand when it comes to bags: Golla.

![Golla Orion L](/golla-orion-large-backpack-salt-and-pepper-front_1.jpg)

To its defense, the backpack actually has a few useful features such as being very lightweight, sporting a dedicated laptop 
pocket and 
being resistant to rain.

At the end of the day, the backpack can only hold 15L though, making it very difficult for me to fit everything I could want 
to bring with me, especially considering I am bringing both my laptop, my Nintendo Switch and my Bose headphones.
Those three alone take up more than half of the bag, and the rest is occupied by amenities such as skincare, dry shampoo and 
makeup.

This obviously leaves me with very limited space to bring with me other things, such as clothes or shoes.

My answer to this was to buy crop tank tops in reasonably good time before the trip, since those would satisfy at least 3 
conditions:

1. Keep me somewhat cool in the sauna that is Japan. Together with extreme humidity we can expect temperatures up to 34°c 
only for the first few days, and the temperature will likely rise as we get further into August.
2. Make me look somewhat fashionable. Plain, white crop tank tops look good with most bottoms, especially given my current 
tan. My first idea was to mix them with a nice pair of denim shorts, but this plan failed. More on this later.
3. Occupy very little space. These crop tank tops obviously have very little fabric, and given that they are sleeveless I 
will hopefully get away with only changing them each other day. We are planning on doing laundry every fourth day, so that 
should work out well.

Another important thing to consider for the trip was proper shoes. Since we will be walking a lot, a good pair of shoes is 
essential. I would have preferred to bring along a pair of sandals, which would look good to any outfit, but that would 
have left me with blisters within a day.

Since my old trainers were damaged in the heel area I opted to buy a new pair, as I was afraid that the old ones would give 
me blisters.

I found a very nice pair of shoes with very good support for the foot, plenty of dampening and a snug fit. The problem? The 
color.

![Asics Gel-Kayano 25 W](/142649904000_10.jpg)

Trainers already look bad with most outfits that are not sporty, and add to that an odd color such as this and you are in for 
a very hard time trying to coordinate an outfit that looks classy.

I still didn't fret though. My plan with crop tank tops and denim shorts worked very well in combination with the shoes, 
although I did have a gut feeling that the outfit just might be a bit too revealing in the midriff area for Japan, which is a 
rather conservative country.

My gut feeling proved to be correct as I searched for people's opinions through internet forums. While it's apparently 
not too 
unusual of a sight in Tokyo, especially in the more fashionable areas such as Harajuku, in the more rural areas of Japan 
where we will be going, people might not be as accepting of it. And so my dilemma had started.

At this point I had only one week to go before the trip, and I was starting to panic a bit.

I didn't want to give up the crop tank top idea as it satisfied the 3 aforementioned conditions very well, so I figured the 
only viable strategy would be to find a pair of very high rise shorts, alternatively a high waisted skirt.

After having ordered a bunch of bottoms and tried them out, and after having consulted a few people, I finally settled on a 
white pleated skirt which looks very elegant and fashionable, but hopefully casual enough to not stand out too much in 
combination with the shoes.

![The skirt](/DSC00054_s.jpg)

I guess I will try to pass it off as fashion. After all, [everything is 
fashion](https://www.youtube.com/watch?v=gVcshkaV4oc).

As for my dilemma with the liquids, me and Steven came up with a clever solution.
I will be bringing my usual bag as checked in luggage and put the liquids in there for our travel between Arlanda and 
Narita.
Once we get to Narita, we will put the bag in storage at the airport for a total cost of roughly 300 SEK for the entire stay 
so that it can be used to carry liquids and goods we might buy back to Sweden as checked in baggage.

The problems were not all solved by that, however.

Our trip in Japan starts by domestic flight to Chitose, where we will travel down through Japan until we eventually come back 
to Tokyo again for the return flight home.
That obviously means we will have to solve the same problem once again, but this time without any possibility of storing the 
bag at the airport as we will not be returning there again.

The solution I settled for was to bring with me the box that the shoes came in, which will hopefully be enough to protect its 
contents from the rough handling of checked in luggage. I will then dispose of the box once we get to Chitose, and from there 
on its contents are to be carried in the backpack again for the rest of the journey.

Phew, that was a lot of information! :)

Thank you for reading this far. I will be back tomorrow with an updated travel report.
