+++
type = "post"
draft = false
date = "2019-08-23T03:52:14+02:00"
tags = [
  "japan",
  "traveling",
]
title = "Japan 2019: Day 18"

+++

# Japan 2019: Day 18

We woke up this day in Osaka, without any plans whatsoever. After thinking about our option for a bit, we came to the conclusion that neither of us had any business in Osaka, and so we decided to head for Tokyo as quick as possible.

We took the subway to Osaka station, where we made sure to reserve seats for the Shinkansen to Tokyo, and then went to Shin-Osaka station where the Shinkansen departs.

I purchased an ekiben to eat on the train, this time in the shape of beef tongue with rice as my previous experience had been so good.

It tutrned out to be quite mediocre in comparison to the real dish, however, which I guess was to be expected.

One thing worth mentioning is that these lunch boxes are crazy expensive. It's not unusual to pay between 1300 and 1500 yen for one, which currently equals 118-135 SEK. Much more expensive than a lunch in Sweden!

The train trip was uneventful as usual, but also very tiring. Even the Shinkansen cannot cover the 500 km between these two cities in less than 3 hours.

When we arrived at Tokyo we first had to find the correct exit from this rather large station, and then walk to a nearby subway station in order to take the Ginza subway to our accomodation in Asakusa.

Steven had been tasked with finding the best way to the hotel, and he said to get off at a stop before Asakusa, as that would be much closer to the hotel.

After walking for a little bit, we found the hotel which was located in a shopping street. The receptionists seemed a bit confused when we tried to check in, and asked us for a booking confirmation, which was a first so far. It turned out that we had walked to the wrong hotel again. This time, however, it was the same hotel chain, which apparently had three hotels in the same area. Very confusing.

As this mistake of ours seemed to be a rather usual occurance, they even had maps prepared for how to get to the other hotel, and after following the map for a few minutes, we reached our destination. 

This time we had arrived at the correct hotel at least, and the check in went without any issues.

The room was very small. The smallest room of any hotel we had stayed at so far, but at least the place felt very modern and clean, and it was close to the subway station.

After inspecting our room and leaving our luggage, we took the Subway to Ueno, where we visited Hard Rock Cafe for an early dinner. It felt so good eating normal, western food after 2 weeks of only Japanese cousine. This was also the first time in 2 weeks that I held a knife and fork.

With full stomachs, we headed out for some shopping in the Shibuya and Harajuku area. I went to several second hand clothing stores, and managed to pick up 4 items for a total price below 5000 yen. We had a very fun time too, and Steven helped me out by holding my bags and the clothes I wanted to try.

After maybe 2 hours or so, the evening finally came to an end with a very weird drink at some cafe in Shibuya. It was tea based, and they had put some weird jelly balls inside it that constantly got stuck in the straw. The evening was warm, and the bustling night life of Tokyo felt great.

I was very happy when I went to bed that evening, even though it took quite some time before I finally fell asleep.
