+++
type = "post"
draft = false
title = "Japan 2019: Day 20"
date = "2019-08-25T02:51:52+02:00"
tags = [
  "japan",
  "traveling",
]

+++

# Japan 2019: Day 20

We woke up this day without any real plans for the day at all.

The day went by without us doing much. Steven selected a bunch of pictures from the ones we had taken, and we went to Seven Eleven to print them as postcards for him to send to friends and family. We then had lunch at a rather shoddy looking Himalayan restaurant in the hotel vicinity.

The rest of the day was mostly spent on the hotel, with me playing Switch and Steven trying to come up with things to write on his postcards.

Sometime during the day, I came up with the idea to go to the cinema. Alladin was showing on a nearby cinema with english audio, and I had heard great things about the movie.

We therefore went to the cinema around 6 in the evening, and after some trouble finding the entrance, we made it a few minutes before the movie was to start.

The movie was great. Much better than I had anticipated. It was quite long though, slightly over 2 hours, so obviously I was very hungry at this point.

I managed to convince Steven to go to Hard Rock Cafe again. Third time's the charm. This time I did not have a Mango Tango though, as I wanted to be able to sleep :)

After this, I was once again in a very good mood, and Tokyo did not feel so bad afterall.

That evening, I booked a yukata rental for the following day. I've wanted to try that since a long time, and as mentioned I was very disappointed when we had to cancel our yukata rental plans for Takamatsu.

I went to bed happy that night, eager for the next day to come.
