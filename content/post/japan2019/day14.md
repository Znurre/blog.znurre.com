+++
author = "author"
title = "Japan 2019: Day 14"
type = "post"
tags = [
  "japan",
  "traveling"
]
date = "2019-08-18T12:11:43+02:00"
draft = false

+++

# Japan 2019: Day 14

A very lazy day. We woke up late as we had not set an alarm.

As I had wanted to try that other dish the previous day, we actually went to the same place where we had our dinner for what could probably best be described as a brunch.

The dish itself was actually really delicious, as I had suspected, but as this place worked by selling food tickets from a vending machine, I did not find a way to order it without noodles on the side. These noodles were cold, thick udon noodles, and were not very delicious at all. I did not manage to eat them all.

After returning to the hotel again, I spent some time playing Nintendo Switch, while Steven was not doing much at all. Prior to this, my Nintendo Switch was one of the few parts of my luggage I had regretted bringing with me. Truth to be told, I had not had any time to actually play games. During our train trips, I had mostly been writing on this blog, or looking out the windows at the beautiful landscapes passing by outside.

One of the games I had brought with me on this trip was the new Fire Emblem, released just a few days before our trip. I didn't really know what to expect from it, but after having played it I can say that I am hooked.

![Lazy switch day](/switch.jpg)

It felt like a shame spending the whole day on the hotel room playing video games, so after an hour or so we went to Nipponbashi again, where I purchased a few Japanese manga which seemed interesting. We then took the subway to a station close to Osaka Science Museum, which was an attraction I had looked up on the internet and which had received good reviews.

The museum was really crowded, and we had to stand in line for 20 minutes or so before we could purchase tickets. This reflected the number of people inside the museum as well, and all experiments and attractions were occupied by kids and their parents. In addition, most text was only available in japanese, making this excursion quite pointless.

We stayed at the museum for 30 minutes or so, and then went for a stroll in the surroundings where we looked for a cafe to have something to drink, and if possible, something to eat.

I was really thirsty from the heat, and really wanted to drink a Lemon Squash, which is a drink that is quite common to see in japanese cafes and which is very quenching on a hot summer day.

After some time of walking, we did actually find a cafe, but unfortunately their ingredients for Lemon Squash were sold out and so we had to settle for a "mix juice", which was not all that tasty.

I had not yet had enough of shopping, and so we went shopping around Osaka station, an area that I had been recommended by several websites. We quickly found though that these shops were way outside my comfort zone, being way too elegant and pricy. Most of the shops were the kind where a person stood waiting at the entrance to guide you around the shop, and they generally felt very aimed towards people with shitloads of money and super expensive clothes and accessories.

Some of the stores were better in that regard, and especially one store had very nice clothes which I had gladly purchased had they been available in my size :(

While the mix juice helped quench my thirst for a while, I was still hungry. I had spotted a chinese restaurant in Nipponbash which I was curious about, and so we went there to have our dinner. Unfortunately it was completely full, and so we had to look for other options.

As we walked back towards our hotel, we looked around for other options, but without any luck.

Once back at the hotel, I had a look at Google maps and found two possible candidates in our vicinity. One was a cafe which seemed very cosy, and another was a chinese restaurant. The cafe turned out to be closed, despite the opening times provided by Google stating that they should be opened, so we went looking for the chinese restaurant.

The chinese restaurant was apparently really popular, and despite being there 10 minutes before the opening, all seats were already reserved.

At this point I was really hungry, and felt like I could eat pretty much anything. We went further and further away from the hotel, and finally found a shopping street with a cafe which seemingly served at least two dishes, both of which I could consider eating.

Upon entering, the staff was surprised to see us there, as they would apparently not open for another 10 minutes or so, despite the sign on the door clearly saying "Open". They were at least kind enough to let us sit down, and offered us water and tissue as is customary here.

When we inquired about the dishes on the menu, they told us that they were out of rice and therefore could not serve any of them. Just our luck. We walked out from the cafe without having eaten anything.

Disheartened, we kept on walking without any real objective. After walking for a while, I spotted what looked like a chinese restaurant. It looked a bit shoddy, but at this point I was ready to accept almost anything. We went inside and we both ordered the same dish, which was stir fried pork with vegetables and rice.

Of course my hunger could have impacted my impression, but damn was it delicious. I would gladly have chinese food over japanese food any day of the week. 

Satisfied, we walked back to our hotel where I spent the rest of the evening playing more Nintendo Switch.
