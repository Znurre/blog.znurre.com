+++
date = "2019-08-18T07:17:27+02:00"
title = "Japan 2019: Day 12"
type = "post"
draft = false
tags = [
  "japan",
  "traveling",
]

+++

# Japan 2019: Day 12

The day started with us waking up as usual, preparing ourselves for departure, and then walking towards the station to get on the train for Takamatsu. On the way there, we stopped by a 7eleven store where we had our breakfast for the day.

It was raining outside due to the typhoon, and as such, we decided to wait at the station for the train to leave instead of trying our luck at any activities.

We sat there for a bit more than an hour, and an interesting detail about this particular train station was an endless loop of piano music played in the background, which gave the station a very sad and eerie kind of feeling.

The typhoon that was headed for Japan had more or less entered Shikoku by this point, and we were aware that we might have to expect quite a bit of wind and rain, even in the northern parts of Shikoku where Takamatsu was located. Even so, we carried on with our plans. Anyhow, going north from Kouchi could do no harm, as the typhoon would strike the south of Shikoku first anyway.

On the train ride for Takamatsu I received an email from the owner of a yukata rental store I had been in contact with. The idea had been to have both me and Steven rent a yukata, and then wear this for the evening as we had a stroll around in Takamatsu where a festival was being held. This was something I had really been looking forward to.

Unfortunately, due to the typhoon, the festival had been cancelled. With the festival being cancelled, and the weather forecast forecasting rain and heavy winds, we felt it was best to cancel our yukata rental plans.

After arriving at Takamatsu station, we had lunch at a restaurant at the train station serving tonkatsu dishes. Tonkatsu is basically fried pork served with rice. I went for a different kind of tonkatsu that was prepared in a garlic sauce, and which was very delicious.

During our lunch, we paid close attention to the public announcements made using the stations PA systems. Apparently, due to the typhoon, the last train leaving for Honshu would leave at around 21:00 in the evening the same day, and the next day, all trains would be cancelled.

With our plans for Takamatsu being ruined, and not really feeling like being stuck in a small city with a typhoon raging outside, we decided to cancel our hotel reservation for the night and head for Osaka instead.

We figured Osaka would be big enough of a city, and far enough from the typhoon to be a good place for staying temporarily until things had settled down. By spending a few days there, we would also be able to survey the damages done by the typhoon and make plans accordingly. As typhoons bring with them heavy rain and wind, it's not unusual for them to disrupt public transportation for weeks, and this could have affected our plans for the rest of the trip, not to mention our chances of getting back to Tokyo for our flight home.

With this in mind, we got on the first available train for Okayama, where we would change to the Shinkansen for Osaka.

The train trip to Okayama went well, however the transfer to the Skinkansen did not go as well. We did not have any seat reservations, as this was a completely unplanned event, and as such we could only ride in the cars with unreserved seats.

We quickly learned that we were not the only people wanting to escape the typhoon. Everything was chaotic, people even skipping lines to get on the trains, something which is very unusual in Japan. At some point, I just gave up trying to get on the train. Me and Steven went to check if there was any possibility of reserving seats on any of the Shinkansen going towards Osaka, but everything was already fully reserved.

As a last resort, we took the Shinkansen in the opposite direction instead. All cars were more or less completely empty, as we were going towards the direction of the typhoon. We then got off at the first station possible, and waited there for the next Shinkansen going towards Osaka.

![Deserted station](/deserted_station.jpg)

Using this tactic, we managed to get seats all the way to Osaka. Unfortunately, as the station we had selected was a very small one, we could only board the Kodama Shinkansen which not only stops at every single station, but also waits there for up to 15 minutes.

We arrived to Osaka late in the evening, and took the subway to our hotel. After the check in, we went to look for a restaurant to have a late dinner. After having walked just a few minutes, we found a restaurant without a menu outside, but the name contained the kanji for "beef", so we figured it could not be too bad.

Once inside, we realized that this was a really fancy place. Steven did not feel all too comfortable. As we soon came to realize, this was a restaurant serving kobe beef that you grilled yourself. Kobe beef is well known to be some of the finest and most expensive meat in the world.

This dinner turned out quite expensive, around 400 SEK for me, but it was very delicious. Even Steven enjoyed it :)

We went to bed quite late that night as we had decided to stay in Osaka for 3 nights, and therefore would not have to check out in the morning for a change.
