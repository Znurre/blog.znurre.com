+++
tags = [
  "japan",
  "traveling",
]
draft = false
author = "author"
date = "2019-08-10T11:28:53+02:00"
title = "Japan 2019: Day 7"
type = "post"

+++

# Japan 2019: Day 7

Still trying to catch up, which continues to be difficult. Today was another busy day as you will soon get to see :)

The day started off quite early, already at 7:00 as we woke up in good time to make it to the train leaving at around 9:00 without having to rush. With this amount of time available, we intended to also have time for a quick breakfast as well before getting on the train. We therefore purchased breakfast at 7eleven after having checked out from the hotel, and sat down to eat it while waiting for the train to arrive. 

The train trip was uneventful, and we arrived at our destination without issues where we quickly found the right bus. The bus ride was quite long, closer to an hour, but it was very scenic and beautiful as we slowly ascended the mountains on small, winding roads.

This bus took us all the way to our final destination for this day's activity, which was a ropeway used to get up the mountain known as Mt. Tanigawa without having to scale it by foot. The intention was to get up there, look at the view, and maybe take a stroll before descending again.

The ropeway ride took approximately 10 minutes, and when we got off we were at around 1300 meters above the sea.

![Ropeway](/ropeway.jpg)

Up there, the air was so much easier to breathe, and the landscape was clad in lushy greenery, with people playing and laughing. It was a strange sensation somehow. I felt like I had arrived to paradise.

We quickly realised that we were nowhere near the top. If we wanted to keep going, we had the choice of either scaling the remaining 200 meters or so by foot, or to take a lift up for a sum of money. We decided to pay up and ride the lift up.

The lift was a regular two seat lift, very similar to the ones used to ascend mountains when skiing. It was my first time riding one, and it was a bit scary. On the way up there, we had our photograph taken by a photographer who thought we were a couple. This photo could later be purchased.

![The lift](/lift.jpg)

Once up there, we spent some time admiring the views which were very epic and beautiful.

![View 1](/view1.jpg)
![View 2](/view3.jpg)
![Me and Steven on the mountain top](/me_and_steven.JPG)

We then pondered our options for a bit. We had purchased roundtrip tickets for the lift, so we could have descended using that again, but we still had a lot of time and so we decided to take the forest path down instead.

At this point, we had intended to buy some water, but unfortunately everything in all of the available vending machines was sold out, so we had to keep going without replenishing our water reserves.

![Path 1](/path1.jpg)
![Path 2](/path2.jpg)
![Path 3](/path3.jpg)

At some point, we reached a sign letting us know that we could either go to the right and return down to the ropeway station landing, or to the left to keep going towards the next mountain top. We chose the latter, and set off towards the next mountain top.

The forest path was absolutely amazing and very well maintained. Lush greenery, oddly shaped rocks and wooden stairs. It provided enough challenge without being too difficult.

I had decided to leave my backpack down at the ropeway station, but Steven had brought his with him. That meant he had quite a burden to carry compared to me. While I was jumping, sliding and climbing, he was struggling to keep up, and before too long he was very exhausted. The lack of water did not help.

After one particularily difficult climb we decided to give up and head back to the ropeway station landing.

![Steven gave up](/steven_gave_up.jpg)
![Lova gazing towards the distance](/lova_gazing.jpg)

Once back at the ropeway station, we made sure to drink a lot of liquid.

![Replenishing liquid](/liquid.jpg)

Another point of interest in the surroundings was the famous train station known as "Doai", situated only about 30 minutes walk from the ropeway station. We walked there in the beautiful surroundings and stumbled across a waterfall which we stopped and admired for a while.

![Waterfall](/waterfall.jpg)

I didn't know what to expect from Doai station. Steven had just told me that it was very special, and that it was definitely worth seeing. When we arrived there, I realized how right he was about that.

One of the platforms of the station was located underground, hundreds of stairs down. Not only was it a great opportunity to hide from the heat, it was also very magnificent and epic.

![Doai station 1](/doai1.jpg)
![Doai station 2](/doai2.jpg)
![Doai station 3](/doai3.jpg)
![Doai station 4](/doai4.jpg)

It was around this time that I realized I had incurred quite a sunburn on my neck and shoulders. Usually, I'd leave my hair free flowing, and tying it up in a bun exposed my neck so much more. I had thought my shoulders would be okay though, given how much of a burn I already had, but apparently not.

When we later changed trains for our final destination, Takayama, the temperature inside the train was 30°c and my shoulders were burning.

The train ride itself was quite epic though. We were riding the Hida 20, which features very large windows, and the scenery with mountains, rice fields and winding rivers flashing by outside the windows was absolutely stunning.

After leaving the train station in Takayama, we went straight for our hotel. A light rain had just begun, and so I finally got a use for the umbrella I had brought with me. The rain increased in strength and intensity as we were walking to our property, and when we got there it was absolutely pouring down.

We checked in and went to our room, which was a classic japanese twin bed room with tatami mats and futon mattresses, but without bathroom and toilet. These facilities were shared with the rest of the guests. What complicated things a bit for me was that the showers were separate for men and women. Luckily, there were at least private booths, and so I went to shower at the women's showers without any issues, even though it was quite an uncomfortable experience.

It felt realy good to shower as I had been sweating quite a lot in the intense heat, especially during our forest path excursion in the mountains. Afterwards, I just lay in the room in front of the air conditioner trying to cool down.

Needless to say, I did not really feel like going outside again after this. All my clothes were sweaty and dirty, and I did not want to get sticky again, just having showered.

Steven was kind enough to go outside to shop some bread, and so this evening's dinner came to consist of melonpan and green tea.

We also started a laundry of our dirty clothes, which in my case included pretty much everything. I even gave up and threw my skirt into the mix, even though it should only be hand washed according to the label. Unfortunately, the laundry could not get rid of some small stains. They are not that noticable though.

While Steven was out shopping and doing the laundry, I laid out our futon mattress for the night. I personally love futon mattress, as I find them very cosy and comfortable, but Steven was not as big of a fan.

After chatting for quite a while as usual, we went to sleep.
