+++
type = "post"
draft = false
tags = [
  "japan",
  "traveling",
]
date = "2019-08-19T09:42:16+02:00"
title = "Japan 2019: Day 16"

+++

# Japan 2019: Day 16

We woke up in Tottori after about 8 hours of sleep.

After preparing ourselves as per usual, we went for breakfast at the nearest 7eleven store, which happened to be located at the train station.

When we had finished our breakfast, Steven used the toilet of the station, and while I was waiting for him, a guy came up to me and asked how tall I was.

I replied with my length, and we then started talking. It turned out he was a foreigner living in Japan, and was also the currently most well visited Couchsurfing host in Tottori, which I guess explained his extrovert attitude.

Steven came back and joined in on the conversation as well. After talking for a while, asking about tips of what to do in Tottori among other things, we decided to go with one of his recommendations to visit the sand dunes of Tottori.

The sand dunes of Tottori are the only sand dunes in Japan, and are therefore a very famous attraction. They even have camels there that you can ride, if you pay of course.

To get the to the sand dunes, we had to ride a bus that departed every hour or so, and we had to wait for 30 minutes before the next departure.

At least we got on the bus, and since we had been lining up for it so early, we even got to sit. There were lots of people on the bus, and many did not get a seat. What happened then really surprised me though. I had expected for them to have to stand, but instead they simply flipped down an additional seat in the middle of the walkway, turning the 2+2 seat design into a 5 seat design.

The bus was quite slow, but after 20 minutes or so we arrived at our stop and got off.

We then took a quick walk on the sand dunes, but even though it was certainly a cool experience having seen the sand dunes, they were not all that interesting. Besides, our shoes were already filled with sand.

![Dunes 1](/dunes1.jpg)
![Dunes 2](/dunes2.jpg)

The bus back took even more time, and when we finally got back to the station, we were both hungry and decided to go for a lunch. Once again, it happened to be a restaurant where the best option seemed to be hamburg steak, making this the fourth time so far in Japan for Steven.

We boarded the train for Hamasaka, where we would change trains for our final destination of the day, Kinosaki-onsen.

The train was a rather ordinary single car diesel train, which slowly made its way towards our target with limited comfort. However, the train we transferred to in Hamasaka was something completely different.

While still a single car diesel train, the interior was certainly very interesting. Dark wooden details and seats facing the windows like bar stools. The whole interior gave off a very luxerous feeling.

![Train 1](/train1.jpg)
![Train 2](/train2.jpg)

We got off the train just a few stations before Kinosaki-onsen to visit a beach at the sea which we had scouted out beforehand. Unfortunately it was not all that hot outside anymore, only 29°c, which by now feels rather cool after being used to 36°c. Even so, we wanted to use this opportunity to take another swim in the sea.

The beach was very crowded, and the waves were huge. We spent maybe 30 minutes or so there before heading back to the station again to continue to Kinosaki-onsen.

![Waves](/waves.jpg)

After arriving to Kinosaki-onsen, we checked in to the ryokan where we would spend the night. The ryokan was located in a very old and traditional house, with lots of stairs and small narrow corridors inside. Our room came with tatami mats, futons and our own private balcony. It was a very nice place.

Having acquainted us with the ryokan, we headed out for dinner at a chinese restaurant. The food was not as good as the chinese restaurant we had been to the day before, but at least it was okay.

Later in the evening, after chilling at the hotel for a while, I used the opportunity to take a bath at the private onsen offered by the ryokan. It felt really nice soaking in the hot water.

I spent the rest of the evening playing Fire Emblem on my Nintendo Switch, and then went to sleep around 23:00 or so.
