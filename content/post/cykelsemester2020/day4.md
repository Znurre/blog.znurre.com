+++
draft = false
title = "Cykelsemester 2020: Dag 4"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-09"
type = "post"

+++

# Cykelsemester 2020: Dag 4

Jag vaknade upp i min säng på Villa Gladtjärn före alarmet, vilket jag vid det här laget var van vid. Oftast ställde jag alarmet på klockan 8 nån gång, men för det mesta vaknade jag av mig själv runt klockan 7.

Sömnen hade varit helt godkänd. Det rum jag bodde i hade två enkelsängar, där varje säng endast var 90 cm, vilket gjorde att jag flera gånger under natten hade slagit i väggen när jag försökt vända mig. Jag förstår inte hur folk kan sova i så smala sängar. Nåja, madrassen och kudden var väldigt sköna, och jag kände mig ändå rätt utsövd.

Efter att ha gjort mig i ordning och klätt på mig gick jag till restaurangen för att äta frukost. Den var folktom förutom den personal som jobbade där, och som satt vid ett bord och pratade. När jag kom fick dom bråttom att återgå till jobbet.

Frukosten var bra. Kanske bästa frukosten hittils den här resan. Förutom det vanliga fanns det våffelsmet och våffeljärn, så jag passade på att äta en våffla med sylt.

Sedan följde den sedvanliga proceduren med att packa ihop allt, montera det på cykeln, och checka ut. Dagen till ära sken solen från en klarblå himmel, och om man skulle tro väderleksprognosen så skulle det dessutom hålla i sig hela dagen.

Jag påbörjade så dagens cykeltur, som först skulle ta mig genom det lilla samhället Söderbärke. Av någon anledning hade jag fått för mig att det inte var speciellt långt dit, bara några km eller så, men det visade sig vara över en mil.

Vägen dit var dock vacker, och tiden gick fort. Jag cyklade genom små samhällen med små röda hus och träalléer som kantade vägen. Det var lite ensamt att inte ha sällskap av nederländaren. Vi hade förvisso bara cyklat tillsammans i ungefär 9 mil totalt, men under den tiden hade vi hunnit prata mycket, och det kändes som om vi kännt varandra betydligt längre tid än så.

Söderbärke visade sig vara ett väldigt trevligt och vackert litet samhälle. Jag stannade till vid en ICA-butik där för att inhandla 2 burkar energidryck, Dextrosol och en ny tandkrämstub eftersom jag gjort slut på den gamla. Efter att ha intagit den ena burken energidryck i solen utanför ICA så sadlade jag upp igen, och fortsatte längs Sverigeleden mot mitt mål för dagen - Nora.

Men mellan mig och Nora låg cirka 10 mil väg, och den totala stigningen var på över 1000 meter, så jag hade inga illusioner om att dagens etapp skulle vara enkel. Trots det kände jag mig försiktigt positiv. Det kändes som om cyklingen, trots trötta och ömmande ben, gick lättare och lättare för varje dag. 

Jag avklarade dom första 4 milen rätt snabbt, och kom fram till Malingsbo vid lunchtid ungefär. Malingsbo är ett litet samhälle med cirka 250 invånare, men här finns en camping, vilket jag antar ger ett visst incitament till att bedriva verksamhet här. Till exempel fanns här en gammal herrgård, som numera användes som pensionat och café. Jag stannade därför till på caféet och köpte en hemmagjord skagenmacka som jag sedan intog sittandes i solen på gräsmattan utanför.

När jag kände mig färdig så började jag förbereda mig för avfärd igen. Det var då jag upptäckte att mina cykelväskor börjat gå sönder. Väskorna spänns fast på pakethållaren med hjälp av tre stycken spännband, och själva fästet för en av dessa hade lossnat från väskorna. Två av fästena satt fortfarande kvar, och tyngden hos innehållet gjorde att väskorna inte gled omkring på pakethållaren, men det kändes ändå lite olustigt.

Eftersom det inte var mycket att göra åt detta just då så gav jag mig iväg igen, mot byn med det intressanta namned Kloten. Där tog jag av upp för en brant backe som ledde mig in på en grusväg, allt jämt längs Sverigeleden. Grusvägen fortsatte uppför ett bra tag, innan den lika plötsligt började slutta brant utför.

Med en känsla av downhill kryssade jag fram mellan sten och gropar på grusvägen, rädd för att cykelväskan skulle ramla av på grund av alla krängningar. Det gick dock bra, och jag slapp som tur var mötande trafik under färden nedåt. Snart kom jag ut på en bredare asfaltsväg igen, i ett litet samhälle som hette Ramsberg. Där stannade jag till vid vad som verkade vara en skola, för att få i mig lite energi, och för att vila ett tag.

![Ramsberg](/ramsberg.jpg)

Jag tyckte att dagens cykeltur hittils gått väldigt bra, och såg ingen anledning att betvivla att resten av resan skulle gå lika bra. Det visade sig vara naivt tänkt. 

Snart svängde jag av söderut igen, och blev välkomnad av motvind och uppförsbackar. Dessutom var omgivningen ytterst oinspirerande i form av vanlig blandskog som man kan hitta vart som helst i Sverige. Jag stretade vidare, och hann fundera på en hel del saker under tiden. 

Till exempel så slog det mig vilket annat fokus på tankarna man får när man är ute såhär och cyklar. Där man i vanliga fall tänker på större problem, som klimatkrisen, existensiella frågor och hungernöd så reduceras tankarna under cyklingen till mer konkreta, banala problem och iaktagelser. "När ska den här backen ta slut?", "Gud vad vackert det är här", eller "Aj, rumpan gör ont".

Cyklingen väcker även andra, mer flummiga tankar. En något slumpmässig reflektion som väcktes av en låt jag lyssnade på var till exempel att uttrycket "din kompis" egentligen bara används i tre situationer så vitt jag kan komma på:

Situation 1: En förälder som pratar med sitt barn.  
Situation 2: Någon som vill få ihop det med en annan persons kompis.  
Situation 3: En något hotfull situation där person A blir instruerad att se till att person B agerar önskvärt, typ "säg åt din kompis att ta det lite lugnt".  

Efter att ha kämpat i vad som kändes som en timme kom jag till sist ut i mer bebyggt område, och det kändes otroligt skönt.

Jag har tidigare sagt till en kompis som bor i Örebro-trakten att jag är så avundsjuk på honom eftersom det är så platt där, och alltså bra mycket bättre lämpat för cykling än Hudiksvalls-trakten där man knappt kan ta sig 1 mil utanför staden utan att åka på 200-300 meters stigning. Efter att nu själv ha cyklat i Örebro-trakten vill jag dock ta tillbaka det påståendet. Den värsta backen hittils under min resa var nämligen en backe precis innan Fanthyttan, som helt tog musten ur mig.

Vägen gick sedan vidare längs sjön Usken, och sedan Fåsjön. Omgivningarna var fantastiskt vackra, och det hjälpte mig delvis att hålla tankarna borta från mina trötta ben och min rumpa som inte mådde så jättebra.

Dom flesta som inte cyklat så mycket tänker att det värsta skulle vara att man får ont i rumpan, som i att man blir öm, men det kan man leva med, och det går över snabbt. Det riktigt stora problemet som man vill undvika är skavsår, och jag kände att jag var på god väg att utveckla det.

Det var hyfsat varmt, runt 20°c, och värmen gjorde att jag svettades om rumpan, vilket i sin tur gjorde insidan av cykelbyxorna fuktiga. Fuktiga cykelbyxor orsakar friktion, och det gör att irritation uppstår. Jag började känna av det någonstans efter Ramsberg, och det blev bara värre och värre.

I ett försök att låta rumpan vila ett tag stannade jag till vid en liten loppis vid vägkanten där jag köpte en glass som jag avnjöt i solen. Under tiden som jag åt den roade jag mig med att betrakta ett gäng polska turister som hade stannat till och provade varenda sko på hela loppisen av vad det verkade som, för att i slutändan inte köpa någonting alls.

När jag hade ätit upp glassen satte jag mig på cykeln igen för att avverka den sista milen. Det var kämpigt, men det kändes ytterst belönande när jag äntligen gled ned för den sista nedförsbacken, in i Nora.

På grund av vägarbete var jag tvungen att cykla upp på en trottoar vid ett tillfälle, och då skedde det som jag hade oroat mig för. Väskan hasade bak och ner mot bakhjulet, där den sedan hängde och bromsade eftersom två av spännena fortfarande var funkionella.

Jag var nära på att ramla på grund av den kraftiga inbromsningen, men lyckades i sista sekund klicka ur skorna från pedalerna. Efter denna incident var jag väldigt försiktig när det gällde cykelväskorna, och tog det ytterst lugnt på dom kullerstensbelagda gatorna i Nora.

Redan här insåg jag vilken fantastiskt vacker och mysig stad Nora var. Kullerstensbelagda gator, stora ekar, och gamla vackra, färgranna trähus. Det var helt enkelt otroligt idylliskt. Att staden dessutom kändes så levande, med restauranger, uteserveringar och butiker kring varje gatuhörn gjorde att staden kändes som tagen direkt från något sydeuropeiskt land som Spanien eller Italien.

Efter lite irrande fram och tillbaka hittade jag till sist hotellet där jag skulle bo den här natten. Det hette "Lilla Hotellet", och visade sig vara ett helt fantastiskt ställe, vilket jag insåg redan vid incheckningen.

Jag blev välkomnad av en man som jag antar drev stället, och som visade mig runt i "det stora huset". Där inne fanns en gemensam matsal, med ett kylskåp fullt av dryck som man fick ta av utan extra kostnad. Bredvid fanns ett gästkylskåp där man kunde ställa eventuella personliga varor som behövde kylas. För den som gillade kaffe fanns det alltid tillgängligt via en kaffeautomat, och flera sorters kakor var uppställda för alla att äta av.

Själva rummen som gästerna bodde i fanns sedan ute på innergården, som små uthus nästan. Jag fick nummer 14, och hade alltså en egen liten lägenhet för mig själv, med eget badrum och en liten uteplats. Han visade mig även att det fanns en tvättstuga, som jag fick använda helt utan extra kostnad. Helt enkelt en gästfrihet utöver det vanliga.

Via Lilla Hotellet hade jag även bokat 4-rätters middag på en gourmetrestaurang i staden, Bryggerikrogen, som varit med i White Guide 2018 och 2019. Jag gick därför och duschade, bytte om och gjorde mig redo för att gå dit. Min första anhalt var dock apoteket i staden, där jag köpte en slags skavsårsplåster som jag hoppades skulle lösa problemet med skavsår i rumptrakten. 

Jag anlände till restaurangen lite väl tidigt, och fann att dörren var låst. Mitt bord var bokat från och med klockan 6, och jag tänkte att dom kanske inte öppnade innan dess, så jag satte mig helt enkelt utanför och väntade. Efter ett tag kom det två andra gäster, och även dom hade bord bokat klockan 6. När dörren inte öppnats trots att klockan var slagen gick vi istället bort till den ingång som inhyste Bryggerikrogens pub, och det visade sig att det faktiskt var dit man skulle gå.

En servitris bekräftade min bordsbokning, och ledde mig till ett tomt bord på deras uteservering. Jag hade klätt upp mig, eftersom jag hade tänkt att det kanske var ett snobbigt ställe, men jag stod nog snarare ut som överuppklädd bland alla andra där. Min klädsel var dessutom inte så varm, och när solen försvann bakom byggnaden så började jag frysa, och jag bad därför om att få flytta in istället. Ett tag var jag den enda gästen inomhus, vilket kändes rätt underligt. Speciellt med tanke på att jag var där utan sällskap. 

Jag hade förväntat mig att det skulle serveras en färdigkomponerad 4-rätters meny, men så var inte fallet. Istället fick man välja mellan 3 olika förrätter, ett antal varmrätter, och ungefär lika många efterräter. Mellan varmrätten och efterrätten skulle ost och kex serveras, vilket alltså gjorde att det blev 4 rätter totalt.

Mitt val föll på toast skagen till förrätt, hängmörad entrecôte med pommes och örtmajonäs till varmrätt, och en efterrätt bestående av choklad, svarta vinbär och mynta. 

Maten var bra, definitivt. Däremot var nog mina förväntningar lite högre, med tanke på att restaurangen är med i White Guide. Förrätten var riktigt bra, men toast skagen är ändå toast skagen. Visst, jag kunde ha tagit något lite mer spännande, så jag får skylla mig själv där.

Varmrätten var också bra. Köttet var till största delar mycket bra, men kryddningen och stekytan var lite ojämn. Vissa delar var väldigt krispiga och smakfulla, med bra kontrast i både smak och textur mellan stekytan och den blodigare delen av köttet. Andra delar saknade dock denna kontrast.

Köttet serverades ovanpå en bädd av grönsaker. Dessa var väldigt fräscha, och speciellt dom solmogna tomaterna var väldigt saftiga och goda. Dom pommes frites som serverades på sidan var dock ganska tråkiga enligt mig, och levde tyvärr inte upp till samma standard som resten.

Det som dock definitivt måste sägas är att portionerna var väl tilltagna. Redan efter varmrätten var jag så mätt att jag knappt kunde förstå hur jag skulle orka efterrätt, än mindre ost och kex, som jag dessutom inte var speciellt sugen på. Jag hoppade därför över osten, och gick direkt på efterrätten som var okej, men lite tråkig.

Servicen var inte fantastisk, och jag tror kockarna hade lite för mycket att göra. Det tog mig nämligen 2 timmar och 40 minuter att bli serverad och äta upp ovan beskrivna mat. Sammanfattningsvis så skulle jag nog ändå rekommendera stället, för maten var bra, men jag skulle inte åka till Nora bara för att äta där om man säger så.

Rejält mätt gick jag sedan tillbaka till hotellet, plockade upp den tvätt jag satt på innan jag gick iväg för att äta middag, och såg till att hänga upp den för tork på rummet. Jag började sedan som vanligt att förbereda allt inför morgondagen, och insåg då med förfäran att jag, när jag planerat rutten 2 månader tidigare, tydligen hade tyckt att det var rimligt att cykla 12 mil nästföljande dag. Det tyckte jag *inte* just då kan jag ju säga.

Med bävan inför morgondagen gick jag och lade mig för att sova.
