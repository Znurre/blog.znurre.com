+++
draft = false
title = "Cykelsemester 2020: Dag 18"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-25"
type = "post"

+++

# Cykelsemester 2020: Dag 18

Jag vaknade återigen upp i gästrummet, den här gången lite mindre utsövd. Kanske var det tanken på att återigen få sätta mig i sadeln som hade stört min sömn, men jag var i allafall förväntansfull inför dagens cykeltur.

Efter att ha ätit frukost gav vi oss iväg, jag och barnen. Klockan var då 10 på förmiddagen. Vi började vår färd längs vackra, lågtrafikerade grusvägar i omgivningen. Precis som dagen innan var stigningen minimal, och vi tog oss fram med en medelhastighet på runt 17-18 km/h medans vi pratade om allt möjligt.

Tanken var att jag, om dom orkade, skulle åka med barnen hela vägen till det ställe där cykelvägen mot Rullsands camping började. Jag tror dock ingen av oss hade förväntat oss att varken Tyr eller Mattis faktiskt skulle orka cykla den 7 mil långa vägen dit.

Ungefär 2 mil senare stannade vi till i det lilla samhället Strömsberg, där vi passade på att köpa lite fika på det cafe som finns där. Jag passade på att äta rejält med mat, eftersom min middag för dagen var bokad till klockan 7, och klockan vid det här laget bara hade hunnit bli lite efter 11. 

Efter ett uppehåll på cirka 1 timme gav vi oss iväg igen, med siktet inställt på Marma. Resan dit gick fortfarande via små, lågtrafikerade grusvägar, och tanken var att vi skulle fortsätta via Upplandsleden när vi väl hade kommit till Marma.

Upplandsleden är egentligen en vandringsled, och även om det inte finns något förbud mot cykling där så ombeds man att inte göra det. Eftersom alternativet hade varit att åka längs högtrafikerade vägar med barnen så ansåg jag leden vara det minst dåliga alternativet, och jag såg även fram mot att få åka längs lite skogsstigar igen, även om jag var lite orolig för att det ojämna underlaget skulle vara dåligt för mina cykelväskor. Som tidigare nämnt hade ju en skruv lossnat vid mitt tidigare äventyr i kuperad terräng.

I Marma stannade vi till ett tag och passade på att gå på toa och äta lite fika för att fylla på energireserverna. Här träffade vi även Jenny och Daniel, som efter lite förseningar hade kommit iväg, och nu var på väg till campingen där dom skulle vänta på Tyr och Mattis. Trots att vi vid det här laget hade tillryggalagt 3.5 mil verkade varken Tyr eller Mattis speciellt sugna på att ge upp och åka med i bilen med föräldrarna, så kort därefter satte vi oss återigen på sadlarna och gav oss iväg.

Upplandsleden började fint, med breda stigar av packad jord och barr, och en och annan rot. Hade jag inte haft mina cykelväskor hade jag nog njutit av cyklingen, för det var riktigt kul att cykla i den något mer utmanande terrängen, och barnen verkade också tycka att det var kul.

Det blev dock svårare och svårare att ta sig fram, och efter en ovanligt brant backe där vi fick gå av och dra cyklarna uppför träffade vi på ett par som vandrade på leden. Dom berättade att det låg en ravin några kilometer längre fram, där dom betvivlade att vi skulle kunna ta oss fram med cyklarna. Så fort möjlighet gavs så svängde vi därför av från leden, ut på en grusväg som vi hoppades skulle leda oss till väg 291 som gick parallellt med leden, och som skulle ta oss till Älvkarleby. Vi cyklade förbi en grupp människor som såg ut att vara klädda för bröllop. Troligen skulle dom ta bröllopsfoton här ute i skogen.

Efter lite gissande lyckades vi hitta ut till den stora vägen, och även om den var högtrafikerad så fanns det väl tilltagna vägrenar som gjorde att vi kunda cykla tryggt dom cirka 5 återstående kilometrarna till Älvkarleby. Det här var första gången jag besökte Älvkarleby, och jag blev imponerad av den mäktiga naturen. Hade jag cyklat ensam skulle jag säkerligen ha stannat till här och beundrat naturen ett tag, men jag märkte hur både Tyr och Mattis började bli trötta vid det här laget. Vår hastighet hade sjunkit ner mot 13 km/h, och om vi skulle komma fram i en rimlig tid var vi tvugna att fortsätta.

Vi följde vägen som gick väster om Dalälven. Den var inte så trafikerad, vilket var trevligt, men den hade ändå en hastighetsbegränsning på 70 km/h, och vägrenen var obefintlig. Jag ville därför komma bort från vägen så fort som möjligt, och analyserade kartan på min cykeldator. Enligt den skulle vi kunna svänga av på en liten skogsväg som såg ut att följa älven, och som skulle ta oss hela vägen bort till bron mot Rullsand. Några kilometer senare så hittade vi skogsstigen, och svängde in på den. Den var lite mer lättcyklad än Upplandsleden som vi cyklat tidigare, men bjöd trots det på en hel del utmaning. Den största utmaningen låg dock i att hitta vart vi skulle. Jag insåg rätt snart att kartan inte överensstämde perfekt med verkligheten, och det blev extra tydligt när vi bara var några hundra meter från vårt mål, då den väg som vi skulle åka inte fanns att finna någonstans. 

Mattis hade redan sedan vi började cykla oroat sig över att han och Tyr skulle sinka mig, och jag kände att den oron började bli alltmer påtaglig när vi irrade omkring där inne i skogen. Jag försökte vara lugn och intala dom båda att det inte var någon fara, att jag skulle komma fram i god tid, men jag började själv känna att tiden rann iväg, och kanske sken det igenom. Till sist hittade vi en skoterled som vi följde, och som tog oss in på den väg vi ursprungligen skulle ha följt. Några minuter senare anlände vi till bron där dom skulle cykla resten av vägen själva till Rullsands camping.

Efter att ha tagit avsked av varandra så gav jag mig iväg igen, och nu hade jag bråttom. Mattis hade gång på gång påpekat att "i den här takten kommer du inte vara framme före åtta!", vilket jag förkastat. Han hade dock helt rätt, och cykeldatorns beräknade ankomsttid som jag försökt ignorera bäst jag kunde, pekade just nu på lite efter klockan 8. Jag visste dock att jag skulle kunna pressa den tiden ganska mycket, med tanke på att den var beräknad på vår medelhastighet hittils, som hade landat på runt 14 km/h.

Jag ödslade dock ingen tid, utan gav allt jag kunde på vägen mot mitt slutgiltiga mål för dagen: Axmarbruk. Inom kort hade jag paserat Skutskär, och var på god väg mot Gävle. Med en medelhastighet på 30 km/h dröjde det inte länge innan även Gävle var avklarat, och jag var återigen ute på landsväg med cirka hälften av etappen kvar. Vid en liten pizzeria stannade jag till för att köpa en Red Bull som jag svepte för att få i mig lite energi, och sedan bar det av igen.

Min beräknade ankomsttid blev rimligare och rimligare, och jag kom på mig själv med att tänka att det var lite som ett spel. Ju bättre tid, desto fler "perks" skulle jag låsa upp. När ankomsttiden började peka på 18:50 började jag tänka att "okej, nu hinner jag kanske checka in och lämna cykeln vid mitt boende innan middagen", vid 18:40 kunde jag unna mig att byta om innan middagen, och efter att ha lyckats pressa ner ankomsttiden till 18:30 kunde jag till och med unna mig en snabb dusch. I slutändan anlände jag 18:20, betydligt bättre än vad jag hade kunnat förvänta mig.

Jag hade satt mobilen i flygläge under min vansinnestur för att inte bli störd, och för att kunna fokusera 100% på cyklingen, så när jag kom fram och slog av flygläget upptäckte jag att jag hade flera missade samtal från ett okänt mobilnummer. Det visade sig vara ägarinnan till det Bed & Breakfast där jag skulle spendera natten. Hon hade blivit orolig när hon inte kunde komma i kontakt med mig, men vi klarade snabbt upp situationen, och hon kom ut för att möta mig och hjälpa mig att låsa in cykeln i ett förråd på baksidan av gården, varefter hon visade mig till mitt rum.

Det var ett jättefint och mysigt ställe, men jag ödslade ingen tid, utan skyndade mig att duscha och byta om. Med god tidsmarginal gav jag mig iväg mot Axmar Brygga där jag hade bokat bord för mig och min kompis h4xxel. Han har sedan en tid tillbaka bosatt sig i Axmar, så det passade verkligen bra. Efter att ha mötts upp på parkeringen gick vi in för att anmäla vår ankomst och bli ledsagade till vårt bokade bord. Jag hade aldrig ätit på Axmar Brygga tidigare, men Jenny hade talat mycket gott om det, så mina förväntningar var höga. Av vad jag har förstått så ska deras fiskrätter vara väldigt bra, men jag kunde inte motstå en av deras köttbaserade trerättersmenyer som verkade väldigt god. 

Jag blev inte besviken. Maten höll mycket hög kvalitet, och var nog topp 3 under hela resan hittils. Dessutom uppskattade jag att få middagssällskap, och det var kul att träffa h4xxel för första gången på flera månader. 

När vi kände oss nöjda så åkte jag med honom för att kolla på hans nya hus. Han hade börjat renovera i princip hela huset invändigt, så det var minst sagt ganska kaotiskt. Det var dock uppenbart att det fanns stor potential i huset och att det skulle bli väldigt fint när det väl var färdigt. Efter husesynen var h4xxel snäll nog att skjutsa mig till mitt boende. Klockan hade hunnit bli 9 vid det här laget, och dagen hade verkligen tärt på min energi. Efter att ha planerat för morgondagen gick jag och lade mig och somnade som en stock.
