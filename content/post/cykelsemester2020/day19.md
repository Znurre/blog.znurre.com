+++
draft = false
title = "Cykelsemester 2020: Dag 19"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-26"
type = "post"

+++

# Cykelsemester 2020: Dag 19

Så var det dags för den sista dagen av min cykelsemester. Det kändes minst sagt overkligt, och lite vemodigt.

Efter att ha sminkat mig och klätt på mig gick jag ner till frukosten. Den serverades vid bordet, och det var bara jag och ett annat par i restaurangen. Vi pratade lite, mestadels om covid19, men även lite om utflyktsmål. Dom skulle tydligen spendera en dag i Hälsingland, och ville hinna se så mycket sevärdigheter som möjligt.

När frukosten var avklarad gick jag upp och packade som vanligt, och gjorde mig redo för avfärd. Vid utcheckningen blev jag stående ett bra tag och pratade med värdparet som verkligen var jättetrevliga. Vi stod säkert och pratade i 40 minuter innan jag var tvungen att ursäkta mig. Jag hade nämligen en tid att passa. Jimmy och Calle hade gått med på att möta upp mig, och vi hade planerat att träffas på Borka Brygga, där jag hade bokat bord för oss tre klockan 14:00.

Jag gav mig alltså iväg, ut på landsvägen igen. Första samhället jag åkte igenom var Ljusne, där jag även passade på att stanna till på en mack för att köpa energidryck. Vid det här laget hade klockan hunnit bli nästan 12, och med 5 mil kvar skulle det bli på håret. Jag bannade mig själv för min ständiga tidsoptimism och ställde in mig på en repris av gårdagen. Trots trötta ben gick det faktiskt riktigt bra, mycket tack vare en svag medvind. Jag lyckades hålla en medelhastighet på runt 27 km/h, vilket gjorde att jag anlände till Borka Brygga med god marginal, där jag satte mig vid vårt bokade bord och beställde in en alkoholfri cider i väntan på Jimmy och Calle.

Till skillnad från mig så blev dom faktiskt försenade, och anlände ungefär 20 minuter senare än avtalad tid. Vid det laget hade jag hunnit dricka upp 2 cider. Vi beställde in mat, och jag valde att beställa en trerättersmeny som vanligt, medans Jimmy och Calle bara beställde varsinn varmrätt. Maten var grym, och återigen fick jag revidera min topp 3-lista. Dessutom var det fantastiskt att åter träffa Jimmy och Calle efter att inte ha sett dom på över 2 veckor.

Vi åt och pratade ett bra tag innan vi betalade för oss och gav oss av hemåt. Det kändes overkligt att åka på dom välbekanta vägarna igen, och jag tror aldrig det kännts så bra att cykla gamla E4an. Det gick dock inte snabbt. Calle är inte en speciellt van cyklist, och han fick ont i nacken på hemvägen. I slutändan höll vi en medelhastighet på runt 15 km/h, vilket bara var marginellt snabbare än vad Tyr och Mattis hade hållit dagen innan.

Jag hann i princip bara komma innanför dörren hemma och snabbt byta om innan jag var tvungen att kasta mig på cykeln igen för att åka ner till Stadstaket där jag och Klara skulle äta middag tillsammans. Det blev ett fint avslut på dagen, och på denna resa som tagit mig genom halva Sverige, bokstavligt talat.
