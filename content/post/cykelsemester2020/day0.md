+++
draft = false
title = "Cykelsemester 2020: Dag 0"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-06T13:07:47+02:00"
type = "post"

+++

# Cykelsemester 2020: Dag 0

Ja, då sitter jag här och väntar på att åka iväg. Allting är packat och förberett in i minsta detalj. Det enda som återstår är att jag ska sätta på mig cykelkläderna, packa ner laptopen och montera cykelväskorna på cykeln.

Det är med en känsla av overklighet jag inser att det är idag jag ska åka, och att det enda som avgör när jag åker är att jag ska anse mig färdig. Sedan bär det av, i tre veckor.

Jag hade nog inte riktigt insett hur lång tid det egentligen är förrän jag började packa lite smått igårkväll, och reagerade på hur mycket av min medicin jag var tvungen att ta med mig. Först då blev det så otroligt konkret. Det var även då jag insåg att jag nog aldrig varit borta från min lägenhet så här länge.

Vad ska jag då göra under dessa tre veckor? Mestadels cykla, som ni säkert redan gissat. Planen är att ta sig härifrån Hudiksvall, ner till Malmö via Dalarna och Örebro, och sedan tillbaka igen via Enköping. Total distans: cirka 200 mil.

Ursprungsplanen var att åka härifrån ner till Kroatien, och föddes under 2019. Jag känner en hel del folk i Europa, och har länge haft en ambition att resa och hälsa på dom, men det har aldrig blivit läge. 2019 bestämde jag mig dock att det äntligen skulle bli av sommaren 2020. Från början var tanken att göra det till en tågsemester i Europa, dels för att få möjlighet att se och uppleva mer än när man flyger, men även såklart för att hålla nere mitt klimatavtryck.

Det var min systers dåvarande pojkvän som väckte tanken om cykelsemester hos mig. Vi diskuterade mina semesterplaner, och han frågade om jag inte funderat på att cykla sträckan istället, jag som gillade att cykla.

För dom av er som inte vet så har jag varit ute på längre cykelturer tre gånger tidigare. Två gånger i Japan, och en gång i Spanien. Samtliga av dessa cykelturer har varit helt fantastiska. Speciellt den första av cykelturerna i Japan hör till något av det bästa jag någonsin gjort.

Med tanke på detta var förslaget inte alls speciellt underligt, och jag nappade direkt. Det skulle dock dröja ett bra tag innan jag faktiskt började planera för resan. 

I slutet av januari 2020 började jag för första gången knåpa ihop kartan för min resa. Till min hjälp använde jag bikemap.net som kan optimera en rutt för cykel utifrån ett antal angivna punkter, i mitt fall dom platser där jag kände folk som bodde. Jag insåg dock ganska snabbt att det skulle bli orimligt att faktiskt besöka alla jag kände i Europa, eftersom dom bor så pass utspritt. Istället valde jag att skapa ut en rutt som gick från Danmark, genom östra Tyskland, Tjeckien, östra Österrike för att därefter ta en liten omväg till Slovenien innan jag slutligen skulle anlända till min slutdestination, Zagreb i Kroatien.

Jag importerade sedan den genererade rutten till Google My Maps där jag markerade ut lämpliga hotell längs vägen. Med tanke på ruttens längd bedömde jag att cirka 10 mils cykling per dag skulle vara en lämplig distans. På så sätt skulle resan i sin helhet ta cirka 20 dagar, vilket skulle ge mig marginal att stanna längre på utvalda ställen. Till exempel planerade jag att göra ett längre stopp i Tjeckien där jag känner en kompis som jag tidigare har bott hos.

Det fanns bara ett litet problem. Jag hade aldrig cyklat 10 mil. Det längsta jag cyklat var 9.5 mil en dag i Spanien, och då hade majoriteten av sträckan varit en enda nedförsbacke. Om jag skulle klara det jag föresatt mig behövde jag alltså träna, och jag behövde en ny, bättre cykel.

I februari köpte jag därför, efter att ha kollat runt lite, en Trek Checkpoint 5 ALR. Det är en s.k. "gravel bike", vilket innebär att den ser ut ungefär som en vanlig landsvägscykel, fast med grövre däck som tillåter bekväm och säker cykling på grusväg och finare skogsstig. Jag såg även till att få en pakethållare monterad, så att jag skulle kunna hänga på cykelväskor.

Med tanke på den minst sagt milda vintern kunde jag börja cykla på den redan efter någon vecka, och sedan dess har jag legat i relativt konstant träning, med mitt mål satt på att cykla i snitt minst två mil per dag. Idag kan jag stolt säga att jag klarat mitt mål med god marginal. Tre mil per dag blev det i snitt hittils under 2020. I detta finns även inräknat ett antal 7 mils-turer, en 10 mils tur, samt en 14 mils tur! Totalt cirka 230 mil.

![Min kondition över tid](/fitness.png)

Denna träning skulle förhoppningsvis ha varit tillräcklig för att kunna möta dom utmaningar som min planerade Europa-resa hade kunnat tänkas bjuda på. Nu förändrades ju dock världen ganska radikalt på bara några veckor i och med covid19, och någon gång under mitten av maj insåg jag att jag nog borde överväga att antingen slopa mina planer helt, eller att fundera på om jag kunde minska mina ambitioner något och bara cykla inom Sverige istället. När sedan beskedet kom att avrådan för inrikes resor skulle hävas den 13e juni bestämde jag mig för att satsa på en Sverige-resa istället, och började planera min nya rutt. 

Även om jag inte hade möjlighet att träffa mina europeiska vänner som jag föresatt mig så öppnades några andra möjligheter istället. För min Europa-resa hade jag planerat att åka till Helsingborg, där jag skulle ta cykeln med mig på färjan över till Helsingør. Nu hade jag dock chansen att istället fortsätta neråt, till Malmö, där min storasyster på pappas sida bor. På så sätt kunde jag få möjligheten att spendera lite kvalitetstid tillsammans med henne, och jag skulle även få någonstans att bo dom två nätter det handlade om.

På vägen hem skulle jag dessutom också få möjlighet att träffa folk jag kände. En stor fördel med att cykla inom Sverige är ju nämligen att jag faktiskt skulle kunna ta mig hem igen på ett enkelt sätt. Detta hade varit en stor osäkerhet för mig när det gällde min ursprungliga plan: Hur tar man sig på bästa sätt hem med en cykel till Sverige från Kroatien?

Med min nya, reviderade plan, skulle jag dock kunna ta mig hem igen på ungefär samma distans som den totala resan ner till Kroatien hade inneburit, vilket såklart var ett stort plus. Jag började således fundera på vilka personer jag skulle kunna besöka på resan hem.

Resan hem skulle gå närmare östkusten, det var något jag redan tidigt hade bestämt. Det innebar att kandidater för folk att träffa var personer som bodde i Uppsala- och Stockholmstrakten. Steven som jag åkte till Japan tillsammans med förra året bor i Rimbo, och jag planerade in i det längsta för att stanna till där.

I slutändan valde jag dock att inte göra det, av fyra anledningar:

* Att åka till Rimbo skulle innebära en relativt stor omväg. Om möjligt föredrog jag att hålla mig lite längre in i landet, så att jag kunde åka rakt norrut för att ta mig hem.
* Det rimligaste sättet att ta sig dit skulle ha inneburit att jag hade behövt traggla mig igenom Stockholms innerstad, och det var ingenting jag var speciellt sugen på.
* Vägarna där omkring är ganska otrevliga att cykla. Dom är smala, utan väggrenar, och är ganska tungt trafikerade.
* Det passade inte speciellt bra rent distansmässigt.

Istället så valde jag att ta vägen över Enköping. Det skulle även tillåta mig att stanna till vid min storasyster på mammas sida, som bor utanför Tierp, utan att behöva ta någon direkt omväg.

Med rutten utstakad så började jag så att leta efter boenden, med varierande framgång. På vägen ner var det av någon anledning lättare att hitta boende än på vägen hem. Speciellt trakterna runt Flen visade sig vara närmast omöjliga att hitta ett vettigt boende i.

I Flen finns det t.ex. ett enda hotell, "Hotell Loftet", som kan skryta med följande smickrande recensioner:

> Fruktansvärt äckligt hotell. Smutsigt, trasigt, luktar illa, flugor i korridoren, dammiga hotellrum med random skvätt på speglarna. Dånande trasig AC, smutsigt akvarium i receptionen. Kablar som sticker ut här och där, trasig inredning. Undvik.

> Möglig frukost. Brödet var grönt!!

Det visade sig även, covid19 till trots, vara otroligt fullbokat överallt! I vissa fall lyckades jag endast få tag på ett rum tack vare att jag ringde. Tydligen verkar det vara rätt vanligt att boenden inte lägger ut alla sina rum för bokning online. I andra fall var samtliga rum uthyrda, och jag fick då söka mig till andra boenden istället.

Allt detta till trots tycker jag mig dock ha fått tag på trevliga boenden i princip överallt. I många fall har jag kunnat välja boenden med lite karaktär. Ofta familjedrivna, mindre boenden. Dom flesta inkluderar även middag som är producerad av lokala, närodlade ingredienser.

Efter att ha cyklat på vägar i Hudiksvallstrakten ska det bli fantastiskt att få ge sig ut och se lite nya vyer. Jag känner mig förväntansfull, men även lite orolig. Kommer jag klara av den påfrestning som den här resan kommer innebära på min kropp?

Fortsättning följer...
