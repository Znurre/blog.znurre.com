+++
draft = false
title = "Cykelsemester 2020: Dag 9"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-14"
type = "post"

+++

# Cykelsemester 2020: Dag 9

Jag vaknade upp och åt frukost efter sedvanliga förberedelser. Frukosten här serverades som en buffé, och jag blev inte förvånad över att den höll hög kvalitet, precis som allt annat där.

När jag hade ätit mig mätt och belåten gick jag till mitt rum för att förbereda för dagens cykeltur, som återigen slog rekord i distans. Idag skulle jag nämligen cykla hela 16 mil, dock med mindre stigning än dagen innan. Med tanke på att jag hade haft problem med 14 mil var jag inte helt säker på hur 16 mil skulle gå. Dessutom var bentröttheten och ömmandet tillbaka efter dom rejäla backarna dagen innan.

Jag pratade ett tag med hon som drev stället vid utcheckningen, och gav mig sedan iväg. Första biten till Halmstad gick längs banvall, och var därmed väldigt lättcyklad. Det verkar som om det finns mycket cykelväg i form av banvall här nere. 

Även om jag aldrig åkte in i Halmstad så fick jag en känsla av stadens cykelinfrastruktur när jag kom in i stadens utkanter via banvallen. Där fortsatte jag sedan in på en annan cykelled via tillgänglig skyltning. Det stämde inte helt med den planerade rutten, men det var trevliga cykelbanor och jag kunde se att jag skulle komma fram till rätt ställe i slutändan. 

Jag kom fram till en viadukt där jag egentligen skulle ha svängt upp på en cykelbana som gick längs en större, högtrafikerad väg. Istället valde jag att följa något som hette "kattegattleden". Jag hade ingen aning om vad det var för led, men jag kunde se att den skulle gå ihop med min planerade rutt längre fram. 

Återigen, det kanske inte låter speciellt smart att ta en omväg längs en okänd cykelled när man redan är osäker på om man kommer orka med distansen, och med regn hängande i luften, men jag hade redan blivit kallad galen över den här cykelturen ett antal gånger, så jag tänkte att det väl var lika bra att löpa linan ut. Dessutom är det precis som dom säger, att distansen ligger minst lika mycket i psyket som i kroppen. Det är något som jag verkligen blivit varse under dom här dagarna hittils. En distans på 2 mil kan kännas evighetslång, och en distans på 8 mil kan kännas som 4, så även i detta fall.

Cykelleden gick växelvis på skogsstig, grusväg och asfalt, och hela tiden med stor variation i vegetation och omgivning. Ibland ledde den mig genom täta skogsdungar, och ibland längs kattegatts stränder med utsikt över sundet. Jag fick även se naturbetande getter och får i det stundvis ganska karga landskapet. Det var helt enkelt en riktigt häftig led, och jag kom på mig själv med att ha riktigt kul när jag cyklade den, helt i motsats till föregående dags monotona granskog.

Så efter ett tag kom jag tillbaka på den planerade rutten, som delvis följde kattegattleden. Här gick rutten längs E20 en bit, med dom för- och nackdelar som det innebär. En av fördelarna var den rastplats som fanns i höjd med La Holm, där jag stannade för att äta grillad korv med pommes på Cirkle K. Efter det stoppet fortsatte kattegattleden med lite annan sträckning än den ursprungliga rutten, som snarare följde E20. 

Det var nu jag gjorde en stor felbedömning, i och med att jag valde att följa kattegattleden. Jag tänkte att det väl knappast kunde vara så stor skillnad, att den bara gick lite närmare kusten, o.s.v. Så jag fortsatte helt enkelt att ignorera min cykeldator som klagade på att jag skulle ta mig tillbaka till den inprogrammerade rutten, och körde på längs den skyltade kattegattleden istället. 

Leden tog mig upp för *världens* värsta backe, utan tvivel den värsta hittils, med hela 10% lutning. Faktum är att jag inte är säker på om jag hade orkat cykla upp för den ens om jag varit utvilad och inte haft någon packning, så att cykla upp för den under dom här omständigheterna var det inte ens tal om. Jag kapitulerade därför, och drog cykeln uppför, vilket bara det var otroligt jobbigt i den extrema lutningen.

Hade min geografi eller uppmärksamhet varit bättre så hade jag insett att kustlinjen inte är helt rak, utan att det går ut ett antal uddar längs kattegatt. När man tittar på cykeldatorns karta i inzoomat läge så inser man inte det, för den visar ju alltid den riktning man är på väg mot som uppåt. D.v.s, oavsett om man åker norrut, söderut, västerut eller österut, så åker man uppåt på kartbilden.

Jag fortsatte alltså längs kattegattleden, och njöt till fullo. Det var verkligen en underbar led, och dom 6.5 mil som jag vid det laget hade lagt bakom mig kändes betydligt kortare. Kort sagt, jag hade riktigt kul. Det hade kommit en eller annan skur, men solen hade inte varit sen med att titta fram och torka mig.

Efter ett tag började jag dock inse att någonting inte stod helt rätt till. Jag hade tidigare sett en skylt som visade att det skulle vara närmare 10 mil till Helsingborg via cykelleden, vilket verkade otroligt långt, men inte reagerat närmare vid det tillfället. Jag tror det var cykeldatorns beräknade ankomsttid som fick varningsklockorna att ringa, för helt plötsligt visade den på en tid som var flera timmar senare än den ursprungligt beräknade tiden. Det var då jag zoomade ut, och insåg att jag leden tagit mig ut på en av dessa uddar, på god väg mot Torekov.

Tjurig som jag är så valde jag trots det att fortsätta på den inslagna vägen. Dels var den, som tidigare nämnt, betydligt roligare att cykla, men jag kände även att det skulle ha varit att ge upp att bara återvända direkt till den planerade rutten. Istället så fortsatte jag, och stannade till vid Vejbystrand där jag åt 2 belgiska våfflor med sylt och grädde runt kl 4 på eftermiddagen.

När jag började närma mig Ängelholm var jag dock tvungen att göra ett val. Antingen skulle jag fortsätta längs kattegattleden, vilket skulle ta mig ut även på nästa udde, eller så skulle jag vika av upp mot Ängelholm för att återgå till den planerade rutten. Min planerade ankomsttid vid det här laget pekade redan på 21.30, och det var under förutsättningen att jag gick tillbaka till den planerade rutten. Skulle jag fortsätta längs kusten hade jag nog fått räkna med en ankomsttid runt 23.30 som tidigast.

Det smärtade att behöva vika av från den vackra leden, men jag insåg att det enda rimliga skulle vara att ta den planerade rutten. Lisa väntade på mig i Malmö, och med mitt tidigare avsteg hade jag utökat den redan då tuffa distansen 16 mil till närmare 20 mil. Att åka ut på nästa udde skulle ha ökat på denna distans med gissningsvis 5 mil till.

Jag lät därför min cykeldator guida mig tillbaka till den planerade rutten, genom Ängelholm och upp på en cykelväg längs riksväg 117. Det var en riktigt tråkig väg, och det kändes endast som transportsträcka. Först nu började jag ångra att jag inte tagit den planerade rutten från början, för hade jag gjort det hade jag redan varit förbi Helsingborg vid det här laget.

Cykelvägen tog mig in genom Helsingborg, och jag har nog aldrig behövt min cykeldator så mycket. Nätverket av cykelbanor var enormt, och utan cykeldatorn hade jag inte haft någon aning om vilka jag skulle följa. Jag spenderade förvisso en relativt kort tid i Helsingborg, men jag fick uppfattningen av att staden verkligen är en förebild vad det gäller infrastruktur för cykling. Cyklisterna är verkligen "first class citizens", och planeringen och utformandet av cykelbanorna är otroligt väl utförda. Jag tror inte att jag vid ett enda tillfälle behövde gå av cykeln för att leda den över en väg eller liknande.

Det tog mig närmare 20 minuter att ta mig igenom Helsingborg, men till sist så var jag ute på andra sidan och befann mig på en cykelled som följde kusten. Den verkade stämma bra överens med min inprogrammerade rutt, så jag släckte skärmen på cykeldatorn och följde skyltningen.

Leden var fantastiskt fin, och följde kusten, ungefär på samma sätt som kattegattleden hade gjort. Klockan var vid det här laget efter 7 på kvällen, och jag kunde se en begynnande solnedgång över Danmark på andra sidan sundet.

![Solnedgång över Danmark](/solnedgang.jpg)

I höjd med Landskrona kom dagens andra skur, och jag tror att det var närmare hagel än regn, för det som träffade min hud kändes snarare som hårda iskristaller än vatten. Kalla mig konstig, men jag tyckte faktiskt att regnet var otroligt uppfriskande. Jag cyklade genom det medans jag lyssnade på trevlig musik, och bara njöt. 

Ni kanske undrar hur mina cykelväskor mådde vid det här laget. Det var ju ett tag sedan jag beklagade mig över dom nu. Mirakulöst nog hade dom klarat sig så här långt, och höll fortfarande ihop. Däremot envisades dom med att konstant hasa bakåt och misshandla mitt stackars baklyse. Lite då och då kunde jag höra ett högt brak genom musiken, som signalerade att väskorna hade åkt bak och lagt tyngden på baklyset, som då tvingats isär. Det var ett tecken på att det var dags att stanna till, försöka lyfta tillbaka cykelväskorna i rätt position, och trycka ihop baklyset igen. Så fortsatte det, gång på gång.

Som tur var skulle detta bli sista dagen med dessa cykelväskor. När jag anlände till Ulricehamn hade jag nämligen tröttnat, och beställt ett par på internet med expressfrakt till Lisas adress i Malmö, och dom låg nu och väntade på att hämtas ut av mig.

Cykeln i sig mådde också mindre bra. Vid det här laget hade den tagit mig närmare 100 mil utan någon typ av service, och kedjan hade börjat låta mer och mer för varje dag. Den skulle behöva sig en dos kärlek i form av kedjerengöring och smörjning, vilket jag också planerat under mitt stopp i Malmö.

Med en sista kraftsamling tog jag mig dom sista 2-3 milen genom Bjärred och Lomma, in i Malmös ytterkanter. Jag följde sedan cykeldatorns anvisningar för att hitta till Lisas adress. Vid det här laget hade jag varit ute väldigt länge, och jag var orolig för att cykeldatorns batteri inte skulle räcka, men det gjorde det.

När jag kom fram till Lisas adress var klockan precis 21.30. Inte illa. Lisa kom ner och släppte in mig, och vi hjälptes åt att bära upp cykeln till hennes lägenhet. Där tog jag sedan en varm dusch, medans hon kokade pasta och värmde på en pastasås med färsk salsiccia.

Vi satt sedan och åt och pratade i cirka 2h. Jag åt 3 portioner av maten, som var fantastiskt god. Min cykeltur hade varit krävande, och jag hade bränt totalt 3800 kcal, alltså nästan 2 dagars intag av kalorier.

Runt midnatt gick jag och lade mig i den säng som Lisa hade bäddat åt mig, och jag somnade som en stock.
