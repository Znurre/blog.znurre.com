+++
draft = false
title = "Cykelsemester 2020: Dag 1"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-06T14:07:47+02:00"
type = "post"

+++

# Cykelsemester 2020: Dag 1

Efter att ha ätit en stadig lunch packade jag ner allt och dubbelkollade mina checklistor för att vara helt säker på att jag inte glömt någonting. Imponerande nog fick även allting plats i mina cykelväskor.

![Packning](/cykelpackning.jpg)

Jag monterade sedan packningen på cykeln och gav mig iväg hemifrån runt klockan 1, med siktet inställt på mitt första boende på resan: Orbaden Spa & Konferens.

Det är egentligen för kort distans, endast 6 mil, men jag hade hört så mycket bra om stället att jag kände att jag behövde uppleva det själv. Därför gav jag mig ut, på den väg som jag vid det här laget har åkt otaliga gånger, nämligen vägen mellan Hudiksvall och Njutånger. Med tanke på att jag åkt den så pass många gånger kändes den mest som transportsträcka, och jag försökte lägga den bakom mig så fort jag kunde. 

Det som däremot var nytt var känslan av att cykla med så pass tunga cykelväskor. Jag hade redan tidigare testcyklat med cykelväskor, dock inte en sån här lång distans, och inte med i närheten av så här mycket packning. Skillnaden i motstånd var enorm, och min puls låg rejält mycket högre än vad den brukar göra på samma sträcka. Kanske har det dock mest att göra med att jag haft ett träningsuppehåll på cirka en vecka innan den här resan. Efter ett tag kändes det nämligen som om jag "kom in i det", och cyklingen gick lättare efter det.

Vädret var tråkigt fram till Njutånger. Molningt och halvkallt, med en känsla av regn hängande i luften. När jag sedan efter några mil svängde av mot Njutånger och vägen till Nianfors så kom det faktisk lite regn. Det var dock så pass lite att det knappt ens kan kallas duggregn, och jag behövde inte använda mig av regnjackan.

Vägen mot Nianfors fortsatte utan några speciella överraskningar. Det är inte heller en speciellt upphetsande väg, även om vissa delar av sträckan är ganska fina där den åker längs med sjön Lill-Nien.

När jag hade åkt ett par mil på den vägen så började plötsligt solen att bryta igenom molntäcket, och det var som en varm omfamning i kontrast till det tidigare vädret. Och så, lika plötsligt, hade jag redan tillryggalagt hälften av dagens distans! 

Ytterligare några mil senare kom jag till den korsning där man antingen kan ta höger för att komma ut i Järvsö, eller vänster för att komma till Vallsta och därmed Orbaden. Detta vägskäl representerade även slutet på den väg jag kände sedan tidigare. Från och med denna punkt var det helt okänd väg, och plötsligt kom äventyrskänslorna över mig.

Jag cyklade den sista, korta distansen ner till Orbaden. Vägen dit var kantad med fält fulla av gula blommor som kanske kan ha varit raps, och fick det hela att kännas otroligt idylliskt.

Väl framme vid Orbaden checkade jag in, och fick hjälp att låsa in min cykel. Jag gjorde mig sedan hemmastadd i rummet jag fått, duschade och bytte om, för att sedan gå till middagen.

Middagen bestod av tre rätter, alla helt vegetariska, men med möjlighet för tillval av antingen kött eller fisk. Jag valde dock att köra på förrätten utan tillval. Råraka på rotfrukter med tångrom, lök och någon slags gräddfil med en touch av citron. Den var helt otroligt god.

Varmrätten var också god, men inte fullt lika imponerande. Den bestod av savoykålsrullader med en fyllning gjord på bland annat bönor och chili, och serverades tillsammans med hemmagjord kimchi, primörer och potatispuré. Lite asian fusion alltså. Till varmrätten hade jag valt fisk som tillval, vilket kom i form av halstrad rödingfilé.

Till efterrätt erbjöds en buffé, där jag valde ut en vaniljpanacotta med blåbär som var mycket bra.

Efter maten hade jag tänkt utnyttja spaavdelningen. Främst var jag sugen på att testa utomhuspoolen. Jag hade dock sådan otur att det precis började regna igen, och jag kände mig inte speciellt sugen på att få håret blött, så jag skippade det.

Istället förberedde jag mig för morgondagens cykeltur, och gick i säng i vettig tid för att vara pigg och utvilad dagen efter. Det skulle nämligen visa sig bli en tung andra dag...
