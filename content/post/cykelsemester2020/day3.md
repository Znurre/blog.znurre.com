+++
draft = false
title = "Cykelsemester 2020: Dag 3"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-08"
type = "post"

+++

# Cykelsemester 2020: Dag 3

Denna morgon vaknade jag upp lite mer utvilad än natten innan. Benen kändes fortfarande rejält möra, men jag var vid gott mod. Innan jag gick och åt frukost sminkade jag mig som vanligt, och skickade iväg ett meddelande till nederländaren för att stämma av våra planer.

Vid frukosten upptäckte jag att jag var långt ifrån ensam i matsalen, för hela Carl Larsson-gårdens personal var tydligen på Trollnäs för någon slags rundvandring. Jag tog för mig av frukosten, som var ungefär vad man hade kunnat förvänta sig, och avslutade den rätt snabbt eftersom jag ville försöka komma iväg så tidigt som möjligt.

Det var nämligen så att väderleksprognosen visade på regn. Frågan var väl kanske snarare hur mycket regn det skulle bli, och exakt när det skulle falla. Alla tjänster var oense, och ingen prognos verkade säker. Jag valde dock att lita på SMHI, vars prognos visade att det kunde finnas en chans att slippa regn iallafall fram till Falun om jag kunde ta mig dit inom någon timme.

Jag och nederländaren kom sedan överrens om att vi skulle slå följe till Falun, där vi planerade att äta en gemensam lunch. Han började packa ihop sitt tält, och jag checkade ut, packade cykeln och åkte ner till stranden där jag varit kvällen innan för att vänta på honom.

Det visade sig att jag inte var ensam där. En man stod på parkeringsplatsen med sin bil, och vi hejade på varandra, vilket sedan startade en lång konversation. Nederländaren tog god tid på sig, vilket tillät oss att avhandla många olika ämnen. Allt ifrån filosofi och målsökande attackrobotar till programmering och illuminati. 

Till sist dök dock nederländaren upp, och vi cyklade iväg mot Svärdsjö där vi stannade till vid ICA så att jag kunde få i mig dagens första obligatoriska dos av koffein. 

Vädret hade nu slagit om från att ha varit soligt på morgonen till att vara molning och blåsigt, och jag hade blivit rejält nedkyld under tiden jag stod och pratade med mannen på stranden. Jag fick därför för första gången på resan användning för mina isolerade sleeves för benen som jag tagit med mig. Det är som två knäsockor, men utan själva strump-delen, som man trär över benet och över cykelbyxorna. På så sätt kan man enkelt förvandla ett par korta cykelbyxor till långa.

I Svärdsjö stod vi sedan inför ett vägval, bokstavligt talat. Antingen kunde vi åka vidare rakt söderut till Falun, eller så kunde vi ta av till öster över Toftbyn och Sundborn. Det senare alternativet skulle innebära en 8 km omväg, men Anna på Trollnäs hade talat mycket varmt om hur vacker den vägen var, och därför valde vi att ta den trots allt. 

Efter bara några hundra meter förstod vi varför. Vägen slingrade sig fram mellan grönskande kullar, bondgårdar och små röda hus. Det var helt enkelt otroligt pittoreskt och idylliskt. Vädret bjöd dessutom på uppehåll, vilket gjorde vår cykeltur mycket njutningsbar.

Tiden flög iväg, och snart var vi framme i Sundborn. Här började ett försiktigt duggregn, men vi valde trots det att stanna till och kolla på Carl Larsson-gården och dom vackra omgivningarna.

![Sundborn](/sundborn.jpg)

Vi åkte sedan längs med vattnet, ner mot Falun, och hittade så småning om på en fin, grusad cykelväg som vi kunde cykla hela vägen till Hosjö Industriområde. Därefter tog vi oss ner till vattnet, och följde sedan promenaden där hela vägen in till centrum. 

Vädergudarna verkade fortfarande ha svårt att bestämma sig för vilket väder det skulle vara. Under tiden som vi cyklat från Sundborn till Falun hade det återigen hunnit bli soligt, för att sedan den sista biten in mot centrum växla till att regna i kombination med kraftig blåst.

Vi tog oss, blöta och kalla, in till stora torget i Falun, där vi låste fast cyklarna och tog med oss cykelväskorna in till en restaurang vid namn Gott & Reco. Jag hade spanat in den redan dagen innan, eftersom jag visste att nederländaren var vegan, och dom hade veganska alternativ av i princip alla rätter på menyn.

Jag beställde en älgfärsburgare, medans han körde på bakad sötpotatis med en vegansk skagenröra gjord på tofu. Burgaren var jättegod, och vi satt där och åt och pratade tills vi kände oss torra och varma igen. 

Innan vi lämnade restaurangen stämde vi av våra respektive planer för dagen. Han hade som mål att ta sig till en stuga på ett berg väster om Borlänge, medans jag skulle fortsätta söderut, men vi bestämde oss för att slå följe så långt vi kunde.

Vi cyklade tillbaka till promenaden, och följde den tills vi kom in på Runnleden, en gång- och cykelled som går längs sjön Runn. Den var inte asfalterad, men det var en fin grusväg, och tack vare begränsad stigning var den väldigt lättcyklad. Dessutom var omgivningarna mycket vackra, och avsaknaden av biltrafik i närheten kändes som en befrielse. Några djur vågade sig fram, och vi såg både rådjur och ekorre. 

Lite längre fram blev vägen asfalterad, och fortsatte då som liten, lågtrafikerad bilväg, genom små pittoreska samhällen. Ytterligare något senare kom vi ut på Sverigeleden. Det var någonstans här jag började inse att jag följt den rätt länge nu, faktiskt hela vägen genom Dalarna. 

Just den del av Sverigeleden som började här efter Falun var riktigt bra. Den erbjöd separat cykelväg med fint underlag som gick parallelt med bilvägen. 

Nederländaren höll ett betydligt lägre tempo än vad jag normalt skulle ha hållit. Det var skönt, för jag har svårt att hålla tillbaka på takten när jag cyklar själv. Däremot började jag bli lite orolig för att jag inte skulle hinna till hotellet i tid för middag. Jag hade inte heller bokat någon middag för den här kvällen, utan räknade med att kunna inta den i den restaurang som fanns i samma lokaler.

För att stilla min oro ringde jag hotellet som bekräftade att köket höll öppet fram till kl 8, och att det inte borde vara något problem att få ett bord i restaurangen. Det lugnade mig en del, men jag hade fortfarande ett orosmoment kvar. Min cykeldator började nämligen varna för låg batterinivå, och utan den skulle jag få betydligt svårare att hitta till mitt boende. Jag såg därför till att cykla med skärmen avstängd så mycket jag kunde.

Efter några mil kom vi så till den del av vägen där nederländaren skulle svänga av, och vi sa hej då och önskade varandra lycka till. Därefter stålsatte jag mig inför dom sista 3 milen av sträckan som var den del jag bävat inför. Den bjöd enligt höjdkurvorna på lika mycket stigning som resten av turen tillsammans. Vinden var dock ytterst gynnsam, och jag gjorde bättre ifrån mig än vad jag hade kunnat drömma om. Faktum är att det gick så pass bra att jag lyckades pressa ner tiden för beräknad ankomst från 18:00 till 17:37.

Väl framme vid hotellet blev jag dock bestört och besviken över att se att den magnet som sitter på en eker i hjulet, och som används för att rapportera varvtal till cykeldatorn, delvis hade lossnat. Det gjorde att ingen rotation rapporterades till cykeldatorn, och den hade då pausat inspelningen av aktiviteten. Dom sista 5 milen av mina totalt 10.5 mil saknades alltså. Jag försökte att skjuta bort tankarna på det, och gick istället för att checka in. 

Hotellet jag hade valt hette Villa Gladtjärn, och låg vackert vid stranden av en sjö som jag antar heter Gladtjärn, cirka 8 km från Smedjebacken. Det var ett mysigt litet ställe, och hade nästan lite fjällhotellskänsla. 

Efter att ha fått tillgång till mitt rum tog jag en varm dusch, och bytte sedan om för att gå till restaurangen och äta middag. Restaurangen betjänades av samma kille som stått i receptionen och hjälpt mig checka in. Jag tog ett bord, och han visade mig menyn i form av två stycken griffeltavlor. 

Många av alternativen kändes lockande, men jag föll trots allt för den kanske mest anspråkslösa rätten av dom alla - "Korv med pommes". Till mitt försvar var det kabanos, och rätten gav mig precis det jag behövde efter en lång cykeltur: fett, salt och protein. Jag avslutade med efterrätt, chokladfondant med grädde och marinerade bär. När man har cyklat en hel dag, och gjort av med mer kalorier än 3 normala portioner mat, ja då kan man faktiskt stoppa i sig precis vad man vill utan dåligt samvete.

Mätt och belåten gick jag till mitt rum där jag som vanligt förberedde rutten inför morgondagen, innan jag till sist gick och lade mig för att sova.
