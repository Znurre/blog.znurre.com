+++
draft = false
title = "Cykelsemester 2020: Dag 15"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-21"
type = "post"

+++

# Cykelsemester 2020: Dag 15

Dagen började som vanligt med frukost, som den här gången serverades i en liten matsal. Det satt redan en del folk där när jag kom dit, och jag reagerade på stämningen, som var långt ifrån livlig. Kanske berodde det på dom restriktioner som hotellet hade lagt på frukosten. Till exempel så var det inte tillåtet för två sällskap eller personer att vistas vid buffén samtidigt, utan man fick ta en plats och vänta tills det var ledigt och man själv kunde gå fram och ta frukost. För att se till att detta efterföljdes fanns i lokalen en frukostvärdinna som höll uppsikt över alla. Inte en speciellt avslappnad frukost alltså, och det märktes på den tunga och dämpade stämningen. Till råga på allt var frukosten väldigt tråkig. Jag skyndade mig att äta upp, och packade sedan och checkade ut från hotellet. 

Dagens tur började med några kilometer längs Göta kanal, och erbjöd både vacker och lättcyklad väg. Vädret var fortsatt vackert, och visade inget spår av gårdagens regnväder. Efter ett tag kom jag till det ställe där vägen korsade kanalen via en sluss, där jag fick kliva av och leda cykeln över. 

![Mem](/mem.JPG)

Därefter fortsatte vägen ner mot Skenäs där jag skulle ta färjan över till andra sidan av den havsvik som sträcker sig in hela vägen till Norrköping. Här fick jag för första gången den här dagen känna på motvind, eftersom min kurs hade ändrats från att åka rakt österut, till att åka mot nordväst. Det blåste en ganska stark västlig vind, så jag fick vinden snett framifrån. 

Jag hade tur, och färjan hade precis anlänt när jag kom fram, och jag kunde genast cykla ombord. Själva turen över vattnet tog bara cirka 5 minuter, men det gav mig iallafall en liten vilopaus. 

![Färja](/farja.JPG)

När färjan anlände till motsatta stranden var jag redo att ge mig iväg, och utan att riktigt tänka efter gav jag mig iväg först av alla. Jag vet inte vad standardförfarandet är i dessa situationer, men såhär i efterhand kan jag säga att jag starkt ångrade att jag hade varit så kaxig och åkt iväg först. Vägen ner till färjan på andra sidan var nämligen så pass smal att det precis fick plats en bil i varje körfält, och i det motsatta körfältet slingrade sig en kö av bilar i vad som verkade vara flera hundra meter. Resultatet blev alltså att jag blev till en extrem flaskhals eftersom ingen vågade köra om mig. I vanliga fall hade detta inte varit något problem, eftersom jag allt som oftast kan hålla 30-40 km/h på plan landsväg, men här sluttade vägen skarpt uppåt i den värsta backen hittills denna dag. Jag försökte så gott jag kunde att trampa på för att inte sinka bakomliggande bilar, och tog ut mig totalt på köpet. Efter några hundra meter tog äntligen kön i det motsatta körfältet slut, och bilarna började köra om mig, en efter en. 

Helt förstörd i benen ställde jag mig vid vägkanten för att vila någon minut, och fortsatte sedan längs vägen där jag snart svängde av in på en mindre väg som gick österut längs vattnet, ut mot ett litet samhälle vid namn Nävekvarn. Här funderade jag ett tag på att stanna till vid något av dom många caféerna, men avstod trots allt eftersom det hade satt käppar i hjulet för min plan för dagen. Jag hade nämligen rekat som vanligt inför dagens tur, och kommit fram till att min sista möjlighet att äta någonting innan jag kom fram till mitt mål för dagen var att äta lunch på en pizzeria i det lilla samhället Jönåker. Den låg bara ett par mil längre fram, och därefter skulle det vara cirka 4 mil utan några restauranger. Därför var det viktigt att jag åt så mycket som möjligt där, så sent som möjligt, så att jag skulle klara mig resten av vägen. Ett stopp här hade alltså varit kontraproduktivt. 

Istället fortsatte jag alltså på den utstakade vägen, som från att ha sluttat nedför nu återigen bjöd på uppförsbackar av en kaliber som mina stackars trötta ben inte uppskattade. Efter ett litet tag kom jag fram till ett vägskäl där jag enligt min planerade rutt skulle vika av på en mindre väg, men den visade sig vara avspärrad med läskiga skyltar som signalerade militär närvaro och att vistelse på området var förenat med livsfara. Det gjorde mig lite ställd, men lyckligtvis kunde jag istället fortsätta på den större vägen ytterligare en bit för att undvika detta hinder. 

Det dröjde inte länge innan jag kom fram till Jönåker, och där sökte jag upp den pizzeria som jag planerat att stanna till vid. Precis när jag parkerade utanför pizzerian så började dagens första regndroppar att falla. Det var inget kraftigt regn, men det kom lite som en överraskning. Jag visste att ett regnväder var på väg inifrån landet, men det borde inte ha kommit redan. Medans jag väntade på den kebabtallrik jag beställt kollade jag därför på väderleksprognosen för att utröna hur dagen skulle utveckla sig. Tydligen var det ett litet, lokalt regnväder just här i Jönåker, så det var ingenting att oroa sig för. Däremot närmade sig även det större regnvädret västerifrån snabbare än vad jag hade väntat mig, och jag insåg att jag skulle bli tvungen att kämpa för att undvika det. 

Efter att ha ätit upp min mat gav jag mig därför iväg igen. Det lokala regnvädret hade gett vika för solen, men jag fruktade att det inte skulle bli långvarigt. Vägen härifrån vek nämligen av åt väster bara några kilometer längre norrut, vilket skulle innebära att jag behövde cykla rakt mot det stundande regnvädret ett tag innan jag återigen skulle kunna svänga av norrut och förhoppningsvis undvika det värsta. 

Den väg jag nu tog skulle nog klassas som en ganska vacker väg av dom flesta. Den slingrade sig fram genom landskapet, med sjöarna Yngaren och Hallbosjön på varsinn sida. Det överhängande hotet om regn i form av mörka moln gjorde dock att jag inte kunde njuta så mycket av den, och konstanta regnstänk påminde mig ideligen om brådskan att ta mig norrut. 

Jag pressade på västerut mot en mycket stark motvind, med byar på upp till 15 m/s. Mina trötta ben jobbade på högvarv, fortfarande inte återhämtade efter backen, och jag kan knappast beskriva det som speciellt njutningsfullt. 

Till sist nådde jag den punkt där vägen återigen vek av norrut, och jag kunde pusta ut. Dels blev jag av med den direkta motvinden, dels visste jag att jag nu lade regnvädret längre och längre bakom mig för varje tramptag. 

Stället jag skulle bo på den här natten var en vingård, belägen en bra bit från närmaste samhälle. När jag därför åkte genom det lilla samhället Bettna, beläget cirka 1.5 mil söder om Flen, passade jag på att göra ett stopp vid den lilla matvarubutik som fanns där. Jag var sugen på något salt efter all ansträngning, och köpte därför på mig en liten påse chips. Till det köpte jag även två burkar Red Bull, varav jag planerade att spara en till morgonen efter. Jag blev minst sagt förvånad när kassören uppmanade mig att visa legitimation för energidrycken. Med en ålder på snart 29 år var det mer än 10 år sedan jag sist behövde legitimera mig för energidryck, men jag antar att man får se det som en komplimang?

Vägen fortsatte genom vackra landskap, och det märktes att jag började komma ut på landet, och därmed även närmare min slutdestination för dagen - Blaxsta Vingård. Dom som känner mig vet att jag inte är något fan av vin, och därför kan det verka underligt att jag valt att boka boende på just en vingård. Till mitt försvar kan jag säga att stället verkade otroligt idylliskt och mysigt, och även stoltserade med en White Guide-listad restaurang som verkade vara i toppklass. Som kuriosa kan nämnas att detta tydligen är Sveriges första, och även världens nordligaste vingård!

Jag anlände till vingården samtidigt som ett annat sällskap på fyra personer, och under tiden som jag parkerade och låste cykeln pratade vi lite. Dom verkade väldigt trevliga, och skulle tydligen bo på hotellet dom också, och även delta på middagen precis som jag. Vi gick till receptionen tillsammans, som var bemannad av en kvinna som jag senare fick veta var en av dom två ägarna till stället. Vingården drevs nämligen av ett par, Göran och Ulrika, som verkligen brann för det dom gjorde. Jag fick intrycket av att Ulrika var den som främst satsade på vinet, medans Göran, som tidigare jobbat som kock i Kanada, stod för maten i den prisbelönta restaurangen.

Jag checkade in, och fick nyckeln till mitt rum som var beläget på översta våningen i huset som låg mitt emot det hus som inhyste receptionen och restaurangen. Rummet var fantastiskt. Stort och luftigt, med smakfull inredning som kombinerade en modern känsla med klassiska detaljer i trä och gjutjärn.

Min första anhalt var som vanligt badrummet, där jag tog en varm dusch, innan jag bytte om till finare kläder inför den rundvandring med vinprovning som ingick i vistelsen. Vi samlades utanför receptionen allihopa, och på behörigt avstånd från varandra begav vi oss ner mot själva inodlingarna. Rundvandringen leddes av Ulrika som hjälpt mig checka in tidigare, och hon beskrev hela processen och gårdens historia med stor entusiasm. 

Vi fick även gå in i lokalerna där vinet tillverkades, och fick till slut provsmaka ett av gårdens viner, ett desertvin gjort på äpple. Faktum är att jag tyckte det var rätt gott, och jag började tänka att det kanske var här på Blaxsta vingård jag skulle börja uppskatta vin. Den förhoppningen grusades dock snabbt.

Efter vinprovningen var det nämligen dags för middag. Jag har inte tänkt dricka något vin alls egentligen, men jag blev serverad glas efter glas av det ena fina vinet efter det andra. Det började redan vid förrätten, med någon slags champagne som tydligen skulle vara väldigt fin. Det var dock helt bortkastat på mig, för jag tyckte det smakade hemskt, men väluppfostrad som jag är drack jag upp allt, bara för att bli serverad ett glas till av samma champagne till nästa rätt. Totalt var det 6 rätter, vilket alltså även innebar 6 glas vin.

Rätterna i sig var rätt otroliga. Det kanske inte var det godaste jag någonsin ätit, men snillrikheten gick inte att ifrågasätta. Till exempel serverades piggvar med kräftstjärtar och maskrossås, svensktillverkad svart kaviar, och torkad älgmjölk. Det var helt enkelt en riktigt rolig matupplevelse, och det märktes hur mycket värdparet brann för det dom gjorde. Mellan varje rätt kom dom ut och berättade en eller annan anekdot kring det som skulle serveras, och stämningen var verkligen på topp. 

På något sätt lyckades jag dricka upp allt vin, och förvånades själv av att jag inte kände mig påverkad. Däremot kan jag inte säga att jag tyckte det var gott, med undantag för vinet som serverades till efterrätten, som återigen var det desertvin vi fått provsmaka tidigare under dagen. 

När middagen var slut passade jag på att prata med värdparet under tiden dom städade, vilket gav oss möjlighet att avhandla många ämnen. Bland annat pratade vi om deras tid i Kanada, varför dom flyttade till Sverige, Görans matlagning, och även mina egna äventyr. Klockan närmade sig midnatt när jag äntligen begav mig tillbaka till mitt rum för att lägga mig och sova.
 
