+++
draft = false
title = "Cykelsemester 2020: Dag 9.5"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-15"
type = "post"

+++

# Cykelsemester 2020: Dag 9.5

Jag vaknade efter att inte ha sovit så jättebra, troligen på grund av att det hade varit ganska varmt och instängt i rummet. Dörren hade varit stängd eftersom jag inte ville störa Lisa, och jag hade inte velat öppna fönstret, rädd för att allt liv från gatan skulle göra det svårt att sova. På det hela taget kände jag mig dock utsövd, och det kändes fantastiskt att få en dags vila, utan att behöva ta sig någonstans. Benen kändes dock rejält möra, vilket knappast var speciellt konstigt.

Efter en enkel frukost bestående av två rostade mackor och ett glas äppeljuice gav vi oss ut på en promenad för att lämna in min cykel på service. Jag hade ringt till ett ställe vid namn Fridhems Cykel som jag blivit rekommenderad av killen från Malmö som jag hade träffat på ICA-butiken i Undenäs. Han hade tydligen jobbat där tidigare, och talade mycket gott om deras service.

Dom var först lite tveksamma till om dom ville ta emot cykeln med tanke på att jag hade så dålig framförhållning och att jag behövde cykeln senast dagen efter, men hade till sist accepterat under förutsättningen att jag lämnade in den så snart som möjligt.

Jag hade visat Lisa på kartan vart cykelstället låg, och hon hade sagt att hon hittade dit. När vi kom fram till det förmodade området och jag kollade kartan på mobilen visade det sig dock att hon hade missuppfattat vart butiken låg, och vi hade således gått åt helt fel håll.

Som tur var är Malmö inte så stort, så det tog inte speciellt lång tid att gå till den korrekta adressen. Jag lämnade in cykeln och fick veta att den skulle vara färdig antingen samma dag eller tidigt dagen efter.

Efter att det var överstökat gick vi och tittade lite i den park som fanns i området, och där Lisa brukade springa. I mitten av parken fanns ett litet café, och eftersom jag fortfarande var ganska hungrig efter den lilla frukost jag hade ätit beställde jag en crêpe med ost och skinka. Crêpe för mig är efterrätt, så kombinationen med ost och skinka föreföll väldigt underlig för mig. Det blev ännu mer underligt av det faktum att den kom serverad med chokladsås. Jag åt min crêpe medans Lisa drack en kopp kaffe.

När vi kände oss färdiga gick vi tillbaka mot lisas lägenhet. Tanken var att vi skulle hämta upp cykelväskorna på vägen. Vi passerade ett litet halvt undanskymt trähus på vägen, och vid närmare inspektion visade det sig vara en slags "offshoot" från en jättekänd restaurang som låg i byggnaden intill och som tydligen hade en Michelin-stjärna. Tanken var att ingredienser som inte kunde användas till rätterna som serverades i den stora restaurangen kunde användas till att göra riktig högklassig "fast food" som sedan serverades i den lilla restaurangen utanför. Vi bestämde oss för att gå dit och äta lunch senare under dagen.

Jag hade inga problem att hämta ut mitt paket, och cykelväskorna verkade vid en snabb inspektion hålla hög kvalitet. Vi tog det lugnt i lägenheten ett tag, och jag fick lyssna på några av Lisas senaste musikprojekt som var riktigt bra. Ungefär en timme senare gick vi sedan för att äta den planerade lunchen.

Både jag och Lisa valde en rätt med marinerat lammkött som kom i ett bröd tillsammans med salsa verde, mest för att den engelsktalande kocken sålde in den så pass bra. Vi blev inte besvikna. Rätten var lite svår att äta eftersom den var så såsig, men den var väldigt god.

Klockan var vid det här laget runt 2 eller så på eftermiddagen, och Lisa kände sig lite trött, så vi gick och tog det lugnt i lägenheten ett tag. Cykelbutiken hade hört av sig och meddelat att servicen var färdig, så någon timme senare gick vi dit tillsammans för att hämta upp den. Tanken var sedan att vi skulle gå ut och ta en drink någonstans innan middagen som vi tänkte äta på en asiatisk restaurang i närheten. Svårigheten låg i att jag ville dricka en god, alkoholfri drink. Faktum var att jag ville ha en väldigt specifik drink: Mango Tango, som ursprungligen kommer från Hard Rock Cafe och innehåller Red Bull, mango och apelsinjuice.

Det visade dock vara svårt att hitta något bra drinkställe. Till sist gick vi till ett matställe som tydligen även skulle servera bra drinkar. Dom kunde inte blanda ihop en Mango Tango åt mig, men jag fick en ganska god alkoholfri drink med smak av lime. En kompis till Lisa kom också dit och gjorde oss sällskap, och följde även med till den asiatiska restaurangen för att äta middag.

Den asiatiska restaurangen visade sig vara riktigt bra. Dom serverade vietnamesisk mat, vilket var första gången för mig. Jag började med en förrätt bestående av vårrullar, och fortsatte sedan med en jättegod varmrätt bestående av tunn, stekt biff tillsammans med grönsaker och ris. Till efterrätt körde jag på en gammal klassiker i form av friterad banan. Portionen var gigantisk.

Efter middagen gick vi och tittade runt lite i folkets park som ligger precis i närheten av Lisas lägenhet, och sedan gick vi hem till henne igen och kollade på serier tillsammans i soffan. Lisa visade mig ett avsnitt av serien "normala människor" som hon tyckte att jag borde börja kolla på.

Runt midnatt gick jag och lade mig. Den här gången efter att ha öppnat fönstret.
