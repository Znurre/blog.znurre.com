+++
draft = false
title = "Cykelsemester 2020: Dag 5"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-10"
type = "post"

+++

# Cykelsemester 2020: Dag 5

Så var det dags. Dagen då jag skulle tillryggalägga 12 mil, med inte mindre än 1300 meters stigning, packning och trötta och möra ben. Jag hatade den version av mig själv som för två månader sedan hade tyckt att detta var en helt rimlig distans, men det var inte mycket att göra åt saken. Istället gick jag för att äta en frukost som förhoppningsvis skulle ge mig tillräckligt med energi för att orka med dagen.

Frukosten serverades inne i det stora huset, och det fanns en ganska så riklig buffé att plocka från, med t.ex. nybakat bröd från ett bageri i staden. Jag blev även erbjuden att ta lite extra bröd för att ha med mig på min resa, om jag skulle bli hungrig, samt att ta med mig Loka från kylen. Jag avböjde båda dessa erbjudanden, men det visade återigen på den fantastiska gästfriheten. 

Innan utcheckning passade jag på att besöka dom två cykelbutiker som fanns i staden för att leta efter nya cykelväskor. Intresset för cykling har dock skjutit i höjden i hela världen som ett resultat av covid19, och alla cykelväskor var slutsålda, och restnoterade hos leverantören. Jag fick helt enkelt hoppas på att mina stackars cykelväskor skulle klara ytterligare några mil.

Efter dom vanliga förberedelsena var det sedan dags att ge sig av. Jag lämnade Nora via Pershyttan, som bjöd på fina vyer. Här började även stigningen i form av en lång, vindlande uppförsbacke, som fortsatte i flera kilometer. Jag fick dock lön för mödan, för sedan var det lika mycket nedför igen, ut på Närke-slätten via Närkes Kil. Hela vägen ner var jag livrädd för att cykelväskorna skulle gå sönder, alternativt hasa ner, vilket förtog upplevelsen något. 

Väl ute på slätten slogs jag över hur otroligt vackert landskapet var. Gyllengula vetefält bredde ut sig till både höger och vänster om den väg jag cyklade på. Jag minns att jag tänkte att med så här fina omgivningar behöver man faktiskt inte åka utomlands för att cykla.

Så slog vädret om. Från att ha varit soligt drog plötsligt mörka moln in, och på bara några minuter hade det börjat regna och åska. Jag tog skydd under ett träd intill vägen, och ja, jag vet att man inte ska stå under ett träd när det åskar, men frågan är om det inte är bättre än att vara ute och cykla i ett helt flackt landskap där man själv är högsta punkten.

![Närke-slätten](/narke.jpg)

När regnet och åskan hade bedarrat satte jag mig upp och åkte vidare. Det dröjde inte länge innan det började regna igen, men åskan höll sig iallafall borta, så jag cyklade vidare. Ytterligare något senare började även regnet att ge vika för solen som åter tittade fram och förskönade dom redan innan så mäktiga omgivningarna. 

Jag cyklade längs vägar som slingrade sig fram genom landskapet, kantade av alléer med stora ståtliga ekar och rönnar. Och både lätt och snabbt gick det dessutom eftersom vinden var på min sida. Jag hade väntat mig motvind, vilket var vad väderleksprognosen hade utlovat, men på något mirakulöst sätt hade den haft fel, och nu hade jag medvind.

Tillsammans med det flacka landskapet tillät det mig att ta mig fram i hög fart, vilket var lite ironiskt. Den vackraste sträckan på min resa hittils var den som jag snabbast lade bakom mig.

På grund av uppförsbacken i början hade min medelhastighet varit ganska dålig, och min preliminära ankomsttid hade först pekat på efter klockan 7 på kvällen. Det skulle ha varit problematiskt, eftersom middagen serverades klockan 7, och ett tag hade jag funderat på att ringa och förvarna boendet om att jag kunde bli sen. Nu minskade dock ankomsttiden snabbt, och redan efter en halvtimme hade jag lyckats pressa ner ankomsttiden till klockan 5.

Jag började bli hungrig, men som tur var närmade sig mitt planerade lunchstopp. Ungefär 4 mil från start låg nämligen ett litet samhälle vid namn Lanna, som mest verkade vara byggt kring golfande och den anläggning som hette Lannalodge Golf Resort. Dom erbjöd förutom boende och golfbanor även lunch i sin restaurang, och det planerade jag att utnyttja.

När jag rullade in till den kombinerade receptionen och restaurangen på området hade mina kläder i princip torkat. Vid det här laget visade min planerade ankomsttid på 16:39, så jag kände mig trygg med att stanna och äta lunch och trots det ha god marginal för oväntade stopp och händelser. Jag gick in och beställde någon slags lax med potatis, som var den rätt som rekommenderades av tjejen i receptionen. Medans jag väntade på att få maten serverad tog jag för mig och åt av en fantastisk salladsbuffé som ingick till lunchen.

Det gick fort att få maten, men den visade sig vara ganska tråkig. Jag blev iallafall mätt, och cyklade vidare, fortfarande med vinden i ryggen. Mitt nästa delmål var Askersund. Jag hade blivit rekommenderad att besöka Askersund på min resa, eftersom det tydligen skulle vara en väldigt vacker stad, och det lämpade sig så bra att Sverigeleden passerade igenom Askersund, vilket gav mig ett gyllene tillfälle att stanna till där.

Vägen dit var dock ganska lång, och jag hann fundera en hel del som vanligt. Dagens reflektion handlade om nummer, t.ex. längd, vikt, och ålder, och hur fixerade vi människor är vid dom.

Min käraste kompanjon denna resa har varit min cykeldator, som hela tiden visar mig vägen, hur snabbt jag cyklar, beräknad ankomsttid, puls, förbrukade kalorier och återstående distans, och just återstående distans är något jag ofta hängt upp mig på under min cykling som ett verktyg för att klara av dom här långa distanserna, rent mentalt. 

Första delmomentet den här dagen blev att komma under 10 mil kvar. Då var det ju "bara" lika långt kvar som tidigare dagar, och det hade jag ju klarat förr. Sedan fortsatte kilometrarna att skalas av, sakta men säkert. Från att ha varit 10 mil, till att vara 9.9 mil, vilket ju känns otroligt mycket närmare. Det är lite samma princip som prissättning, där varor oftast prissätts till t.ex. 1999:- istället för 2000:-, just för att det *känns* billigare. 

Kilometrarna fortsatte skalas av. 9.8, 9.7, 9.6, 9.5, 9.4... och helt plötsligt var det betydligt närmare kvar till 9 än 10. När sedan 5 mil närmade sig någon timme senare kändes det som otrolig milstolpe, för att inte tala om när det bara var 4.9 mil kvar.

En annan väldigt "magisk" distans för mig är 2 mil, för det vet jag är en distans som jag hemma tillryggalägger väldigt enkelt, och utan speciellt mycket ansträngning. När det bara är 2 mil kvar, ja då vet jag att jag kommer orka och att jag nästan är i mål.

Innan Askersund avtog det flacka landskapet, och kandidaten för värsta backen den dagen kom i samhället Fjugesta. Cirka 2 km lång, med en medellutning på 4%. Men även efter Fjugesta fortsatte backarna, en efter en. Vinden hade dessutom bytt riktning, så när den oundvikliga nedförsbacken till sist kom så var den inte fullt så hjälpsam som jag hade önskat.

Jag rullade in i Askersund runt klockan halv fyra, och hade ingen aning om vart jag skulle börja. Vilka delar av staden var värda att se? Vart fanns det caféer och restauranger? En titt på kartan tog mig till busstationen, där jag hittade en servering och en allmän toalett.

Man blir generellt sett inte så kissnödig när man är ute och cyklar, för hur mycket vätska man än dricker så gör man av med det. På 6 timmar däremot kan det vara bra att stanna iallafall en gång, och jag passade på att göra det här nu när tillfälle gavs. 

När jag tog av mig cykelbyxorna märkte jag snabbt att skavsårsplåstret hade gjort vad det skulle, och skyddat dom utsatta delarna. Däremot hade jag svettats en hel del, och det hade fått klistret på plåstren att lösas upp på något sätt. Dom satt bara fast tack vare dom tighta cykelbyxorna, så nu när jag tog av mig cykelbyxorna hängde plåstren bara som dom två sladdriga bitar silikon dom faktiskt var. Jag valde att slita loss dom helt, och fick en halv brazilian wax på köpet.

Efter toalettbesöket besökte jag även serveringen, där jag köpte en Magnum Strawberry White och en RedBull Summer Edition. Det förvånar nog ingen, men det var inte en speciellt bra kombo, och jag rekommenderar den inte.

Medans jag satt där i solen med min glass kollade jag kartan och höjdkurvorna, och blev inte allt för uppmuntrad. Jag hade trott att den sista stigningen innan Askersund skulle ha varit den värsta delen av resan, men jag hade tydligen helt förträngt att den värsta delen av turen låg framför mig.

Jag skulle nämligen ta mig till Tivedens Nationalpark, där jag skulle bo på en vildmarkscamp mitt inne i skogen, och det var konstant stigning fram till mitt mål. Uppgiven satte jag av igen, och cyklade förbi torget i Askersund på vägen ut från staden. På det hela taget var jag inte allt för imponerad av Askersund, men kanske hade min uppfattning förändrats om jag hade haft mer tid där, och någon som kände staden bättre med mig som kunde ha visat mig runt.

Från Askersund bar det sedan av rakt sydväst mot Tiveden, fortfarande med motvind. Det här var tveklöst den jobbigaste delen av dagen. Omgivningarna var inte alls lika vackra, det lutade konstant uppför, och jag hade motvind. 

Efter att ha cyklat någon mil såg jag en person som stod intill vägkanten med en fullpackad cykel, och jag såg det som en chans att få lite vila och någonting annat att tänka på än cyklingen, så jag stannade till och hälsade. Det visade sig vara en medelålders man som tydligen var på väg till sin sommarstuga i Tivedstorp, vilket låg väldigt nära den plats jag skulle till.

Jag frågade om han ville slå följe, och det hade han ingenting emot. Tydligen cyklade han hela vägen från Stockholm, på en cykel med navväxel och max 8 växlar. Dessutom hade han en hink med falurödfärg fastsurrad fram på cykeln, som han skulle använda för att måla om huset. Inte nog med det, han hade tydligen gjort detta i 25 års tid!

Helt plötsligt kändes allting väldigt lätt för mig. Jag fick såklart sänka min hastighet rejält för att hålla hans tempo, men medans han slet upp för backarna kunde jag cykla uppför helt oberört. Det gjorde det, hemskt nog, betydligt mer uthärdligt för mig. Att ha någon att prata med gjorde även det såklart att tiden flöt på betydligt snabbare, och när vi sedan skildes åt i Tivedstorp hade jag endast 15 km kvar till mitt mål.

Vid det här laget var jag djupt in i Tivedens skogar, och det kändes rätt så kusligt att cykla runt där i den dunkla, täta skogen, ensam på en liten grusväg. Efter ett litet tag svängde jag av från huvudvägen mot Ösjönäs, där jag skulle spendera natten.

Om jag hade tyckt att backen efter Fjugesta var jobbig så var det ingenting mot backarna som kom nu. Speciellt den näst sista uppförsbacken var helt sjuk. På något sätt lyckades jag ta mig upp, men det kändes verkligen som om benen skulle sluta fungera dom sista metrarna. Jag fick helt enkelt inget syre till benen, och när backen äntligen var över hettade det i hela benen.

Men nu var jag nära mitt mål. Enligt cykeldatorn handlade det bara om några hundra meter. Däremot såg jag inte anstymmelse till boende, bara en parkeringsplats och någon slags camping. Jag ringde därför till boendet, och fick en vägbeskrivning. Några minuter senare cyklade jag äntligen in på området, precis innan det började regna.

Jag checkade in, och fick ställa min cykel i ett cykelförråd. Mitt boende för natten var i ett av rummen i dom små stugor som fanns på området. Det var litet, men väldigt modern och fräscht, med egen dusch och toalett. Ett gemensamt pentry fanns också om man ville laga lättare mat eller sitta vid ett bord.

Som vanligt så tog jag en varm dusch, bytte om, och gjorde mig redo för middagen som skulle serveras klockan 7. 

Middagen skulle i vanliga fall serveras utomhus i sann vildmarksanda, men på grund av det dåliga vädret hade den istället flyttats inomhus till en ombyggd lada. Där inne fanns träbord med bänkar och skinnfällar att sitta på. Däremot fanns inte tillräckligt med bord för att alla sällskap skulle få ett eget. 

Medans jag satt och väntade på att maten skulle börja serveras kom därför ett par och frågade om dom också kunde sitta vid bordet, varpå jag sa att det gick bra. Dom var tydligen också nya där, och visste precis lika lite som jag om hur matserveringen gick till, så vi började prata lite om det. Det ena gav det andra, och konversationen tog verkligen fart. Det visade sig att dom båda var från Sundsvall, även om dom just nu bodde i Stockholm, och vi hade mycket att prata om.

Maten serverades till sist. Rökt lax med citronpeppar, färskpotatis och citronsås med sallad. Det verkade vara ett väldigt internationellt team av folk som jobbade där, för kocken var från England tror jag, och hans hjälpreda var från Argentina. På det hela taget kändes det som ett väldigt härligt ställe med god gemenskap.

Jag åt glupskt av maten, och avslutade med efterrätt i form av glass med sylt. Hade vädret tillåtit det hade jag gärna tagit en promenad i nationalparken efter middagen, men det spöregnade verkligen, så jag gick in på mitt rum istället för att skriva på bloggen, medans regnet smattrade mot plåttaket ovanför mig.

Det är intressant vad lite tid man har. Jag hade förväntat mig att jag skulle få tid över, att jag skulle ha tråkigt under den här resan. Som hjälpmedel för att roa mig tog jag bland annat med mig 3 böcker och mitt Nintendo Switch, men jag har inte rört något av dom. 

Istället har all min "lediga" tid gått åt till att skriva på bloggen, vilket är mer jobb än vad man kanske kan tro. Min förhoppning är att jag ska kunna komma ifatt någon gång, men det händer helt enkelt så mycket under dagarna som jag vill skriva ner att det verkar omöjligt.

Klockan hann bli 11 innan jag äntligen gick och lade mig.
