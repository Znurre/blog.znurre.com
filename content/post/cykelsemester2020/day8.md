+++
draft = false
title = "Cykelsemester 2020: Dag 8"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-13"
type = "post"

+++

# Cykelsemester 2020: Dag 8

Jag vaknade innan alarmet som vanligt, och förberedde mig för att gå och äta frukost. Frukosten serverades nere i källaren, men huset var av typen suterräng, så det fanns fönster som vette ut mot Åsunden. Corona till trots serverades frukosten här som buffé, och jag vågar nog påstå att det var den bästa frukosten hittils.

Efter frukosten packade jag ihop allt och checkade ut, för att påbörja den cykeltur som skulle bli den längsta hittils, hela 14 mil. Det låter kanske övermäktigt med tanke på hur mycket jag beklagade mig över dom 12 mil jag var tvungen att cykla dag 5, men då ska man ha i åtanke att dagens tur endast hade en stigning på 700 meter, alltså ungefär hälften av den stigning jag hade under hela dag 5. En förklaring till den låga stigningen låg i att början av turen bestod av den banvall jag cyklat dagen innan, och som fortsatte från Ulricehamn till Sjötofta.

Jag hittade snabbt på banvallen igen, som började som cykelväg genom Ulricehamn. Den här delen av banvallen var inte fullt lika vacker, och hade inte lika många stopp, men den var lika lättcyklad, och hade det inte varit för den sedvanliga motvinden hade jag troligen kunnat hålla en bra bit över 30 km/h på sträckan.

Det verkade finnas ett ganska rikt djurliv längs banvallen, för jag såg både ekorre, rådjur, kaniner och tranor. Dom sistnämnda tre höll sig dock på behörigt avstånd, ute på åkrarna.

![Rådjur](/radjur.jpg)

Den häftigaste delen av banvallen var dock ett parti där man fick cykla genom dom gamla, bevarade järnvägstunnlarna, där belysning hade installerats för att göra dom mindre kusliga.

Jag stannade till och åt lunch i Tranemo i form av en hamburgare och Red Bull. Vädret var okej. Växlande molnighet, lite väl kallt kanske, men med vindjackan på gick det bra att sitta ute och äta, där jag även hade uppsikt över cykeln.

I Ambjörnarp, som var stationen efter Tranemo, tog cykelbanan slut. Där ligger tydligen 7 km av det gamla järnvägsspåret kvar, och används för dressinåkning. Här blev det alltså till att åka ut på normal landsväg igen, och faktum är att jag insåg att jag hade saknat det. Banvall må vara lättcyklat, men det är inte speciellt spännande. En bit längre fram stötte jag sedan på den resterande biten järnväg, som korsade landsvägen på ett antal ställen, och vid ett av dessa ställen såg jag även ett gäng personer som faktiskt var ute med dressiner.

Det dröjde inte länge innan jag började bli trött på landsvägen också. Dom västergötländska skogarna var föga inspirerande, och precis lika tråkiga som dom vi har uppe i Hudiksvall. Det kändes som att cykla längs 84an ungefär, med skillnaden att väglaget var bättre, och trafiken minimal. Motvinden var fortfarande ett faktum, och rumpan skavde.

Under tiden jag cyklade roade jag mig med att komponera en lista på saker jag ogillade mest just då, och även om det var svårt att välja placeringar så kom jag till sist fram till följande topp 3-lista:

1. Motvind
2. Tråkig, oinspirerande landsväg som bara fortsätter och fortsätter genom blandskog där varje kilometer är den andra lik
3. Rumpsvett

Till sist blev jag räddad från den monotona landsvägen av att jag åkte igenom samhället Fegen, där skogen lättades upp något i förmån för utsikt över sjön med samma namn. Det dröjde dock inte länge innan jag befann mig på landsväg med lika tråkig skog igen, men den här gången höll det inte i sig lika länge. Jag kom ganska snart fram till samhället Kinnared, som mestadels verkade vara ett enda stort sågverk, och doften av nysågat trä fyllde mina näsborrar när jag åkte förbi.

Här valde jag återigen att ta en liten avstickare från rutten. Det var nämligen så att cykelspåret gick en annan väg än min planerade rutt, och en titt på kartan bekräftade att jag lika gärna kunde åka där. Det visade sig vara ett bra beslut, för dels så gick cykelleden längs järnvägen, och visade sig därför vara ganska lättcyklad, och dels var den betydligt roligare än det landskap jag cyklat genom hittils. 

![Bög](/bog.jpg)

När jag sedan kom ut på det ställe där jag skulle ansluta till min planerade rutt var jag på betydligt bättre humör än tidigare. Det visade sig att cykelleden fortsatte, fortfarande med lite annan dragning än min planerade rutt, men jag valde trots det att fortsätta enligt den. Det skulle bli en liten omväg, vilket kanske inte var jättesmart med tanke på att jag redan hade en bra bit kvar till mitt mål, men å andra sidan så cyklar jag ju inte bara för att ta mig från punkt A till B, utan själva resan är minst lika viktig.

På ett ställe hade jag valet att fortsätta rakt på, eller svänga av tillbaka mot min ursprungliga rutt. Båda valen skulle ta mig längs cykelspåret. Jag valde rakt fram. Någon kilometer senare insåg jag dock att det var ett dumt val, för det skulle bli en allt för stor omväg, och därför vände jag och cyklade tillbaka.

Mitt lilla äventyr orsakade inte bara en omväg på några kilometer, utan även lite extra stigning, men det kändes ändå värt det. Cykelspåret fortsatte nu längs min utplanerade rutt dom sista 2 milen, och hade jag bara fortsatt rakt på hade jag till sist kommit till Halmstad. Jag skulle dock inte till Halmstad, utan svängde istället av österut för att ta mig till min slutdestination för dagen - Simlångsdalen.

För att komma dit var jag dock tvungen att ta mig över ett berg. Jag hade teoretiskt sett kunna fortsätta till Halmstad, och sedan åka österut för att komma till Simlångsdalen, vilket hade besparat mig flera hundra meter i stigning. Det hade dock inneburit en omväg på minst 1 mil, och om det var någonting jag hade lärt mig av den här dagen så var det att mängden mil faktiskt spelar stor roll, inte bara stigningen. Vid det här laget var jag trött och hade ont i rumpan, och jag tog hellre lite uppförsbacke än ännu en halvtimme av tråkig landsväg.

Det var en bra bit upp till toppen. 116 meter stigning över cirka 3 km, vilket gav en lutnig på ungefär 3.7%. Jag tyckte dock inte att det var så farligt. Kanske berodde det på att mina ben var så pass utvilade efter flera dagar utan speciellt mycket stigning. Dessutom var skogen och naturen som omgav mig väldigt vacker här, vilket hjälpte mig att hålla tankarna borta från cyklingen.

När jag till sist kom upp till toppen hade jag förväntat mig en rejäl nedförsbacke. Jag blev dock besviken. Den första nedförsbacken minskade min höjd med cirka 44 m, bara för att kasta en ny uppförsbacke i ansiktet på mig som tog mig upp till nästan samma höjd igen. Sådär fortsatte det ytterligare 2 gånger, som en trappa, innan jag till sist rullade in på parkeringen till mitt boende.

Det här stället tillhörde ett av dom boenden jag sett fram emot mest att bo på, tillsammans med Fredrikssons Pensionat. Jag blev därför inte speciellt förvånad över att finna att hela anläggningen var jättemysig. Anläggning får det att låta som någonting stort och opersonligt, men egentligen bestod den bara av ett stort hus med reception, restaurang och några rum, samt några fristående stugor och ett poolområde.

Redan vid incheckningen fick jag bra vibbar. Efter att ha gått in i huvudbyggnaden ledde en liten trätrapp upp till övervåningen där receptionen fanns. Jag checkade in, och fick nyckeln till mitt rum som låg i samma byggnad bara några meter längre bort. Och vilket rum det var. Det kanske inte var det absolut bäst utrustade rummet jag någonsin bott i, men designen var fantastisk. Modernt, och med stort fokus på smart användande av trädetaljer. Dessutom luktade det färskt trä där inne, så jag antar att det var helt nyrenoverat.

Jag tog som vanligt en dusch och bytte om inför middagen, som alltså serverades i samma byggnad, bara några meter utanför mitt rum. Det var en hel del gäster i restaurangen när jag kom dit, och jag blev visad till ett bord av samma kvinna som stått i receptionen och hjälpt mig att checka in tidigare. Jag började kolla på den meny jag fått, och fick genast svårt att välja. Menyn bjöd på för-, varm- och efterrätter, och alla verkade väldigt intressanta. Någon gång skulle jag vilja åka tillbaka hit och prova allt på menyn.

Med lite hjälp av kvinnans rekommendationer valde jag sedan en carpaccio på hjort med marinerade cocktailtomater och parmesan till förrätt, fetaostbakad kyckling med potatiskaka och fransk dragonsås till varmrätt, och en havtornsparfait till efterrätt.

Förrätten bjöd på mycket fräscha och distinkta smaker. Varmrätten var dock en klass för sig. Potatiskakan var fantastiskt god, med ett tunnt lager av smält ost uppepå som gav den en riktigt god krispighet. Dragonsåsen var perfekt avvägd, med en liten touch av dijon för att få lite "edge". Och förutom det som nämndes på menyn serverades rätten desutom med smörsteka kantareller, som tillsammans med dragonsåsen skapade en helt fantastisk smakupplevelse. Kycklingen var kanske det minst imponerande. Jag skulle ha föredragit om den inte hade varit fullt så tillagad, för den var tyvärr på gränsen till torr, men det framstod som ett ytterst litet problem i förhållande till resten av rätten.

Sen kom efterrätten, och den var minst lika imponerande den. Som jag nämnde tidigare var det en havtornsparfait, och den var utformad så att glassen var gjord med havtorn. Det visade sig vara en riktigt genial kombo. Dess smaker gifte sig otroligt bra tillsammans med dom andra bären och marängen.

Värt att nämna är också att all mat serverades av samma kvinna. Det var tydligen hon som drev stället, och hon gjorde det mesta själv där. Köket jobbade hon dock inte i, även om hon tydligen gjort det tidigare, och tydligen var det hennes idé med havtornsglassen.

Mycket nöjd och belåten gick jag och lade mig. Jag kunde fortfarande känna krängningarna från cykelväskorna som fortplantade sig i hela cykeln, ungefär på samma sätt som när man har varit ute till havs och känner hur det gungar flera dagar efteråt.
