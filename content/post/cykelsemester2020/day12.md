+++
draft = false
title = "Cykelsemester 2020: Dag 12"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-18"
type = "post"

+++

# Cykelsemester 2020: Dag 12

Efter en natt med halvokej sömn vaknade jag innan alarmet av en kombination av solen som lös in i rummet, och ventilationen utanför som hade dragit igång för fullt igen.

Jag klädde på mig och gick ner för att äta frukost som serverades som en buffé. Den höll hög kvalitet även om utbudet inte var det största. Min frukost intog jag sedan sittandes ute i trädgården i morgonsolen.

Efter att ha packat och gjort mig i ordning som vanligt checkade jag ut och gav mig iväg. Jag passade på att stanna till vid coop i Alvesta för att köpa dagens första Red Bull, och åkte sedan vidare mot mitt planerade lunchställe för dagen som var ett litet café i Öhr vid namn "Systrarna Skogströms lantcafè".

Resan dit gick utan problem, och jag anlände runt klockan 1, vilket passade ypperligt för lunch. Utanför caféet hängde en White guide-flagga som visade på att stället tydligen varit med i 2019 års upplaga av White guide. Jag valde en matpaj med ost och skinka, och köpte en lemonad att dricka till det.

Det tog lite tid för maten att bli färdig, men det gjorde mig inte speciellt mycket, för jag njöt av att bara sitta i solen ute på innergården och smutta på min lemonad. Till sist kom maten, serverad av en av dom många personer som jobbade där. Jag är inte helt säker på vilka av dom som faktiskt var systrarna som caféets namn anspelade på.

Pajen var okej. Jag hade gärna sett lite mer skinka, och jag tyckte tyvärr att den var snudd på smaklös, men det var gott att få någonting i magen.

Jag passade på att låna toaletten och satte sedan av mot nästa planerade stopp på vägen, som jag faktiskt hade hittat av misstag dagen innan när jag tittade efter potentiella matställen att stanna vid. Det var ett utkikstorn som enligt informationen på internet skulle vara 25 meter högt. Jag tänkte att det kunde vara kul att få se utsikten där uppifrån, och det var knappt någon omväg att stanna till där.

För att komma dit var man dock tvungen att göra ett kort avsteg från vägen och ta in på en liten grusväg som ledde en tillbaka 1 km eller så. För att undvika det hade jag istället dragit om min rutt så att jag tog av från asfaltsvägen lite tidigare, in på vad som enligt kartan såg ut att vara en grusväg. Denna grusväg såg sedan ut att vara sammankopplad med den andra, vilket skulle innebära att jag kom ut på ursprungsvägen lite längre fram igen.

Jag insåg dock rätt snabbt efter att ha tagit av från asfaltsvägen att det här inte var någon grusväg som jag hade trott, utan en vandringsled i form av en vanlig skogsstig, med rötter, stenar och pressad jord. Här borde jag kanske ha vänt om, men jag bestämde mig för att fortsätta. Skogsstigen fortsatte i någon kilometer innan jag till sist kom ut på den grusväg som ledde mig upp till utkikstornet.

Det var bemannat av en man från den lokala hembygdsföreningen, och han tog emot min betalning på dom 20 kr som det kostade att få gå upp i tornet. Jag gick upp dom 6 eller så trapporna, och kom till sist till ett rum i toppen av tornet. Utsikten var betydligt tråkigare än vad jag hade förväntat mig, i princip bara skog. Besviken gick jag ner igen.

![Utsikt](/utsikt.jpg)

Väl nere igen pratade jag ett tag med mannen, och det visade sig att tornet var en hyfsat ny konstruktion. Det hade byggts, på initiativ av honom faktiskt, år 1958 om jag kommer ihåg riktigt. Däremot hade det stått ett annat utkikstorn där förr i tiden med ungefär samma höjd, som hade använts för att övervaka transporter i området.

Innan jag åkte vidare passade jag även på att köpa en glass och något att dricka för att få lite mer energi i mig. När det var uppätet och uppdrucket så gav jag mig iväg för att ta mig ut på asfaltsvägen igen.

Det visade sig dock vara lättare sagt än gjort. Grusvägen som gick upp till utkikstornet ledde, så vitt jag kunde se, bara till en återvändsgränd längre ner på vägen. Kanske hade det funnits en annan väg ut tidigare, men den hade jag isåfall missat.

Istället valde jag till sist, med stor tveksamhet, att följa cykeldatorns rutt, som jag hade trott skulle vara grusväg, men som visade sig vara skogsstig av betydligt sämre kvalitet än den första jag hade cyklat på.

Jag skumpade fram längs den ojämna vägen, och tyckte synd om både min cykel och mina nya cykelväskor som behövde bli utsatta för det här. Till sist kom jag ut på asfaltsvägen, till synes oskadd, och färden kunde fortsätta.

Landskapet hade bit för bit blivit betydligt mer kuperat, och inte av den snälla typen av stigning, utan här pratar vi om ganska rejäla backar. Mer än en gång önskade jag att jag hade haft en till, lägre växel på min cykel.

Backarna fortsatte hela vägen fram till min destination, och även om det såklart också innebar en hel del nedförsbacke emellanåt så kändes det som en väldigt dålig byteshandel.

När jag bara hade ungefär en mil kvar till mitt boende så hände någonting konstigt. Min cykeldator som hittills fungerat klanderfritt hela resan från Hudiksvall, tappade GPS-täckningen gång på gång. Den försvann i någon kilometer, kom tillbaka en kort stund, bara för att försvinna igen kort därefter. Detta upprepades ett fyrtal gånger under en sträcka på kanske 2-3 km. Jag började först såklart oroa mig för att det var någonting fel med cykeldatorn, men helt plötsligt så betedde den sig som den skulle igen, och fungerade utan problem dom sista kilometrarna till boendet. Jag undrar såhär i efterhand om det fanns någonting precis där som störde ut signalerna.

Men jag kom alltså fram som jag skulle, och givetvis bestod även den sista biten och infarten till Wallby säteri, där jag skulle spendera natten, av en stor uppförsbacke. Jag svängde in på grusplanen och lokaliserade snabbt receptionen som låg i ett litet uthus bredvid den stora, herrgårdsliknande byggnaden där tydligen ägarfamiljen bodde.

Efter en snabb incheckning blev jag visad till ett av dom mindre husen på området, där jag skulle bo. Det var ett jättefint inrett hus med hög standard. Stort och rymligt var det också, vilket gjorde att jag kunde ta in cykeln.

Jag passade som vanligt på att ta en varm dusch, och eftersom dom hade riktigt schampo här passade jag även på att tvätta håret. Nyduschad och fräsch gick jag sedan till middagen, som serverades ute i trädgården i den milda sommarkvällen. På vägen dit passade jag även på att utnyttja den tvättmaskin som fanns i receptionsbyggnaden. Mina cykelkläder hade ju blivit rejält blöta under cykelturen till Malmö, och luktade surt. Dessutom kändes det skönt att få tvätta upp mina vardagskläder också.

Vid middagen blev jag placerad vid ett bord som var dukat för en person, och blev ganska raskt serverad en enkel grön sallad med jättegott nybakat bröd under tiden jag väntade på förrätten, som också skulle vara en sallad, med bland annat sockerärter, eldost (svensk halloumi), rostade pumpakärnor och nässelpesto.

Förrätten kom efter ett tag, tillsammans med en mousserande vitvinbärsdryck som jag också hade beställt. Jag brukar inte vara något fan av halloumi, men tyckte faktiskt att den här var helt okej. Den var skuren i små, tunna bitar, vilket gjorde att den var riktigt krispig och inte hade den där karaktäristiska konsistensen som halloumi brukar ha. Nässelpeston var också den överraskande god och gifte sig bra med resten av salladen.

Varmrätten dröjde lite väl länge, och när den väl serverades blev jag lite besviken. Dagens varmrätt bestod nämligen av revbensspjäll med barbequesås och pommes. Inte heller revbensspjäll är någon storfavorit för mig, och jag tyckte det kändes lite tråkigt. Jag hade nog förväntat mig något lite mer spännande i stil med dom andra ställena jag hade bott på, speciellt med tanke på hur högklassigt det här stället kändes i övrigt. Det här kändes mer som något man skulle kunna få på vilken amerikanskinspirerad restaurang som helst. Maten var dock god, om än lite tråkig, och speciellt deras pommes stod ut som mycket goda.

Efterrätten var en chokladkaka med färska bär, maränger och grädde, och var mycket god. Den vägde delvis upp för den något tråkiga varmrätten, och när jag var färdig med den kände jag mig trots allt mycket mätt och belåten. Det hade börjat bli kallt ute, eftersom solen var på väg ner. Jag hämtade upp mina kläder från tvättmaskinen och hängde upp dom på rummet för torkning innan jag gick och lade mig i dom sköna sängarna för att sova.
