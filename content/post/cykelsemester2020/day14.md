+++
draft = false
title = "Cykelsemester 2020: Dag 14"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-20"
type = "post"

+++

# Cykelsemester 2020: Dag 14

Jag vaknade upp i min säng på vandrarhemmet med en känsla av att inte vara helt utvilad. Regnet under natten till trots hade det varit väldigt varmt och kvavt i det lilla rummet, och jag hade även gång på gång plågats av en mygga som verkade omöjlig att ta kål på.

Jag hade blivit erbjuden frukost, vilket jag tacksamt accepterat, även om jag tyckte synd om Kenneth och hans fru Ewa som måste ordna med frukost bara för mig. Kenneth hade dock sagt att det inte var något problem, och jag hade alltså valt att få frukosten serverad klockan halv tio.

Eftersom jag hade vaknat rätt så tidigt fick jag fick fördriva tiden ett tag fram tills dess,  men när jag sedan gick ner möttes jag av Kenneth och Ewa vid ett bord uppdukat med fullt av godsaker. Där fanns flera sorters bröd och pålägg, och en massa olika sorters yoghurt att välja på. Faktum är att jag blev riktigt rörd över att dom hade dukat upp en sådan fin frukostbuffé bara för min skull. Givetvis hade jag betalat för det, men ändå.

Jag åt mycket av den goda frukosten, och när jag kände mig mätt och belåten satte jag mig i allrummet med min laptop för att kolla upp all information kring dagens rutt medans Kenneth och Ewa dukade undan frukosten.

Det regnade ute. Faktum är att regnet från natten inte verkade ha lagt sig, och det fullkomligt öste ner. En titt på väderleksprognosen visade på att detta var vad jag hade att förvänta mig hela dagen. Jag förberedde mig alltså på en mycket blöt dag.

Kenneth och Ewa skulle iväg på någon slags ärende, och lämnade mig ensam med instruktioner för vart jag skulle lämna nyckeln vid avfärd. I vanliga fall skulle senaste utcheckningstid vara klockan 12, men eftersom jag var ensam gäst antar jag att det inte var så viktigt. Det gav mig möjligheten att sitta kvar där ett tag, tills regnet lugnat ner sig något.

Någon timme senare hade regnet faktiskt reducerats till den grad att det knappt kunde kallas regn längre. Jag utnyttjade tillfället för att komma iväg därifrån. Även om jag redan tidigare denna resa hade cyklat genom regn ett antal gånger så kändes det av någon anledning betydligt värre att ge sig av när det regnade än att ovetande cykla in i ett regnoväder.

Innan jag lade Horn bakom mig så passade jag på att stanna till i ortens lokala ICA-butik för att handla på mig lite energi, och sedan bar det av mot Söderköping som var mitt mål för dagen.

Dagens tur skulle bli relativt kort med mina mått mätt, endast cirka 8 mil. Därför hade jag ingen brådska, utan kunde ta det väldigt lugnt längs det som troligen hade varit en väldigt vacker väg om vädret inte hade varit så tråkigt. Vägen gick nämligen upphöjt precis längs vattnet på ett flertal ställen, med bara ett vägräcke som skydd från att ramla ner. På andra sidan tornade stora klippväggar upp sig, och skapade en känsla av att vara någon annanstans än i Sverige.

Jag hade planerat att stanna till för att äta lunch i ett samhälle som hette Åtvidaberg, där det tydligen skulle finnas ett stadshotell som serverade lunch. Dagen till ära serverades korv stroganoff, vilket är en personlig favorit, så ett stopp där var givet. Deras öppettider var också väldigt generösa, och när jag rullade in där någon gång efter klockan 2 var deras lunchservering fortfarande öppen, även om det vid det laget inte fanns speciellt många andra gäster i lokalen.

Att hitta till hotellet hade inte varit helt okomplicerat. Jag kom in till samhället via en cykelväg, utan någon som helst kännedom om var jag befann mig i förhållande till centrum. Att det saknades skyltning mot centrum hjälpte såklart inte heller. Det fanns någon slags karta uppsatt, som jag försökte tyda, men utan någon slags markering som talade om vilken som var kartans position var den mycket svåranvänd. Jag försökte bilda mig en uppfattning om min ungefärliga position med hjälp av landmärken i närheten, och satte av mot det jag antog var centrum. En bit på vägen träffade jag på 3 kommunarbetare som satt och dinglade med benen från räcket av en bro, och alltså inte verkade allt för upptagna. Jag frågade dom om en vägbeskrivning till hotellet, och fick kanske inte riktigt det svar jag hade väntat mig.

Dom frågade om jag menade "klinten", och tillade att stadshotellet låg en bra bit bort. Utan någon som helst lokalkännedom kunde jag såklart inte veta om det var "klinten" jag menade, och dessutom hade jag sett klart och tydligt när jag planerade rutten samma morgon att det skulle finnas ett stadshotell i staden. Nåja. Dom kunde iallafall peka mig mot centrum, och jag gav mig alltså av i den riktningen. Inte långt senare hittade jag mycket riktigt stadshotellet, precis i centrum, och det hette klart och tydligt Åtvidaberg Hotell.

Efter lunchen fortsatte vägen genom häftiga skogar, med stora ståtliga träd. Faktum är att omgivningen påminde väldigt mycket om Tiveden. På något mirakulöst sätt hade jag dessutom lyckats undvika regnet, och jag började väl bli lite väl kaxig. Det var bara 3 mil kvar till Söderköping, och med den fart jag höll skulle det vara en smal sak att ta sig fram. Inte skulle det väl hinna komma något regn fram tills dess?

Det skulle dock snart visa sig vara ytterst naivt tänkt. Bara några minuter senare så började jag känna regndroppar, och inom kort befann jag mig i det värsta ösregn jag varit med om, på samma nivå som vid en tyfon i Japan ett år när jag varit där. Inom någon minut var jag helt genomblöt.

Nu började även stigningen. Från att ha varit relativt plant hela vägen kom såklart dagens värsta backe precis när regnet hade börjat. Inte nog med det. Från att ha varit asfaltsväg blev det nu också grusväg. Jag cyklade uppför, med vattnet pumpande ut ur skorna för varje tramptag, på den väg som regnet på rekordtid förvandlat till vad som närmast kunde beskrivas som lervälling.

Regnet fortsatte ösa ner med samma intensitet, och jag kunde knappt se en meter framför mig. Hade det inte varit för mina blöta skor hade jag dock inte tyckt att det var så farligt. Det är en väldigt mäktig känsla att trotsa vädrets makter på det här sättet, att cykla genom kraftigt regn samtidigt som man helt oberört fortsätter lyssna på musik med sina regnsäkra hörlurar, och med en cykeldator som fortsätter att fungera som om ingenting hänt.

Vägen gick genom ett naturreservat, alltjämt på grusväg. Regnet verkade locka ut djuren, för jag såg både stora rovfåglar, en älgko med två kalvar som betade på en äng, och dom vid det här laget rätt så vanligt förekommande rådjuren.

Allt eftersom började regnet att sakta avta, och såklart avtog det lagom tills att jag svängde ut på asfalterad väg igen. Vid det laget var dock skadan redan skedd. Min cykel, jag själv, och mina cykelväskor var helt täckta av grus och lera, och när jag klev av cykeln för att inspektera läget upptäckte jag att det hade bildats en rejäl "gruskaka" på sadeln där jag hade suttit.

Sista biten mot Söderköping gick bra, och jag kom fram till hotellet runt klockan 5 och checkade in. Hotellet jag hade valt hette Laurenti, och var ett ganska litet, familjeägt hotell nära centrum.

Från början hade jag planerat att bo på ett säteri cirka 1 mil utanför staden, men avbokat när jag fått reda på att dom av någon anledning inte serverade middag på måndagar. Även om det boendet i övrigt hade verkat trevligt så hade det helt enkelt blivit för krångligt. Alternativ ett hade varit att åka till Söderköping först, äta middag där i mina svettiga cykelkläder för att sedan åka till säteriet. Alternativ två hade varit att först cykla till säteriet, duschat och bytt om, cyklat cirka 1 mil till Söderköping i klänning, ätit middag och sedan cyklat tillbaka igen. Inget av alternativen kändes speciellt lockande, och dessutom försvårades allting ytterligare av att E22 delade av vägen mellan Söderköping och säteriet, utan vettiga möjligheter att korsa den. Jag skulle ha behövt cykla en bit på E22, och det kändes inte alls kul. Jag bokade alltså av säteriet.

Hotell Laurenti var inte mitt första val. Jag fick inget vidare intryck från bilderna på deras hemsida, som visade rum med tråkig, 80-talsinspirerad inredning. Istället försökte jag boka rum på ett bed and breakfast som såg väldigt mysigt ut, men som tyvärr visade sig vara helt fullbokat. Den höga beläggningen skulle kunna förklaras av att det hotell i staden som verkade hålla högst standard, och som jag gärna hade velat bo på om det hade varit möjligt, hade stängt ner sin verksamhet under sommaren på grund av covid.

Istället fick jag alltså boka rum på hotell Laurenti, och när jag klev in i receptionen, blöt och grusig, hade jag inte speciellt höga förväntningar. Jag blev dock mottagen av en vänlig, äldre kvinna, som jag antar var en av ägarna till stället. Efter incheckningen gick jag direkt till mitt rum, där jag tog av mig mina blöta kläder och tog en dusch. Jag gjorde även vad jag kunde för att få mina saker att torka. Badrummet hade golvvärme, vilket jag utnyttjade till min fördel genom att ställa upp den till max och lägga alla kläder på golvet. Skorna försökte jag förgäves att få torra med hjälp av en liten hårtork.

Rummet var ungefär så deprimerande som det hade sett ut på bilderna. Det som dock förvånade mig var att det främst bara var valet av inredning som bidrog till detta intryck. Hade möblemanget varit lite mer modernt och smakfullt hade detta hotell framstått som ett riktigt trevligt ställe, för det var rent och fräscht och hade gott om faciliteter.

Efter att ha duschat och bytt om gav jag mig sedan iväg mot den del av Söderköping som ligger vid Göta kanal och som utgör dom äldsta och vackraste delarna av staden. Eftersom Söderköping knappast är en speciellt stor stad gick promenaden dit snabbt. Jag hade bokat bord på en restaurang vid namn Bakfickan som verkade vara en av dom bättre restaurangerna i staden, men jag var tidig, och det var fortfarande cirka 40 minuter kvar tills mitt bord skulle vara redo, så jag tog en promenad längs Göta kanal för att fördriva tiden.

Omgivningarna var vackra, och vädret hade förbytts helt till att bjuda på en fantastisk sommarkväll. Hade jag haft mer tid på mig hade jag gärna utforskat vägarna närmare, men eftersom min bordsreservation började närma sig vände jag och gick tillbaka.

Det var en hel del folk vid restaurangen när jag kom dit, men troligen hade jag klarat mig utan att reservera bord. Jag hade gjort min reservation flera dagar tidigare, under min vistelse på Villa Gransholm, och då hade jag tagit det säkra före det osäkra och bokat ett bord inomhus. Med facit i hand hade det troligen varit trevligare med ett bord utomhus, med tanke på det fina vädret, men nu fick jag stå mitt kast.

Jag blev visad till mitt bord av en servitris och fick en meny. En sak som slog mig direkt vart att det fanns otroligt få alkoholfria dryckesalternativ på menyn. Dom flesta ställen jag bott på hittills hade haft någon trevlig alkoholfri cider eller liknande, men här fanns det bara tråkig läsk, typ Coca Cola och Fanta. När jag påpekade detta kontrade servitrisen med att dom hade ett stort urval av alkoholfria öler och viner. Detta argument har jag dock själv aldrig förstått. Är det verkligen så att dom flesta som vill ha ett alkoholfritt alternativ är personer som i vanliga fall skulle ha valt en alkoholhaltig dryck, men som av någon anledning inte kan eller vill? Problemet för mig är att jag generellt inte gillar smaken av alkoholhaltiga drycker. Jag tycker att både vin och öl smakar hemskt, och då vill jag väl inte ha det i någon form, alkoholfritt eller inte. Till sist valde jag en 4.5-procentig Sommersby-cider. Jag kan dricka alkoholhaltig cider, även om jag tycker det är betydligt mindre gott. Den jästa smaken och den bittra alkoholsmaken tar över nästan helt, och dränker smaken av frukt. 

Efter att ha fått min dryck beställde jag maten. Toast Skagen till förrätt, en pasta med kalvfärsbullar till varmrätt, och vaniljglass med jordgubbar och kolasås till efterrätt. Jag tänkte att det skulle vara trevligt med en riktigt bra pastarätt, eftersom jag hittills inte hade ätit någon på min resa. Den visade sig dock tyvärr vara ganska så medioker. Förrätten och efterrätten var dock goda. Något besviken gick jag därifrån, tillbaka till hotellet för att sova.
