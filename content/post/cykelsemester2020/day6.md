+++
draft = false
title = "Cykelsemester 2020: Dag 6"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-11"
type = "post"

+++

# Cykelsemester 2020: Dag 6

Jag vaknade upp till dag 6 av min cykeltur ovanligt utvilad i benen. Det verkar som om min kropp har börjat återhämta sig, trots att jag inte har haft några vilodagar. Idag skulle dessutom bli betydligt mindre jobbig än tidigare dagar, eftersom jag bara skulle cykla 8 mil med cirka 600 meters stigning. 

Efter sedvanliga förberedelser gick jag för att inta min frukost, som serverades i samma lada där middagen hade serverats dagen innan. På grund av covid19 serverades frukosten på personligt utformade brickor utifrån en beställning vi fått göra kvällen innan. Det gjorde att storleken på frukosten var förutbestämd, och kanske i minsta laget för mig som brukar vilja äta en rejäl och stadig frukost innan jag ger mig iväg.

Jag checkade sedan ut, och påböjade turen mot mitt nästa boende, som faktiskt var det boende jag sett fram emot mest att bo på - Fredrikssons Pensionat i Sjöbacka. Pensionatet var ett litet, familjeägt ställe, beläget vid en sjö nära Skövde. Jag hade blivit tvungen att ta en liten omväg för att stanna till där, men jag trodde mig veta att det skulle vara värt det. Stället såg otroligt idylliskt ut, dess betyg var utmärkt och det som framförallt fångade min uppmärksamhet var beskrivningen av maten som serverades där:

> Min tanke med köket på Sjöbacka är att laga maten med livsmedel från Vallebygden och med ekologiska basvaror. Jag har därför inte någon färdig meny, utan tillgången på de bästa råvarorna och säsongen är det som styr.

Innan min ankomst hade jag fått en meny med maten som skulle serveras, och den lät minst sagt imponerande:

> Förrätt: Kallrökt laxrulle med kräftstjärtssås, sallad och brödkrutonger.  
Varmrätt: Oxfilé från Hermoängens lantrasgård, cognacssås, pommes Anna, vitlökdsdoftande körsbärstomater, sockerärtssallad.  
Dessert: Parfait av trädgårdens fläder, salta jordnötter, strimlad mörk choklad.

Och för den som undrar så fick jag givetvis en variation på efterrätten utan jordnötter, men nu går jag händelserna i förväg.

Jag gav mig alltså iväg efter utcheckningen, och med tanke på hur mycket uppförsbacke jag hade avklarat dagen innan var det knappast en överraskning att vägen började med en rikligt tilltagen nedförsbacke som tog mig flera kilometer.

Efter att ha åkt grusväg en bit så visade min cykeldator att jag skulle svänga in på en liten timmerväg, så jag lydde snällt. Det var den första timmervägen jag cyklade på hittils denna resa, och jag njöt till fullo. Dels för att jag var glad över att få lite variation, och dels för att jag alltid har gillat att cykla mindre skogsvägar. Däremot var jag rädd för att mina vid det här laget ännu trasigare cykelväskor inte skulle gilla det lika mycket, så jag tog det ytterst försiktigt.

Det dröjde inte länge innan jag var ute ur skogen och började närma mig civilisationen igen. Mitt första stopp var på ICA i byn Undenäs för att handla energidryck. Här mötte jag ännu en kille med fullpackad cykel, och som vanligt frågade jag om han var på cykelsemester. Det visade sig att han var betydligt mer galen än mig, för han skulle tydligen cykla från Malmö hela vägen till Kiruna. Inte nog med det. När han väl hade kommit dit tänkte han dessutom bestiga Kebnekaise.

För att klara det hade han hittils cyklat cirka 20 mil per dag, från det att han vaknade tills att det blev för mörkt för att cykla. Det verkade dock inte som om han hade någon jättebra koll på vägen han hade tänkt cykla. Framförallt så verkade han inte vara medveten om att mängden stigning skulle öka betydligt från och med Undenäs, så jag är minst sagt nyfiken på hur det har gått för honom.

Vi pratade i säkert en timme, och hade riktigt trevligt. Vi diskuterade bland annat dom djur han sett på vägen. Tydligen hade han sett både älg, varg och vildsvin. Han nämnde även att han haft medvind hela vägen, vilket bådade illa för mig eftersom jag ju var på väg i motsatt riktning.

Mycket riktigt. När jag sagt hejdå till honom och åkt vidare så möttes jag av motvind, vilket gjorde cyklingen betydligt jobbigare än vad jag hade trott. Efter ett litet tag började det dessutom regna, och jag stannade till under ett träd för att ta fram min regnjacka. Plötsligt hörde jag en röst på ett språk som jag inte riktigt kunde placera. Den visade sig komma från en kvinna i en trädgård vid ett hus precis intill vägen där jag hade stannat. Först tänkte jag att hon kanske pratade danska, men när jag tittade på registreringsskylten på hennes bil förstod jag att hon var från Tyskland.

Hon bjöd mig till att sitta på hennes farstu, och gick och hämtade apelsinjuice åt mig att dricka. Sedan pratade vi lite för att fördriva tiden medans vi väntade på att regnet skulle avta. Tydligen hade hon och hennes man haft sommarstuga i Sverige i 35 år, vilket förklarade att hon kunde en del svenska, om än bristfällig.

Efter kanske 10 minuter hade regnet upphört, även om det snart såg ut att kunna börja igen. Jag kände dock inte för att sitta där hela dagen, så jag gav mig av igen, ut i motvinden. 

Jag hade även börjat bli riktigt hungrig vid det här laget, troligen på grund av att frukosten hade varit i minsta laget. Mina försök att fråga tyskan om eventuella caféer eller restauranger i närheten hade inte varit speciellt lyckade. För det första så var hennes svenska bristfällig, och för det andra så verkade hon inte veta särskillt mycket om omgivningen. Det enda hon kunde säga med säkerhet var att det fanns någon restaurang i Tibro med "bra pris, går bra att äta". Jag skulle dock inte ens åka till Tibro, så det var inte till mycket hjälp.

En titt på kartan visade att jag snart skulle vara framme i ett litet samhälle som hette Beateberg, så jag satte mina förhoppningar till att det kanske skulle finnas något att äta där. Det gjorde det inte. Det enda som verkade finnas där, förutom kor och hus, var en kyrka, som dock var otroligt imponerande.

![Beateberg](/beateberg.jpg)

Jag cyklade alltså vidare, allt hungrigare. Motvinden i kombination med hungern gjorde mig på dåligt humör, och förtog i princip all njutning av cyklingen. För att försöka dämpa min hunger något stannade jag till på vägen och intog min sista foodbar. Under tiden jag tuggade i mig den så betraktade jag ett gäng kor som stog i en hage precis bredvid vägen och idisslade. Från början nöjde dom sig med att stå på avstånd och betrakta mig, på det där till synes intresserade sättet som kor ofta gör. Ungefär samtidigt som jag var färdig med min foodbar hade dom dock vågat närma sig så pass att jag hade kunnat röra vid dom om jag hade velat. 

![Fyra damer](/kor.jpg)

Lite mättare för stunden fortsatte jag, fortfarande med stark motvind. För varje samhälle jag paserade hoppades jag på att hitta någonstans att äta, men det var hopplöst. I vanliga fall brukar jag vara noga med att kolla upp vart det går att äta innan jag ger mig av, men just idag hade jag slarvat med det, och nu fick jag betala priset.

Till sist närmade jag mig ett samhälle vid namn Tidan, som såg ut att vara lite större än dom tidigare. Jag satte allt mitt hopp till att hitta något att äta där, och blev överlycklig när jag åkte förbi en liten kiosk. Om inte annat så borde dom ha korv där, tänkte jag, och det hade dom också. Men jag behövde inte nöja mig med det, för det satt en lapp på dörren som nämnde ett luncherbjudande på ortens pizzeria. Jag köpte två Red Bull, och passade på att fråga killen bakom disken om pizzerian. Den låg tydligen en bit in i samhället, och med hans instruktioner hittade jag dit.

Först trodde jag att stället var stängt, för det såg otroligt dött ut, men jag drog i dörren, och den öppnades. Menyn var rätt stor och varierande för att vara en pizzeria, men jag antar att det är så det blir när man är den enda restaurangen i ett samhälle. Det var inte helt lätt att bestämma sig, men i slutändan föll mitt val på en italiensk pizza med mozarella, pepperoni, soltorkade tomater, svamp och ruccula.

Jag satte mig utomhus för att kunna hålla uppsikt över min cykel, och blev snart serverad pizzan som var rejält flottig på grund av mozarellan. Precis vad jag behövde. Jag tröck i mig pizzan på mindre än 10 minuter, ett tydligt tecken på min hunger, och svalde ner den med en av dom Red Bull jag hade köpt. 

Mätt och belåten gav jag mig så iväg igen. Solen hade även börjat titta fram, och helt plötsligt kändes allting mycket lättare. Jag valde att följa Sverigeleden, vilket innebar en liten avstickare genom ett samhälle vid namn Flistad. Det visade sig dock vara ett underbart litet samhälle, så det var väl värt omvägen.

Hittils hade dagen mestadels bestått av flack terräng, alternativt nedförsbackar, även om motvinden förvandlat nedförsbackarna till platta vägar, och den flacka terrängen till uppförsbackar rent ansträngningsmässigt. Nu väntade dock dom riktiga uppförsbackarna, och dom var inte nådiga. Faktum är att den backe som jag mötte efter Övertorp var minst lika illa som den värsta backen dagen innan. Motvinden hade tröttat ut mig rejält, och den här dagen hade knappast blivit den vila jag hade väntat mig. Mina ben var så pass trötta att jag kände att 
ett försök att cykla upp för backen kunde sluta illa, så jag vek mig för dess stigning och drog cykeln uppför.

Backen tog mig till samhället Lerdala, som var minst lika vackert som Flistad. Jag beundrade omgivningarna samtidigt som jag avverkade den sista stigningen för dagen. Den sista biten till pensionatet var lyckligtvis en rejäl nedförsbacke, och inte ens motvinden kunde stoppa min framfart.

Jag var framme lite tidigare än planerat. Eftersom Margareta i princip driver stället själv så vill hon gärna att man uppger vilken tid man ska komma, så att hon inte behöver stå i receptionen hela dagen. Därför blev jag inte speciellt förvånad när jag fann att ingen stod där vid min ankomst. Lyckligtvis var det inte lång tid kvar  tills kl 5, då jag egentligen skulle ha kommit fram, så jag satte mig på verandan för att ladda upp dagens aktivitet till Strava från min cykeldator.

Föga förvånande så hade ett försiktigt regn börjat falla igen. Maken till ostabilt väder får man leta efter. Jag undrar om det alltid är så här nere, eller om det är extremt just nu. Det känns som om vädret *brukar* vara betydligt mer stabilt hemma, men det kanske beror på att jag inte är ute fullt lika mycket där?

Jag behövde inte vänta länge innan Margareta kom ut på verandan. Hon hade tydligen varit någonstans i huset, och hört att någon var i receptionen. Hon visade mig till mitt rum, Röda rummet, med egen ingång mot verandan. Det passade mig utmärkt, för då kunde jag enkelt ta in mina cykelväskor.

Rummet gjorde verkligen skäl för namnet. Allting där inne gick i rött, inklusive väggarna. Inredningen och möblemanget var intressant, och definitivt inte min stil, men det kändes kärleksfullt inrett. Margareta informerade mig även om att middagen skulle serveras klockan halv 7 istället för 7, eftersom hon hade "två systrar på besök som ville äta tidigare". Ordvalet fick mig att tro att det var hennes systrar, men det skulle det snart visa sig att det inte var.

Min första anhalt var badrummet, där jag tog en varm, ångande dusch. Jag tror aldrig det kännts så skönt att ta en dusch faktiskt. Sedan gjorde jag mig hemmastadd och skrev lite på bloggen innan det var dags för middag.

Middagen serverades på glasverandan på baksidan av huset, med fönster som vätte ner mot sjön och trädgården utanför. Det var dukat för mig och systrarna vid varsitt bord, med gott om avstånd, och dom satt redan på sina platser när jag kom dit.

När Margareta kom för att servera förrätten introducerade hon oss för varandra, och vi började prata. Dom var jättetrevliga, och vi pratade oss igenom hela middagen. Tydligen var dom systrar med varandra, men inte med Margareta som jag hade trott, och dom var uppväxta på en gård inte långt ifrån pensionatet. Just nu åkte dom runt i Sverige för att spela golf.

Maten var verkligen jättegod, och jag njöt till fullo av varenda rätt. Att alla ingredienser var ekologiska, med kött från naturbetande kor gjorde dessutom det hela ännu bättre. Mycket av det som serverades var till och med odlat i trädgården utanför av Margareta och hennes man.

Efter maten passade jag på att ta ett dopp i sjön. Det var jättekallt, men uppfriskande. Kanske var det därför jag somnade ovanligt snabbt den natten.
