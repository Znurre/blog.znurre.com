+++
draft = false
title = "Cykelsemester 2020: Dag 2"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-07"
type = "post"

+++

# Cykelsemester 2020: Dag 2

Jag vaknade upp den andra dagen av min resa efter att tyvärr inte ha sovit speciellt bra. Vädret ute till trots var det ganska varmt på rummet, och dom täcken som fanns var rejält varma. För att få en behagligare temperatur att sova i öppnade jag därför fönstret, vilket dock medförde två problem.

För det första var fönstret, i singular, av den typ som öppnas uppåt och utåt. Den svenska sommarnatten är fortfarande ganska ljus, och därför ville jag gärna mörklägga rummet med den rullgardin som fanns. Men en rullgardin i kombination med ett sånt fönster gör att rullgardinen kan färdas en hel del, och det blåste ganska mycket ute. Det gjorde att rullgardinen svängde fram och tillbaka i vinden, och slog i fönstret med jämna mellanrum, vilket såklart lät en hel del.

Genom att hissa upp gardinen en bit fick vinden inte tag i den lika lätt, men dess ljusutestängande egenskaper blev såklart också lidande som en följd, och alltså fick jag stå ut med en cirka 2 dm stor glipa mellan gardinen och nederkanten av fönstret som såklart släppte in rejält med ljus.

Mitt fönster vätte även mot järnvägen som gick bara ett femtiotal meter utanför, och godstågen åkte förbi fram till midnatt. 

Inte så goda förutsättningar för att sova således, men jag kände mig ändå ganska utvilad när jag vaknade, och framförallt var jag entusiastisk över att komma ut på vägen igen.

Jag sminkade mig och gick och åt frukost som serverades i form av buffé. Flertalet åtgärder hade vidtagits för att minska eventuell smittspridning, som att t.ex. bara tillåta 6 personer samtidigt vid buffén. Frågan är hur pass effektivt det var dock. Den svagaste länken kommer alltid vara dom personer som är där, och jag såg en äldre man som gick och hostade, i händerna, och som sedan tog upp brödet ur brödkorgen med dessa händer utan att ens använda den tång som fanns för detta ändamål.

Frukostbuffén i sig var helt okej. Inte i närheten av den bästa frukostbuffé jag varit med om, men även långt ifrån den sämsta.

Efter att ha intagit min frukost gick jag till mitt rum och bytte om till cykelkläder, packade allt, och gick sedan till receptionen för att checka ut och få tillgång till min cykel som var inlåst i deras förråd. Jag monterade cykelväskorna på cykeln, och så bar det av igen, förbi samma fält av blommor som dagen innan med sikten inställd på Vallsta som var min första anhalt för dagen. 

Där stannade jag på en mack för att köpa 2 st Red Bull, varav jag drack en på plats för att få lite extra energi inför resan och för att få min nödvändiga dos av morgonkoffein. Den andra packade jag ner för att inta senare under dagen.

Färden gick sedan vidare mot Bollnäs. Jag hade tagit på mig min vind- och regntäta jacka vid macken eftersom det var ganska kyligt i luften, speciellt med tanke på att det blåste ganska mycket och solen låg bakom tjocka, mörka moln. Jag upptäckte dock rätt snabbt att den inte var allt för bekväm att ha på sig, då den inte andas överhuvudtaget.

Sträckan fram till Bollnäs gick annars bra. Lite väl trafikerad väg för min smak, utan någon direkt väggren, dock inte alls lika illa som 84an. Som tur var sträckan inte så lång, så jag kunde snabbt vika av på cykelvägar när jag kom fram till Bollnäs. Jag reagerade även på hur pass bra cykelvägarna verkar vara i Bollnäs jämfört med Hudiksvall, trots att Bollnäs är en mindre stad.

Min vistelse i Bollnäs blev väldigt kort, och jag var snart ute på andra sidan utan att ha sett stadens centrum. Vägen fortsatte, skyltad mot ett litet samhälle vid namn Annefors. Jag hade kollat på höjdkurvan innan avfärd, men inte insett riktigt hur illa det skulle vara. Redan här visade vägen prov på vad som komma skulle, och det blev inte bättre av att jag hade motvind hela vägen till Annefors.

Vägen gick förbi ett litet samhälle vid namn Finnfara. Den här delen av vägen bjöd på rikligt med båda upp- och nerförsbackar, tillsammans med snäva kurvor, och var faktiskt riktigt rolig. Den fina omgivningen gjorde även sitt.

Här hade jag ursprungligen planerat att stanna till. Dels för att bada om vädret hade tillåtit det, eftersom det finns en jättefin strand i området, och dels för att äta lunch på vad som verkade vara en jättemysig gård vid namn "Gårdstunet". Tyvärr var det fortfarande ganska kallt, och solen visade inga tecken på att komma fram, så något bad var det inte tal om, och Gårdstunet visade sig endast vara öppet torsdag-söndag. Till nästa gång får jag se till att göra lite bättre research helt enkelt.

Det var dock väldigt olyckligt. Gårdstunet var den sista utposten innan den del av sträckan började som jag själv namngivit "dödens sträcka". Namnet kommer sig av att det under cirka 8 mil inte finns anstymmelse till affär, restaurang eller hotell. Det är faktiskt den enda bit på hela min cykelsemester jag hittat där det är så pass tomt på bebyggelse och faciliteter av något slag.

Jag hade dock förutsett detta, och köpt med mig måltidsersättning av just den anledningen. Jag intog därför en tråkig lunch i form av den Red Bull jag hade kvar samt en "food bar" med blåbärssmak.

![Rastplatsen](/rastplatsen.jpg)

Efter Annefors blev det grusväg i några mil fram till det lilla samhället som heter Gruvberget, där det återigen blev asfalt i några km. Vägen ringlade sig alltjämt sakta uppåt, och mina ben började bli rätt så möra vid det här laget. En trevlig överraskning kom dock i form av solen, som letade sig fram bakom molnen.

Den sista sträckan väg innan Gruvberget förtjänar ett eget stycke. Jag har nog aldrig varit med om en så fascinerande väg. Den var upphöjd cirka 20 meter eller så i förhållande till omgivningen på både vänster och höger sida, och med vägräcken överallt. Faktum är att jag fick en känsla av att vara på zoo eller safari. Som om jag åkte där uppe i tryggheten, medans farliga djur strök omkring långt där nere för mig att beskåda.

Jag antar att en anledning till denna något underliga design kan ha varit ett försök att plana ut vägen, men det motsägs delvis av att vägen knappast var plan. Förvisso var den fri från extrema uppförs- och nedförsbackar, men den rullade fram som dyningar allt eftersom den krängde höger och vänster.

Innan Gruvberget svängde jag av, och befann mig inom kort på grusväg igen. Den började med en rejäl nedförsbacke, men jag kunde tydligt se den största backe jag sett hittils denna resa torna upp sig några hundra meter längre fram. Jag vet från erfarenhet att backar ofta ser värre ut än vad dom faktiskt är, och den här var inget undantag. Trots dess till synes respektingivande lutning klarade jag den utan större problem, men bakom nästa sväng kom en till backe, och så en till, och en till...

Någonstans här slutade dock äntligen stigningen, och det började sakta slutta något. Jag kunde hålla rätt bra tempo, och susade fram på grusvägen, ackompanjerad av dofterna från myrmark, varmt lingonris och ljung.

Kanske 1-2 mil senare kom jag fram till ett litet samhälle vid namn Svartnäs. Här fanns förrutom några enstaka hus en rastplats och en kyrka, fint belägna precis vid mynningen till en sjö.

![Svartnäs](/svartnas.jpg)

Här träffade jag även på en annan cyklist. En kille satt nämligen vid ett av borden vid rastplatsen, med en fullpackad cykel, och knappade på sin mobiltelefon. Jag gick fram och hälsade och frågade om han också var på cykelsemester, varpå han svarade "Ja, men pratar du även engelska?". Vi bytte till engelska, och det framkom att han var nederländare och att han för närvarande studerade i Östersund.

Nu skulle han dock ha ett studieuppehåll, och därför var han på väg mot Göteborg, där hans kompis skulle hämta upp honom med bil och köra honom den sista biten till Nederländerna. Vi fortsatte prata ett bra tag om cykling, och han verkade trevlig, så jag frågade vad han hade för planer för dagen.

Det visade sig att han skulle till *nästan* exakt samma ställe som mig! Till skillnad från mig så bodde han inte på hotell, utan campade, och hade med sig all utrustning som krävdes för att klara sig. Därför skulle han stanna till vid ett vindskydd en bit innan mitt boende, men vi slog följe fram tills dess.

Vi pratade hela tiden, och dom sista två milen gick väldigt fort. Vädret fortsatte vara oberäkneligt, och gick från halvmulet till mulet, till regn, och till sist till hagel. Sista biten ner till sjön där våra respektive slutmål för dagen låg bjöd på en lång nedförsbacke. Solen bröt igenom molnen, och vi susade ner för den flera kilometer långa backen i ett solblänkande dis. Det var en fantastisk känsla.

Så kom vi fram till den korsning där vi skulle skiljas åt. Vi bytte telefonnummer och Strava-profiler med varandra, och bestämde att vi skulle hålla kontakten för att se om vi kunde slå följe även dagen efter.

Jag fortsatte sedan cirka 1 km till, och korsade den bro som gick över sjön. På andra sidan låg mitt boende för natten, Trollnäs, ett över 100 år gammalt hus som blickade ut över vattnet.

![Trollnäs](/trollnas.jpg)

Huset har precis bytt ägare, och drivs numera av en familj som har bestämt sig för att satsa på att rusta upp stället. Till sin hjälp har dom anställt en barndomsvän, Anna, som håller i det mesta på hotellet. Det var hon som välkomnade mig när jag kom dit, hjälpte mig att checka in och visade mig rummet jag skulle få bo i. Hon hade valt ut ett jättegulligt litet rum med eget badrum och toalett åt mig.

Klockan 6 blev jag serverad hemlagad lasagne som var fantastiskt god. Den kom med en sallad, och en lokalproducerad alkoholfri cider. Jag hade ingen aning om att lasagne kunde vara så gott.

![Lasagne](/lasagne.jpg)

Efter maten gick jag ut i omgivningarna, eftersom solen sken och skapade förutsättningara för en fantastisk sommarkväll. Det finns inte så mycket i omgivningen då Svärdsjö, som är närmaste samhälle, ligger cirka 2 km bort. Istället gick jag ner till den strand som fanns precis utanför hotellet och kände på vattnet. Jag blev förvånad över hur pass varmt det var. Varmare än i luften, utan tvekan. Ett tag funderade jag på att ta ett dopp, men jag kom på bättre tankar när mörka molk började dra in igen.

![Sjö i sommarkväll](/trollnas_kvall.jpg)

Bara ett kort tag senare när jag åter var tillbaka på mitt rum så började regna igen. Jag förberedde det sista inför morgondagen och gjorde mig redo för att sova, med förhoppningen om att sova bättre än natten innan.
