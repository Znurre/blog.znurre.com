+++
draft = false
title = "Cykelsemester 2020: Dag 17"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-23"
type = "post"

+++

# Cykelsemester 2020: Dag 17

Dagen började tidigt som vanligt, efter en natt med undermålig sömnkvalitet. Mitt hotellfönster vätte ner mot gatan, och det visade sig vara en minst sagt högtrafikerad gata. Att sova med stängt fönster var inte heller ett alternativ, eftersom det var varmt nog på rummet även med fönstret öppet. Efter att ha klätt på mig gick jag ner till hotellets restaurang för att inta min frukost. Det var en stor buffé, och jag passade på att äta flera rejäla portioner som vanligt. Innan utcheckningen besökte jag en närbelägen ICA-butik för att köpa dagens första Red Bull som jag sedan intog på hotellrummet medans jag packade och förberedde mig för avfärd.

Efter att ha fått hjälp att hämta upp cykeln ur källaren gav jag mig iväg, med siktet inställt på Tierp. Luften var kall och fick det att kännas som höst snarare än sommar. Som tur var så hade solen hittat fram, och gjorde att jag klarade mig utan jacka. Det blåste en stark nordvästlig vind, vilket initialt passade mig utmärkt. Dom första 2 milen eller så var snabbt avklarade, med vind snett från sidan som förde mig framåt över kullarna. Därefter blev det dock betydligt jobbigare, eftersom vägen svängde av västerut, vilket gav mig motvind istället. Så där fortsatte det, med ömsom medvind, ömsom motvind, allt eftersom jag kryssade mig fram på dom uppländska vägarna.

Jag blev förvånad över att finna att Uppland var så vackert. Faktum är att av alla dom landskap jag har cyklat genom så hamnar nog Uppland på plats 3 eller 4, med sina stora fält fulla av allsköns spannmål och blommor.

![Uppland](/uppland.jpg)

I ett litet samhälle vid namn Vänge stannade jag till för att göra ett snabbt besök på en liten kiosk, där jag köpte en Vitamin Well och en glass som jag intog sittandes utanför butiken i solen där det var lä. Med denna välbehövliga vila avklarad fortsatte jag sedan på slingrande vägar, fortfarande med stark motvind.

Den energi jag fått i mig i Vänge räckte inte länge under dessa förhållanden, och jag blev glad att finna att det fanns en lanthandel i det lilla samhället vid namn Oxsätra. Det verkade dock inte finnas någon mat där, iallafall ingenting som skulle kunna intas utan att först tillaga det, så jag fick nöja mig med två stycken Sportlunch som jag skölde ner med dagens andra Vitamin Well.

Vid det här laget hade jag tillryggalagt hälften av dagens etapp. Återstoden av vägen gick mestadels via mindre grusvägar som erbjöd gott om vindskydd. Detta i kombination med den allt plattare terrängen gjorde cykelturen väldigt enkel och behaglig. Däremot oroade jag mig allt mer för vildsvin. Morsan har pratat mycket om dom stora vildsvinsbestånden i dessa trakter, och jag cyklade hela tiden på helspänn för den händelse att ett skulle springa fram ett framför cykeln på någon av dessa små grusvägarna.

Det gick dock bra, och jag närmade mig mitt slutmål för dagen. När jag bara hade några kilometer kvar så stötte jag på en minst sagt intressant syn i form av ett par som var ute och rastade hunden - med hjälp av en fyrhjuling..

Några minuter senare cyklade jag in på uppfarten hos Jenny, och fick ett hjärtligt mottagande. Vi gick in tillsammans och hungrig som jag var så serverade jag mig själv två ugnsrostade mackor med salami och smält ost, som jag sköljde ner med en Red Bull. Nästa punkt på listan var en varm dusch, och efter det installerade jag mig själv i det sovrum på övervåningen som verkade fungera som kombinerat lagringsutrymme och gästrum.

Dagens middag bestod av tre rätter, som barnen i familjen stod för. Dom hade tydligen förlorat en snigelletartävling som gick ut på att hitta så många mördarsniglar som möjligt, och priset för vinnarna var en trerätters middag. Lägligt nog inföll den precis den dag som jag var på besök.

Förrätten bestod av Toast skagen, som hade blivit ett återkommande tema vid det här laget. Huvudrätten var däremot hamburgare, som jag faktiskt inte ätit en enda gång den här resan. Till efterrätt serverades vaniljglas med vinbär och kolasås. Allt som allt en mycket trevligt middag.

Nu kanske man skulle kunna tro att jag hade fått nog av cykling, och skulle utnyttja det faktum att jag hade fått en vilodag till att faktiskt ta det lugnt, men icke. Istället gav jag mig ut med barnen och cyklade några kilometer efter middagen, och faktum är att just denna korta cykeltur fick mig att inse hur pass mycket jag gillar att cykla, och jag började känna att en vilodag var det jag minst av allt ville ha.

Nåja. Det fick bli morgondagens problem. Nu var det dags att sova.
