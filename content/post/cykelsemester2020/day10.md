+++
draft = false
title = "Cykelsemester 2020: Dag 10"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-16"
type = "post"

+++

# Cykelsemester 2020: Dag 10

Jag vaknade upp efter en natt med betydligt bättre sömn än dagen innan. Ljudet från gatan till trots hade det visat sig vara en god ide att öppna fönstret, för det hade varit betydligt svalare i rummet. Lisa var redan vaken, och jag gjorde henne sällskap i köket för att inta min frukost.

Efter frukosten gjorde jag mig redo, och packade mina nya cykelväskor. Det var lite klurigare att få plats med allt. Även om dessa nya väskor hade större volym på pappret, så gjorde deras smalare formfaktor det svårare att packa ner allting på ett effektivt sätt.

Jag passade även på att be Lisa fläta mitt hår, eftersom det är väldigt skönt att ha det uppsatt när man cyklar. Själv är jag dock helt värdelös på att fläta mitt eget hår, så det blir inte gjort speciellt ofta.

Vi hade bestämt att vi skulle äta lunch en sista gång på samma ställe som dagen innan, men klockan var fortfarande inte speciellt mycket. Därför gick Lisa på en springtur med 2 kompisar, medans jag satt och vilade i parken. Det var inget vidare trevligt väder, kallt och blåsigt, så jag fick ta på mig både mina sleeves och min jacka för att inte frysa.

När Lisas springtur var över gick vi och beställde maten. Jag valde en lammfärsburgare, ytterligare en av dom rätter som kocken hade talat väldigt gott om. Det var väldigt mycket mat, 2 stora skivor med kött, och precis som dagen innan var även denna rätt ganska svåräten. Men gott var det, och jag blev väldigt mätt.

Efter lunchen var det sedan dags att säga hejdå. Jag hade tagit med mig cykeln, så jag var redo att sticka, och efter att ha tagit avsked av varandra cyklade jag iväg norrut, ut ifrån Malmö. Ungefär 7-8 km senare upptäckte jag att cykeldatorn inte visade på någon hastighet. Tydligen hade hastighetsmätaren hoppat lös igen, troligen efter servicen. Jag rättade till den och fortsatte.

Det var helt otroligt underbart att cykla med dom nya cykelväskorna, och med en dags vila var jag full av energi. Tillsammans med den obefintliga stigningen gjorde det att jag bara flög fram, och innan jag visste ordet av var jag i närheten av Lund.

Allt var inte guld och gröna skogar dock. Det hade blåst sydliga eller västliga vindar varje dag sedan jag åkte från Tiveden, så jag hade tänkt att jag kanske skulle få medvind på hemvägen, men nej så bra skulle vi inte ha det. Ute vid kusten blåste det tydligen fortfarande sydliga vindar, vilket hade gett mig en fin start, men här lite längre in i landet kom vinden från nordväst istället.

Jag hade inte heller fått i mig något koffein den här dagen, så jag hade tänkt stanna någonstans på vägen och köpa något att dricka. Helst ville jag hitta någon slags bemannad mack eller liknande, med en liten butik, så att jag slapp lämna cykeln obevakad så länge, men det verkar finnas dåligt med sånna här nere i Skåne.

Istället så satte jag mina förhoppningar till Lund, och tänkte att där måste det ju finnas någon liten butik. Men helt plötsligt så såg jag en skylt som pekade bakåt mot Lund, och insåg att jag hade åkt igenom det. Troligen måste jag ha åkt precis i utkanterna, för det lilla jag såg verkade knappast som en stad med 90.000 invånare.

Nåja, det kommer väl fler chanser tänkte jag. Jag skulle cykla igenom fler samhällen, och någonstans måste det väl ändå finnas en butik. Eller?

Jag åkte igenom samhälle efter samhälle, och ingenstans fanns det en butik. Ett ställe hade till och med en däckbutik och en bilverkstad, men ingen mataffär eller ens en liten kiosk. Vid det här laget började jag bli måttligt irriterad, och uppdaterade min "hatlista" till att se ut enligt följande:

1. Motvind
2. Samhällen utan butiker
3. Rumpsvett

Till sist kom jag till Höör, som var ett större samhälle och som hade en butik, och jag kunde äntligen få min koffeinabstinens stillad. Återstoden av vägen till Hässleholm gick på små, fina vägar genom tät bokskog som bildade som ett tak över vägen.

Väl framme vid hotellet gick jag för att checka in, och fick ett mycket bra bemötande av personalen som även hjälpte mig att ställa in cykeln i deras baggagerum. Jag lyckades även tidigarelägga min middagsbokning, som egentligen skulle ha ägt rum klockan 7. Dagens cykeltur hade gått oväntat snabt, och klockan var nu bara runt 5. Även om jag hade stått mig bra på den enorma hamburgaren så började mättheten definitivt att klinga av vid det här laget, och jag började känna att middag skulle sitta fint.

Efter att ha duschat och fixat till mig gick jag därför till restaurangen klockan 6.30 för att äta, och blev visad till ett bord av en mycket trevlig, manlig servitör som pratade med en dialekt som fick mig att tänka på komikern Jesper Rönndahl.

Jag beställde 3 rätter, som började med en förrätt bestående av scampi i tomat- och chilisås, varmrätt i form av flankstek med rostad potatis, och en marängswiss-inspirerad efterrätt.

Maten kom snabbt, och det kändes som om servitören verkligen brydde sig om att jag skulle få bra service. Förrätten var helt okej. Inte den bästa jag ätit hittils, men långt ifrån den sämsta. Varmrätten var riktigt bra, men lite svåräten eftersom jag inte fått någon köttkniv. Efterrätten var även den bra, och väldigt stor.

Mätt och belåten gick jag upp på mitt rum för att ordna med lite saker innan jag till sist gick och lade mig.
