+++
draft = false
title = "Cykelsemester 2020: Dag 7"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-12"
type = "post"

+++

# Cykelsemester 2020: Dag 7

Jag vaknade upp i det mycket röda rummet efter vad som kändes som en ovanligt god natts sömn. Om det berodde på doppet, om det var den avkopplande atmosfären, eller någonting helt annat som gjorde det vet jag inte. Frukosten skulle, precis som natten innan, serveras på en personlig bricka utifrån en beställning som jag fått fylla i i samband med middagen dagen innan. Den här gången var mängden dock lite bättre tilltagen, och jag intog den i morgonsolen ute på verandan på baksidan av huset, tillsammans med dom andra gästerna på pensionatet.

Efter frukosten började jag förbereda för dagens cykeltur som vanligt. Det tog lite längre tid än väntat, och det dröjde tills närmare klockan 11 innan jag var redo för utcheckning och avfärd. Vi pratade ett bra tag vid utcheckningen, jag och Margareta, och vi kom överens om att jag skulle komma tillbaka någon annan gång i framtiden och då stanna lite längre. Hon erbjöd sig till och med att hämta upp mig i Skövde om jag skulle komma med tåg! Jag blev även rekommenderad lite sevärdheter på vägen.

Så gav jag mig iväg. Vädret var faktiskt riktigt fint, och vägen var lättcyklad. Jag cyklade genom lummiga träalléer och böljande landskap med fält och åkrar som såg ut som långhåriga ryamattor när veten och havren vajade i vinden. Det gjorde mig även glad att se alla naturbetande kor och får som såg ut att ha det riktigt bra.

Mitt första stopp för dagen blev på Naturum vid Hornborgasjön där jag även passade på att fika lite. Jag hade inte kommit speciellt långt, 1 eller 2 mil kanske, men jag hade verkligen ingen brådska utan kunde unna mig att stanna till så mycket jag ville. Idag skulle jag nämligen bara behöva cykla 8 mil, och inte nog med det, det skulle vara knappt 400 meter total stigning. Rena rama semestern alltså.

Efter fikapausen satte jag av igen, och fortsatte på lika vackra vägar. När jag hade kommit till samhället Vartofta valde jag att göra ett litet avsteg från den planerade rutten. Det var nämligen så att jag ramlade på Västgötaleden, och bestämde mig för att följa den istället. Det tog mig in på en liten väg, i stil med dom tidigare vägarna jag hade åkt på, med väldigt lite biltrafik. 

![Fina vägar](/skara.jpg)

Ungefär en halvtimme senare cyklade jag in i samhället Åsarp. Här inne började den banvall som jag skulle följa resten av vägen till Ulricehamn, som var mitt slutmål för dagen. Innan dess så ville jag dock få någonting i magen, så jag cyklade till mitt planerade lunchstopp för dagen - Ekehagens Forntidsby. Det hade legat på vägen om jag tagit den rutt jag ursprungligen planerat, men nu fick jag istället ta en liten omväg.

När jag kommit fram var jag riktigt hungrig, så jag slösade ingen tid, utan gick direkt till restaurangen. Jag beställde en vegetarisk blandtallrik och satte mig och väntade. Maten kom oväntat snabbt, och gud vad gott det var. Tallriken innehöll bitar av grönsakspaj, raggmunk med lingonsylt och någon slags kikärtsröra med vitlök, och allt smakade fantastiskt. Mätt och belåten cyklade jag tillbaka till Åsarp där jag svängde in på banvallen som skulle ta mig till Ulricehamn.

![Banvallen](/banvall.jpg)

Denna banvall sträcker sig, av vad jag har kunnat läsa mig till, hela vägen mellan Falköping och Sjötofta. Efter att järnvägen lades ner köpte kommunerna upp banvallen, och asfalterade delar av den för att kunna fungera som cykelväg.

Som väntat var banvallen mycket lättcyklad. Det jag inte hade väntat mig, och som var väldigt trevligt, var att sträckan var full av små bänkar vid natursköna platser där man kunde sätta sig och vila. Det fanns även gott om små caféer i anknytning till cykelbanan i dom små samhällen man åkte igenom. Sträckan verkade vara populär, för jag såg väldigt mycket folk som var ute och gick, cyklade eller åkte rullskridskor.

![Rastplats vid banvallen](/banvall_bank.jpg)

Efter att ha åkt igenom ett antal mindre samhällen kom sedan turen till Ulricehamn, där jag rullade in någon gång efter klockan 4 på eftermiddagen. Det här var nog första dagen jag kände att jag kunde ha cyklat lite längre utan att det hade gjort något, men det var skönt att vara framme så pass tidigt. Jag hittade snabbt hotellet jag skulle bo på, Bogesund, som låg på en kulle och blickade ut över sjön Åsunden. 

Incheckningen gick smidigt. Jag nämnde att jag hade en cykel med mig, och till min förvåning sade killen i receptionen att dom gjort om ett konferensrum till cykelförråd eftersom det var så pass mycket cyklister som bodde där just nu. Jag frågade även om middagen, och fick veta att deras restaurang var semesterstängd. Det var ett rejält bakslag, eftersom jag valt hotellet baserat på just deras restaurang. 

Istället fick jag leta efter någon annanstans att äta middag, vilket visade sig vara lättare sagt än gjort. Av dom restauranger som fanns i staden var dom flesta inte öppna eftersom det var söndag. Jag kunde antingen välja mellan ett antal pizzerior, kina-, thai-, indisk- eller japansk restaurang. 

Det kanske låter som gott om valalternativ, men jag kände inte för snabbmat, så pizzeriorna gick bort direkt. Istället kollade jag på dom andra restaurangernas menyer online, och blev bestört. Kinarestaurangen hade fler icke-kinesiska rätter på sin meny än kinesiska, t.ex. pokébowls (Hawaii), sushi/karaage (Japan), bibimbap (Korea), pad thai (Thailand), osv, och thai-restaurangen hade stekt fläskfilé med bea och pommes på sin meny?! 

I slutändan valde jag ändå att gå på kina-restaurangen, mest för att killen i receptionen hade rekommenderat den och för att den låg på andra sidan gatan från hotellet. Jag satte även mina förhoppningar till att menyn på internet bara var en takeaway-meny, och att den riktiga menyn skulle vara större och bättre. 

Stället såg trevligt ut invändigt, och jag fick ett bord i en liten utbyggnad med fönster mot gatan. Tyvärr blev jag besviken vad det gällde menyn, för den var identisk med online-varianten. Jag valde friterad kyckling med sötsursås och ris, mest för att jag tänkte att det är en sådan standardrätt på kina-restauranger att det väl knappast går att misslyckas med det.

Kanske är det bara jag som är konstig, men jag hade förväntat mig att få någon slags gryta med sötsursås, grönsaker och friterad kyckling. Istället fick jag typ chicken nuggets i en skål, sötsur sås i en annan, och så ris. Inga grönsaker eller nånting annat. Besviken åt jag den torftiga maten som troligen var det sämsta jag ätit hittils denna resa. Det var en helt absurd mängd kyckling, så jag blev iallafall mätt.

Efter att ha betalat och gått där ifrån så passade jag på att kolla mig runt lite i staden. Det verkade vara en trevlig stad, och jag skulle gärna ha spenderat lite mer tid där.

Nu var det dock dags att sova, för morgondagen skulle bjuda på den längsta cykelturen hittils denna resa.
