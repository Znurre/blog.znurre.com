+++
draft = false
title = "Cykelsemester 2020: Dag 13"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-19"
type = "post"

+++

# Cykelsemester 2020: Dag 13

Jag vaknade upp i sängen på Wallby säteri efter en hyfsad natts sömn, och kände mig redo att tackla dagens utmaningar. Men först ville jag få lite frukost i magen, så jag gick till receptionsbyggnaden som även huserade frukostbuffén. Buffén var rätt så bra, och jag åt mig mätt och belåten.

När det sedan var dags för utcheckning och avfärd så diskuterade jag dagens  planerade rutt med hotellföreståndaren som rekommenderade en alternativ väg som skulle vara finare, och som faktiskt blev kortare. Tacksamt accepterade jag det förslaget.

Vägen började mycket riktigt väldigt vackert, med öppna, bördiga landskap och utsikt över en sjö. Backarna från dagen innan fortsatte dock. Faktum är att landskapet här i Småland är väldigt annorlunda från andra landskap jag cyklat genom i Sverige.

Istället för enstaka stora backar och stora partier med antingen svagt sluttande nedför eller svagt sluttande uppför så gick vägarna ideligen upp och ner för vad som bäst kan beskrivas som kullar. Den ena branta backen efter den andra följdes av nedförsbackar så fort jag hade nått ett krön, och jag lärde mig snart att hata dessa nedförsbackar eftersom jag visste att dom skulle följas av ytterligare en uppförsbacke.

Man kan tycka att jag borde ha lärt mig min läxa vid det här laget, men uppenbarligen inte. Jag hade nämligen återigen ingen Red Bull med mig, och det närmaste samhället med butik hade varit Vetlanda, vilket hade inneburit en rejäl omväg. Istället satte jag fart mot nästa samhälle med butik, Mariannelund, där jag även planerat att äta lunch.

Redan när jag hade läst namnet Mariannelund så tyckte jag att det kändes väldigt bekant. Det slog mig snart att det ju var en plats från Emil i Lönneberga. Med tanke på att jag började närma mig Vimmerby var detta knappast någon överraskning.

Efter att ha börjat så fint blev vägen mycket snart  skittråkig. Den gick bland annat genom ett samhälle vid namn Pauliström som bara verkade bestå av industrier, och utöver det bestod vägen främst av blandskog.

Den tråkiga omgivningen i kombination med min önskan om att snabbt komma fram för att få i mig dagens första dos av koffein och något att äta gjorde att jag avverkade vägen till Mariannelund på rekordtid.

Väl framme parkerade jag cykeln utanför den restaurang där jag planerat att inta min lunch. Restaurangen hette "Paj och pilsner", och som namnet antyder serverade dom pajer av olika slag. Jag beställde en köttfärspaj och gick ut och satte mig i solen för att vänta på att få den serverad. Under tiden smuttade jag på en apelsinläsk för att släcka törsten.

Efter bara några minuter kom en liten flicka ut med min paj. Jag antar att hon var barn eller barnbarn till dom som drev stället. Pajen var god. Faktum är att jag skulle säga att den var bättre än den paj jag ätit dagen innan på White guide-stället. Med mat i magen gick jag sedan till den ICA-butik som var belägen bara cirka 50 meter bort för att köpa på mig några burkar Red Bull.

Med tanke på att jag inte visste med säkerhet on jag skulle stöta på någon mer butik denna dag så ansåg jag det klokt att köpa på mig minst en extra. Vid det här laget hade jag nämligen börjat lära mig av mina tidigare misstag.

Efter Mariannelund fortsatte kullarna. Omgivningen var dessutom precis lika tråkig som tidigare. Småland måste verkligen vara Sveriges tråkigaste landskap. Dessutom hade jag börjat störa mig rejält på dom flugor som verkade ha positionerat sig taktiskt i uppförsbackarna. Under dom allra värsta etapperna uppför blev jag nämligen gång på gång antastad av vad som närmast kan beskrivas som svärmar av flugor. Dom verkade verkligen besatta av att få slå sig ner på min svettiga kropp, och eftersom jag befann mig i uppförsbacke kunde jag inte heller cykla tillräckligt snabbt för att lägga dom bakom mig. Istället fick jag försöka vifta runt omkring mig som en idiot för att hålla dom på avstånd. Nu vet jag hur dom stackars kossorna känner sig.

Dagens hatlista bestod därför föga förvånande av följande punkter:

1. Flugor
2. Skittråkig väg genom blandskog
3. Kullar

Alla dessa kullar tog på krafterna, och gjorde mig dessutom väldigt törstig. Jag hade såklart vatten med mig, men det var ljummet vid det här laget. Jag kunde även ha druckit min Red Bull som jag hade köpt, men så pass söta drycker är sällan speciellt törstsläckande. Istället stannade jag till vid en ICA-butik i ett samhälle vid namn Södra Vi där jag köpte en Vitamin Well som jag svepte. Efter det kände jag mig redo att tackla dom sista milen till Horn där jag skulle spendera natten i en stuga på en camping.

Förvånande nog visade sig detta vara en brytpunkt, för den återstående vägen var riktigt vacker, om än tyvärr lika full av kullar. Öppna landskap med stenrösen varvades med dunkel barrskog bestående av mäktigt höga granar som tornade upp sig på båda sidor om den smala vägen. Helt enkelt riktigt njutningsfullt.

Till sist kom jag fram till Horn, och hittade snabbt vägen till den camping där jag hade bokat en stuga. Incheckningen gick relativt smidigt, även om personalen inte verkade ha superbra koll på vem som hade bokat vad. Jag fick iallafall en nyckel till stugan, sängkläder och en handduk, och gick sedan till min stuga som var belägen i utkanten av campingen, precis vid entrén.

Övernattningen i Horn hade varit den natt jag varit mest missnöjd med av alla dom boenden jag bokat. På alla andra ställen hade det funnits bra boenden att tillgå, men här hade jag varit tvungen att nöja mig med en camping. Hade jag valt att skippa övernattning i Horn till förmån för någonting annat så hade det inneburit en omväg på flera mil. Istället hade jag försökt övertyga mig själv om att det säkert skulle bli bra, och att det kanske till och med kunde bli mysigt att spendera en natt i en egen liten stuga på en camping.

Men när jag öppnade dörren och möttes av den kvava luften från ett litet utrymme med plåttak, tillsammans med synintrycken från en sunkig korkmatta, en skranglig våningssäng med skumgummimadrasser från Jysk, samt ett bord som inte såg ut att vara från detta århundrade började jag ifrågasätta mina val. När jag dessutom sedan insåg att jag skulle vara tvungen att använda gemensamma duschar och toaletter, för att inte tala om hur jobbigt det skulle bli att sminka sig morgonen efter, så var min enda tanke - "Vad har jag gjort? Hur kunde jag boka boende här egentligen?"

Jag satte mig på den skrangliga sängen för att fundera. När jag precis hade åkt in i Horn så hade jag sett en skylt som hade gjort reklam för ett vandrarhem. Jag vet inte om jag hade missat det när jag bokade boende, eller om jag helt enkelt hade avfärdat det för att det var ett vandrarhem. Min erfarenhet av vandrarhem är inte så jättebra, och jag tänker mig typ ett delat rum med en massa våningssängar och delad toalett och badrum. Jag bestämde mig dock för att kolla in deras sida, och såg att det fanns enkelrum.

Det avgjorde saken. Jag resonerade att vandrarhemmet knappast kunde vara värre än stugan på campingen och ringde därför det nummer som stod angivet på hemsidan. Jag hade oroat mig för att det kanske inte skulle finnas några rum lediga med tanke på min dåliga framförhållning och det faktum att beläggningen på andra boenden jag besökt hade varit så hög, men jag gick det glädjande beskedet att det absolut fanns ett rum till mig.

Jag satte därför av på cykeln igen för att ta mig till vandrarhemmet som bara skulle ligga några hundra meter därifrån. När jag skulle montera cykelväskorna upptäckte jag att en skruv hade lossnat från en av väskorna, vilket gjorde att den inte satt fast helt som den skulle. Troligen hade den på något sätt skakat loss under gårdagens äventyr på ojämn terräng. Hädanefter skulle jag behöva vara försiktigare, intalade jag mig, inte bara för att den saknade skruven riskerade att orsaka ytterligare skador, utan också för att jag ville undvika att andra skruvar lossnade.

Det blev som väntat en kort cykeltur för att ta sig till vandrarhemmet, och när jag kom dit satt Kenneth, en av dom två ägarna till vandrarhemmet, och väntade på mig utanför. Han släppte in mig och gav mig en  kort rundvandring för att visa dom gemensamma utrymmena och mitt rum. Det visade sig att det just nu, tvärt emot vad jag hade befarat, inte bodde några andra gäster där, så jag hade hela stället för mig själv.

Mina förutfattade meningar om vandrarhem kom även på skam. Det var verkligen ett jättemysigt ställe, inrättat i en gammal byggnad som tidigare hade varit hotell, och det syntes tydligt att det drevs med mycket kärlek och passion av  Kenneth och hans fru. Jag hade fått ett litet rum på översta våningen med takfönster, där Kenneth hade bäddat åt mig.

Efter att ha visat mig runt så lämnade Kenneth mig sedan ensam i det stora huset, och det kändes faktiskt lite kusligt. Jag blev dock inte kvar där speciellt länge, eftersom jag vid det här laget hade börjat bli hungrig och sökte mig ut för att hitta något att äta. Den enda restaurang i området som serverade middag visade sig ligga på campingen, och ett tag funderade jag på att gå och handla på ica och laga någonting i det gemensamma köket på vandrarhemmet istället. Latheten tog dock över, och jag gick tillbaka till campingen trots allt.

Det var lång kö till restaurangen, så jag hade gott om tid på mig att överväga vad jag skulle äta. Mitt val föll på korv och pommes som var en av dom rekommenderade rätterna på menyn, vilket jag antar sade ganska mycket om restaurangen.

Jag fick vänta ett bra tag på att maten skulle serveras, och under tiden betraktade jag vad som närmast kunde beskrivas som en traktorparad. En stor mängd gamla traktorer körde in på campingen, en efter en. Jag vet inte om detta tillhörde en organiserad aktivitet på campingen, eller om det var någonting som gjordes på initiativ av personerna i traktorerna. Oavsett vad kändes det som en aktivitet som tydligt befäste dom fördomar personer kan ha om små samhällen som dessa.

Korven var ganska tråkig, men jag var hungrig så den gick ner utan större problem. Till maten hade jag även köpt en krusbärsdryck, mest på grund av att det verkade vara ett populärt val bland dom kunder som stått före mig i kön. Jag hade förväntat mig att den kanske skulle vara lite syrlig och uppfriskande, men jag blev besviken. Den var söt och sliskig, och helt enkelt inte speciellt god alls.

Efter maten passade jag på att kasta in nyckeln till stugan i den postlåda som användes för att returnera nycklar vid utcheckning. På så sätt skulle jag slippa ta i tu med det morgonen efter, utan kunde åka vidare direkt från vandrarhemmet.

Jag åkte tillbaka till vandrarhemmet och gick upp på mitt rum där jag lade mig och spelade TV-spel för första och sista gången denna resa. Det kändes väldigt skönt att för en gångs skull strunta i att skriva på bloggen, och faktiskt bara ägna mig åt att göra det jag kände för där och då.

Efter någon timme bestämde jag mig för att det nog var dags att sova, och släckte lampan. Det hade börjat regna kraftigt vid det här laget, och eftersom jag hade ett run precis under taket somnade jag till ljudet av regn som smattrade mot plåttak.
