+++
draft = false
title = "Cykelsemester 2020: Dag 11"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-17"
type = "post"

+++

# Cykelsemester 2020: Dag 11

Jag vaknade upp på hotellet i Hässleholm och gick ner för att äta frukost. Som på dom flesta hotell hittills serverades den som buffé, och den var riktigt bra.

Med frukost i magen gick jag sedan till apoteket för att köpa mer skavsårsplåster och food bars. Det gäller att passa på när man är i ett större samhälle för en gångs skull.

Efter utcheckningen så gav jag mig sedan iväg. Dagen började ganska tråkigt på en cykelväg som gick längs den högtrafikerade riksvägen 25. Den bjöd inte på några fina vyer direkt, utan jag fick nöja mig med att titta på trafiken som åkte förbi. Som tur var fortsatte inte vägen så långt, utan jag kunde snart ta av till mindre vägar igen.

En intressant observation som jag har gjort är att cykelvägarna bara blir sämre och sämre ju längre jag åker från Skåne. Skåne är verkligen ett paradis för cyklister, och resten av Sverige skulle må bra av att ta lärdom av hur infrastrukturen för cykling fungerar där. Dessutom är ju landskapet relativt platt där, men just det är ju kanske svårt att göra så mycket åt i andra delar av Sverige.

Ungefär en timme efter avfärd kom jag fram till ett litet samhälle vid namn Hästveda, som faktiskt hade en ICA-butik
Där stannade jag till och köpte Red Bull och lite godis. Förutom ICA-butiken hade samhället till synes inte så mycket att erbjuda. Det verkade vara en ganska så deprimerande plats.

Jag såg ingen anledning att stanna där någon längre tid, så jag fortsatte rakt mot mitt nästa delmål på dagens resa - Älmhult, där jag även planerade att äta lunch.

Det dröjde inte allt för länge innan jag rullade in på stadens kullerstenstäckta gator, och stannade för att fråga en man efter rekommendationer för lunchrestauranger. Han pekade mig i rätt riktning, och efter lite övervägande valde jag en asiatisk restaurang eftersom jag kände för någon mat som inte var så mastig. Det var nämligen ganska varmt ute, och det hade påverkat min aptit.

Jag beställde en rätt som hette "biff szechuan", mest för att jag har fått för mig att jag gillar mat från den regionen i Kina. Det visade sig vara en rätt med biff serverad i en slags mild och krämig tomatsås. Det var helt okej, men det blev lite mastigare än vad jag hade tänkt trots allt.

Efter att ha ätit hela den rätten var jag minst sagt mätt, och rullade ut från Älmhult med målet satt på att avverka dom sista 4 eller så milen till mitt boende för dagen, "Villa Gransholm", som tidigare hade varit  boende för dom rika ägarna till pappersbruket i området av vad jag kunde läsa mig till.

Om jag hade tyckt att det var tråkigt att cykla på cykelväg längs landsväg 23 tidigare så fick jag nu perspektiv på saker och ting. Jag var nämligen tillbaka till samma väg, men den här gången fanns ingen cykelväg bredvid, utan jag var tvungen att cykla på den högtrafikerade 2+1-vägen som var skyltad för en hastighetsbegränsning på 100 km/h, komplett med vajerräcke i mitten och allt.

Det var varken motorväg eller motortrafikled, så cykling var tillåtet där, men det var minst sagt läskigt. På dom bitar där det bara var ett körfält i min färdriktning höll jag mig så långt ut i vägrenen som jag kunde, och bad för att det inte skulle komma någon stor lastbil.

Det var med enorm lättnad jag kunde svänga av från den stora vägen cirka 1 mil senare, ut på mindre vägar igen. Efter en liten bit blev asfaltsvägen till grusväg som tog mig nästan hela den sista biten fram till boendet som var en stor, vitputsad herrgårdsliknande byggnad.

Jag gick in till receptionen och ringde på klockan, varpå en väldigt trevlig man dök upp och gav mig ett varmt välkomnande och hjälpte mig att checka in. Jag fick ett rum på övervåningen av byggnaden och gick upp för att inspektera det. Det var utan tvekan ett väldigt häftigt, hus med mycket historia. Även om det numera var renoverat och ombyggt till hotell så fanns mycket av den ursprungliga designen och känslan kvar.

Jag tog som vanligt en varm dusch, och byttte sedan om från cykelkläderna och gick ner till umgängesrummet på nedervåningen där rabarberkaka, kaffe och te serverades till alla gäster. Jag tog en bit rabarberkaka och gick ut i trädgården för att äta den i solen.

Kakan var fantastiskt god, och trädgården var avslappnande och trevlig, med en damm i mitten som var omringad av stora praktfulla rabatter. Jag spenderade lite tid där ute vid ett bord med min laptop för att skriva lite på bloggen, och när klockan började närma sig 7 började jag röra mig inåt för att äta middag.

Jag blev dock stoppad på vägen av en kvinna som satt vid ett bord nära ingången tillsammans med sin man. Hon frågade om jag hade jobbat färdigt nu, varpå jag svarade att det inte var jobb, utan att jag skrev på min blogg. Det ledde oss in i en lång konversation om lite allt möjligt, och jag fann att dom båda var väldigt trevliga.

Kvinnan nämnde att jag var väldigt lik tjejen i serien "normala människor", vilket såklart var ett roligt sammanträffande eftersom jag hade kollat på den för första gången bara 2 dagar tidigare. Jag förstod dock vad hon menade. Med min fläta och lugg fanns det definitivt en viss likhet.

Middagen serverades inne i restaurangen, och bestod av 3 rätter. Som på  dom flesta andra boenden jag bott på var rätterna här också lagade nästan uteslutande på närodlade, säsongsberoende ingredienser.

Förrätten bestod av yoghurtklickar med honung i, där bina tillhörde en av dom anställda. Till det serverades även en "coppa", som tydligen var som parmaskinka, fast från Sverige, och i det här fallet ett lokalt charkuteri. På sidan av detta serverades även en gazpacho gjord på fläder. Jag har väldigt svårt för gazpacho, och den här var inget undantag, men  jag fick i mig den. Resten av förrätten var helt okej, men ingen smaksensation direkt.

Varmrätten var desto bättre. Oxfilé från en lokal gård tillsammans med en god kryddkorv. Till detta serverades ungsrostade primörer och en polenta som var väldigt god. Som grädde på moset kom rätten med en rökig emulsion och vitlöksmajonnäs. Riktigt gott.

Efterrätten var också god. En rykande färsk chokladfondant med marinerade bär.

Värt att nämna är också att servicen var helt otrolig. Rätterna serverades på löpande band, med bara några minuters mellanrum, trots en stor mängd gäster. Väldigt imponerande.

Mätt och belåten gick jag för att sova i det klassiskt utformade rummet. Det var ganska varmt där inne, så jag öppnade fönstret. Tyvärr hade jag fönster direkt vid någon slags utblås från ventilationen i köket, och det väsnades en hel del, men det verkade lugna ner sig mot midnatt när aktiviteten i köket avtog.
