+++
draft = false
title = "Cykelsemester 2020: Dag 16"
tags = [
  "cykling",
  "semester",
]
date = "2020-07-22"
type = "post"

+++

# Cykelsemester 2020: Dag 16

Blaxsta vingård var ett av dom ställen där frukosten serverades på utsatt tid för samtliga gäster. Det var lätt att förstå, eftersom hela verksamheten, som tidigare nämnts, drevs av endast två personer. Både Ulrika och Göran närvarade under frukostserveringen, och vi gäster bjöds på nybakat bröd med massvis av pålägg, färskpressad juice, och yoghurt med färska bär. Faktum är att dom blåbär som jag toppade min yoghurt med var plockade samma morgon.

Jag såg till att få i mig rejält med mat, uppmuntrad av både Ulrika och Göran, som verkade tycka att det var viktigt att utfordra cyklisten. När jag lämnade frukosten fick jag även med mig den frukt och dom bär som var kvar, för att äta under cykelturen. Som ni kan se var gästfriheten verkligen på topp.

Efter att ha packat, checkat ut och satt mig på cykeln igen gav jag mig iväg ut på dom idylliska småvägarna, som dock snart blev både större och mer högtrafikerade. Cirka 40 minuter senare cyklade jag genom Flen, och fick se Hotell Loftet med mina egna ögon när jag stannade till för att köpa en Vitamin Well på den OK/Q8-mack som låg mitt emot hotellet. Jag gick dock aldrig in för att se om Google-recensionerna faktiskt stämde eller inte. Till min förvåning gav staden som helhet ett betydligt trevligare intryck än vad jag hade förväntat mig. 

Efter Flen kom jag ut på 100-väg igen, vilket inte var speciellt kul. Med facit i hand skulle jag gärna ha lagt om rutten för att skippa dessa vägar, för det är verkligen olustigt att cykla där. Det var dock svårt för mig att göra någonting åt det just där och då. En snabb titt på kartan visade att det inte fanns några andra rimliga vägar att ta, utan jag skulle ha behövt vända och ta en helt annan väg. Jag fortsatte alltså framåt.

Ytterligare cirka 40 minuter senare började jag närma mig Malmköping, vilket pressade ner hastigheterna något. Jag såg skyltning för ett glasscafé, och tog chansen att komma av vägen och fylla på med lite energi. Stället hette "Grinda gårdsglass", var familjeägt och serverade glass som tillverkades på den gård som låg bara några hundra meter från själva caféet. Dom hade dock inte bara glass, utan jag passade på att beställa en nybakad citronpaj som jag sedan toppade med glass. Till det valde jag en lemonad att dricka, och gick ut för att sätta mig. Det blåste riktigt kalla och höstiga vindar, och hade det inte varit för solen som värmde hade det varit väldigt kallt.

Efter Malmköping kunde jag bitvis ta in på mindre trafikerade vägar vilket var en stor lättnad. En av dessa var en liten och mycket trevlig skogsväg. Att få cykla skogsväg kändes härligt efter att ha cyklat på stora asfaltsvägar så länge. Det dröjde dock inte länge innan jag var tvungen att svänga ut på 100-vägen igen. 

Jag stannade till vid en parkeringsficka och åt upp den frukt jag fått med mig från Blaxsta. Det var allt ifrån färska jordgubbar till vattenmelon och ananas, och även om platsen kanske inte var den trevligaste så njöt jag ändå av att stå där i solen.

Några mil senare hade jag lyckats kämpa mig fram till Strängnäs. Först funderade jag på att stanna till där och äta, men jag valde till sist att bara åka vidare eftersom frukten och den paj med glass jag ätit tidgare fortfarande höll mig relativt mätt. Det ångrade jag lite i efterhand, med tanke på att Strängnäs verkade vara ett väldigt vackert och idylliskt litet samhälle som jag gärna sett mer av. Istället blev det till att åter ge sig ut på den högtrafikerade väg 55 som går mellan Strängnäs och Enköping.

Faktum är att den här biten av vägen nog var den absolut värsta. Väggrenen var i princip obefintlig, och även om hastigheterna sänkts från 100 till 90 så gjorde det i praktiken ingen skillnad för mig. Det hade börjat blåsa en väldigt stark motvind snett från sidan, vilket gjorde att jag var tvungen att omfördela vikten på så vis att jag lutade inåt mot körbanan för att inte dom starka vindbyarna skulle blåsa omkull mig ut i diket på sidan av vägen. När det däremot åkte förbi stora långtradare så sögs jag snarare mot vägbanan, vilket krävde att jag snabbt omfördelade vikten åt andra hållet istället för att inte ramla in i körbanan. Och givetvis, så fort långtradaren åkt förbi krävdes det ett snabbt skifte åt andra hållet igen. Så där höll jag på, i över 2 mil, och det var minst sagt jobbigt.

Höjdpunkten var bron över vattnet mellan Strängnäs och Enköping. Hade inte väg 55 varit så högtrafikerad, eller om det hade funnits en vettig cykelväg som följt denna, hade det nog varit en fin sträcka att cykla. Omgivningarna var vackra, och just den här bron bjöd på mäktiga vyer ut över vattnet.

När jag började närma mig Enköping kom även resans första skyltning mot Uppsala. Uppsala må vara ganska långt från Hudiksvall, men just området längs Ostkustbanan har jag varit i så mycket att det känns som hemma vid det här laget, och för första gången denna resa började jag känna att jag faktiskt närmade mig resans slut.

En avfart från väg 55 tog mig in i Enköping, och efter en titt på mobilens karta hittade jag till hotellet som låg beläget centralt. Receptionisten var mycket trevlig och tillmötesgående, och hjälpte mig att låsa in cykeln i ett källarutrymme så att jag inte behövde oroa mig för att den skulle bli stulen. Jag tog sedan hissen upp till mitt rum, bara för att upptäcka att nyckelkortet inte fungerade, så det fick bli ett till snabbt besök ner till receptionen för att få det omkodat.

Rummet var stort och kändes fräscht, men var väldigt sparsamt inrett, vilket gav det en sjukhusliknande känsla. Jag lämnade min packning, duschade, bytte om, och gick sedan ner en tredje gång till receptionen för att bli rekommenderad en restaurang att inta min middag på. Receptionisten rekommenderade en restaurang vid namn "Hamnmagasinet" som var belägen nära den kanal som leder in i Enköping från Mälaren. Restaurangen var ett av många hus som kantade kanalen, lite likt sjöbodarna vid Möljen i Hudiksvall. Just "Hamnmagasinet" verkade vara det mest populära stället, och först verkade det som om jag inte skulle kunna få någon plats, men det visade sig bero på ett missförstånd. Tydligen hade dom hört fel, och trott att jag ville ha bord för fem personer. När det misstaget var utrett fick jag ett bord, och en kypare kom och tog min beställning. Jag valde att beställa oxfilépasta till varmrätt, och rabarber- och jordgubbskaka till eferrätt.

Servicen var initialt ganska bra, och speciellt varmrätten kom väldigt snabbt. Jag blev dock lite besviken, eftersom jag hade förväntat mig något i nivå med den oxfilépasta som Andreas brukade laga, men det levde den tyvärr inte upp till. Efterrätten dröjde betydligt längre, och när jag väl fick den var den också ganska medelmåttig. Det dröjde även ett bra tag innan jag fick betala, men det var en fin och solig sommarkväll, så det gjorde mig inte så mycket. Efter att ha betalat gick jag tillbaka till hotellet där jag lade mig i sängen för att skriva på bloggen några timmar innan det till sist var dags att sova.

![Astoria](/astoria.jpg)
